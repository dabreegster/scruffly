package scruffly

import scruffly.engine._
import scruffly.util._

import org.newdawn.slick

abstract class Renderer {
  // The caller must translate to the position of the sprite before calling this
  def draw(obj: Sprite)
  def get_dims(): Pt
  def set_mode(mode: String) {}
  def cloneme(): Renderer
}
case class NullRenderer() extends Renderer {
  override def draw(obj: Sprite) {}
  override def get_dims() = throw new UnsupportedOperationException()
  override def cloneme = copy()
}
case class ImageRenderer(fn: String) extends Renderer {
  override def draw(obj: Sprite) {
    Game.engine.gfx.image(fn)
  }
  override def get_dims() = Game.engine.assets.get_image_dims(fn)
  override def cloneme = copy()
}
case class TextureRenderer(points: List[Pt], fn: String) extends Renderer with GeometryFactory {
  override def draw(obj: Sprite) {
    Game.engine.gfx.texture_polygon(points, fn)
  }
  override def get_dims() = calc_dims(points)
  override def cloneme = copy()
}
case class PolygonRenderer(
  points: List[Pt], filled: Boolean, closed: Boolean, color: RGB
) extends Renderer with GeometryFactory {
  override def draw(obj: Sprite) {
    Game.engine.gfx.polygon(points, filled, closed, color)
  }
  override def get_dims() = calc_dims(points)
  override def cloneme = copy()
}

case class SpritesheetRenderer(sheet: Spritesheet, default_anim: String, speed: Float = 1f) extends Renderer {
  private var anim = default_anim
  private var frame = 0f
  override def draw(obj: Sprite) {
    sheet.draw_frame(anim, frame.toInt)
  }
  override def get_dims() = sheet.get_dims
  override def set_mode(requested_mode: String) {
    // Keep the same animation if we're asked to show one we don't know
    val mode =
      if (sheet.has(requested_mode))
        requested_mode
      else
        anim

    if (anim == mode) {
      frame += speed
    } else {
      anim = mode
      frame = 0
    }
  }
  override def cloneme = copy()
}

abstract class Spritesheet() {
  // frame should be applied mod num_frames
  def draw_frame(animation: String, frame: Int)
  // Assume all the same size
  def get_dims(): Pt
  def has(animation: String): Boolean
}

// TODO move to slick?
class SlickFileSpriteSheet(
  assets: SlickAssetsModule, fn: String, tile_width: Int, tile_height: Int, raw_names: List[String]
) extends Spritesheet {
  private val names: Map[String, Int] = raw_names.zipWithIndex.toMap
  private lazy val sheet = new slick.SpriteSheet(assets.get_image(fn), tile_width, tile_height)
  //assert(sheet.getVerticalCount == names.size)

  override def draw_frame(animation: String, frame: Int) {
    // TODO same num of frames for all animations, for now :\
    sheet.getSprite(frame % sheet.getHorizontalCount, names(animation)).draw(0, 0)
  }
  override def get_dims(): Pt = {
    val img = sheet.getSprite(0, 0)
    return Pt(img.getWidth, img.getHeight)
  }
  override def has(animation: String) = names.contains(animation)
}
