package scruffly.components

import scruffly._
import scruffly.engine._
import scruffly.util._

import scala.collection.mutable

class ErrorAggregator() extends Component {
  private var errors_last_tick: Set[String] = Set()
  private val errors_this_tick = new mutable.HashSet[String]()

  def warn(msg: String) {
    errors_this_tick += msg
  }

  override def update(dt: Float) {
    if (errors_this_tick.toSet != errors_last_tick) {
      for (msg <- errors_this_tick) {
        println(msg)
      }
      println("")

      errors_last_tick = errors_this_tick.toSet
    }
    errors_this_tick.clear()
  }
}
