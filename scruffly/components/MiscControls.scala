package scruffly.components

import scruffly.Game
import scruffly.engine._
import scruffly.util._

class Keypress(key: String) {
  private var was_key_down = false

  def pressed(): Boolean = {
    val now = Game.engine.keymap.keydown(key)
    if (!was_key_down && now) {
      was_key_down = true
      return true
    } else {
      was_key_down = now
      return false
    }
  }
}

class Buttonpress(button: String) {
  private var was_button_down = false

  def pressed(): Boolean = {
    val now = Game.engine.mouse.button_down(button)
    if (!was_button_down && now) {
      was_button_down = true
      return true
    } else {
      was_button_down = now
      return false
    }
  }
}

class DebugControls() extends Component {
  private val debug_key = new Keypress(Game.engine.keymap.key_debug)

  override def update(dt: Float) {
    if (debug_key.pressed) {
      Game.engine.gfx.debug = !Game.engine.gfx.debug
    }
  }
}

class ZoomControls() extends Component {
  override def update(dt: Float) {
    val gfx = Game.engine.gfx

    val old_scale = gfx.scale
    if (Game.engine.keymap.keydown(Game.engine.keymap.key_zoom_in)) {
      gfx.scale *= 1.1f
    }
    if (Game.engine.keymap.keydown(Game.engine.keymap.key_zoom_out)) {
      gfx.scale *= 0.9f
    }
    if (Game.engine.keymap.keydown(Game.engine.keymap.key_reset_zoom)) {
      gfx.scale = 1
    }

    val wheel = Game.engine.mouse.scroll_since_last
    if (wheel > 0) {
      gfx.scale *= 1.1f
    } else if (wheel < 0) {
      gfx.scale *= 0.9f
    }

    // Zoom towards the mouse
    if (gfx.scale != old_scale) {
      val pos = Game.engine.mouse.get_screen_pos
      gfx.x_offset = (gfx.scale * (gfx.x_offset + pos.x) / old_scale) - pos.x
      gfx.y_offset = (gfx.scale * (gfx.y_offset + pos.y) / old_scale) - pos.y
    }
  }
}

// Users have to poll the value
class Tuner(initial: Float, step_size: Float = .01f) {
  private var value = initial
  private val down_key = new Keypress(Game.engine.keymap.key_tune_down)
  private val up_key = new Keypress(Game.engine.keymap.key_tune_up)

  def get_value(): Float = {
    if (down_key.pressed) {
      value -= step_size
      println(s"Tuned value is $value")
    }
    if (up_key.pressed) {
      value += step_size
      println(s"Tuned value is $value")
    }
    return value
  }
}

class InputForm(initial: String) extends Component with GeometryFactory {
  var result: Option[String] = None
  Game.engine.keymap.start_recording(initial)

  override def update(dt: Float) {
    if (Game.engine.keymap.accumulated_input.endsWith("\n")) {
      Game.engine.keymap.stop_recording()
      result = Some(Game.engine.keymap.accumulated_input.stripSuffix("\n"))
    }
  }

  override def render(gfx: DrawModule) {
    // Draw relative to the screen!
    gfx.screen_transforms()

    gfx.polygon(
      translate(rect(gfx.get_screen_width, 50), 0, gfx.get_screen_height - 50),
      true, true, RGB(255, 255, 255)
    )
    gfx.text(Game.engine.keymap.accumulated_input, 10, gfx.get_screen_height - 50, RGB(255, 0, 0))

    gfx.camera_transforms()
  }
}
