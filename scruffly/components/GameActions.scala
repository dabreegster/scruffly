package scruffly.components

import scruffly._
import scruffly.engine._
import scruffly.util._

// Context-sensitive interact thingy
class InteractButton() extends Component with GeometryFactory with Disableable {
  private var action: Option[String] = None
  private var actor: Option[Sprite] = None
  private val interact_key = new Keypress(Game.engine.keymap.key_interact)
  // TODO a hack to make order work. something presses us, we update and clobber it, then render
  // nothing. the fix is to have z-order and force ourselves on top for rendering, but early for
  // updating.
  private var survive = false

  override def update(dt: Float) {
    if (!survive) {
      action = None
      actor = None
    }
    survive = false
  }

  def is_first_action(act: String) = action.getOrElse(act) == act

  def pressed(player: Sprite, act: String): Boolean = {
    if (enabled && is_first_action(act)) {
      action = Some(act)
      actor = Some(player)
      survive = true
      return interact_key.pressed
    } else {
      return false
    }
  }

  override def render(gfx: DrawModule) {
    for (act <- action) {
      gfx.camera_transforms()
      gfx.image(
        "hotel/interact_mark.png", actor.get.position_x, actor.get.position_y - 60
      )
    }
  }
  override def zorder = scruffly.hotel.Hotel.gui_zorder
}
