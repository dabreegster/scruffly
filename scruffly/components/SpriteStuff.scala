package scruffly.components

import scruffly._
import scruffly.engine._
import scruffly.util._

// Should be registered after the sprite moves
class FollowSpriteCamera(val owner: Sprite) extends Component with Disableable with GeometryFactory {
  // This is here for convenience; this is a common component that has the player
  private val debug_tile_key = new Keypress(Game.engine.keymap.key_debug_tile)

  override def update(dt: Float) {
    if (enabled) {
      // Limit at borders of level for that nice gooey closed-in feel
      Game.engine.gfx.center(owner.get_center, Some(owner.world.bounds))

      // Conveniently have player-dependent debugish things here
      if (debug_tile_key.pressed) {
        val map = owner.world.get_tilemap.get
        val at = map.unpixel(owner.get_collider_pos)
        println(s"At $at")
        println("  Scenery: " + map.get(at).platforms)
        println("  Sprites: " + map.sprites(at))
      }
    }
  }

  override def render(gfx: DrawModule) {
    val text_height = 50
    val x_offset = (gfx.get_screen_width / 2) - 100
    val text_width = 600

    if (gfx.debug) {
      gfx.set_translation(gfx.x_offset + x_offset, gfx.y_offset + gfx.get_screen_height - text_height)
      gfx.polygon(rect(text_width, text_height), true, true, RGB(0, 0, 0))
      val mouse_at = Game.engine.mouse.get_map_pos

      gfx.text(
        s"Mouse at $mouse_at (tile ${owner.world.get_tilemap.get.unpixel(mouse_at)})",
        0, 0, RGB(0, 0, 255)
      )
    }
  }
  override def zorder = scruffly.hotel.Hotel.gui_zorder
}

// Between topdown and platformer
class PhysicsSwitcher(sprite: Sprite) extends Component {
  protected def default_mode = sprite.instance.physics

  private var alt_physics: PhysicsModule = default_mode match {
    case "platformer" => new TopdownPhysicsModule(
      Game.assets(sprite.instance.asset, sprite.instance.effects, true).topdown_lines, sprite
    )
    case "topdown" => new PlatformerPhysicsModule(
      Game.assets(sprite.instance.asset, sprite.instance.effects, true).platformer_lines, sprite
    )
  }
  private var alt_behavior: Behavior = alt_physics match {
    case t: TopdownPhysicsModule => new TopdownControllerBehavior(t)
    case p: PlatformerPhysicsModule => new PlatformerControllerBehavior(p)
  }
  protected var currently_in_zone = false

  protected def match_zone = "toggle_physics"
  // Toggle upon entry
  protected def toggle(zone_now: Boolean) = !currently_in_zone && zone_now

  override def update(dt_s: Float) {
    val zone_now = sprite.in_zone(match_zone)

    if (toggle(zone_now)) {
      // Enter
      val old_physics = sprite.physics
      val old_behavior = sprite.behavior
      sprite.physics = alt_physics
      sprite.behavior = alt_behavior
      alt_physics = old_physics
      alt_behavior = old_behavior
      sprite.speed_x = 0
      sprite.speed_y = 0
    }
    currently_in_zone = zone_now
  }
}

class ClimbPhysicsSwitcher(sprite: Sprite) extends PhysicsSwitcher(sprite) {
  // So it has no effect if starting in topdown mode
  override def default_mode = "platformer"
  override def match_zone = "climb"
  // Toggle upon entry or exit
  override def toggle(zone_now: Boolean) =
    (!currently_in_zone && zone_now) || (currently_in_zone && !zone_now)
}

class ResetPosition(sprite: Sprite) extends Component {
  private val reset_key = new Keypress(Game.engine.keymap.key_reset_zoom)

  override def update(dt: Float) {
    if (reset_key.pressed) {
      sprite.warp(sprite.instance.start)
      sprite.physics match {
        case p: PlatformerPhysicsModule => p.is_jumping = true
        case _ =>
      }
    }
  }
}

class LevelSwitcher(owner_factory: () => Option[Sprite]) extends Component {
  private var current_switch: Option[Cutscene] = None

  override def update(dt: Float) {
    for (owner <- owner_factory()) {
      val interact = Game.engine.ez_interact(owner.world.level)

      current_switch match {
        case Some(scene) if scene.done => current_switch = None
        case _ =>
      }
      for (z <- owner.get_zone("switch_levels")) {
        val target = z.data("level")
        if (!current_switch.isDefined && interact.pressed(owner, s"Warp to $target") && !Cutscene.in_scene) {
          do_switch(owner, target)
        }
      }
    }
  }

  def do_switch(owner: Sprite, target: String) {
    val orig_level = owner.world.level
    // Make our collider_pos start at that of the portal
    val spawn_at = scruffly.hotel.Hotel.get_portal_start_pos(target, orig_level, owner)
    val new_owner = Instance(
      owner.instance.asset, spawn_at,
      owner.instance.effects, "topdown"
    ).manifest(scruffly.hotel.Hotel.get_level(target), true)

    current_switch = Some(new Cutscene(owner.world.level,
      Cutscene.act(() => {
        owner.destroy()
        Game.engine.safely_add_component(new_owner, target)
      }),
      Cutscene.just_switch_levels(owner.world.level, target, Some(new_owner))
    ).start(None))
  }
}

class ShakeEffect(owner: Sprite) extends Component {
  private var countdown = 0f
  private var orig_x: Option[Float] = None
  private var orig_y: Option[Float] = None

  override def update(dt: Float) {
    countdown = math.max(0, countdown - dt)
    // TODO actually changing position is dangerous without doing world.before/after_move...
    // This assumes the sprite doesn't move normally during shaking.
    if (countdown > 0) {
      owner.position_x = orig_x.get + RNG.int(-3, 3)
      owner.position_y = orig_y.get + RNG.int(-3, 3)
    } else if (orig_x.isDefined) {
      // Stop shaking
      owner.position_x = orig_x.get
      owner.position_y = orig_y.get
      orig_x = None
      orig_y = None
    }
  }

  def shake(time: Float) {
    countdown = time
    // Just continue shaking if we already are
    if (!orig_x.isDefined) {
      orig_x = Some(owner.position_x)
      orig_y = Some(owner.position_y)
    }
  }
}

class LineOfSight(sprite: Sprite, sight: Sprite) extends Component with GeometryFactory {
  private var added_zone = false

  override def update(dt: Float) {
    if (!added_zone) {
      added_zone = true
      if (!Game.engine.all_components(sight.world.level).contains(sight)) {
        Game.engine.safely_add_component(sight, sight.world.level)
      }
    }

    val radius = 300
    val da = 35
    val cone = points_to_closed_polygon(List(Pt(0, 0)) ++ circle_arc(
      radius, sprite.direction_angle - da, sprite.direction_angle + da, segments = 1
    ))

    sight.world.before_move(sight)
    sight.physics = new TopdownPhysicsModule(cone, sight)
    sight.world.after_move(sight)
    sight.warp(sprite.get_center)
  }
}

// Immediately releases the first, self-destructs when done
class StaggeredSpawner(ls: List[Sprite], delay: Float, level: String) extends Component {
  private var timer = 0f
  Game.engine.safely_add_component(ls.head, level)
  private var queue = ls.tail

  override def update(dt: Float) {
    timer += dt
    if (timer >= delay) {
      timer = 0
      if (queue.nonEmpty) {
        Game.engine.safely_add_component(queue.head, level)
        queue = queue.tail
      }
    }
    if (queue.isEmpty) {
      Game.engine.del_component(this, level)
    }
  }
}

class PauseSprite(owner: Sprite) extends Component {
  private var behavior: Option[Behavior] = None
  // when None, we've been forcibly paused until unpausing
  private var countdown: Option[Float] = Some(0)

  override def update(dt: Float) {
    if (behavior.isDefined) {
      for (time <- countdown) {
        countdown = Some(time - dt)
        if (time - dt <= 0) {
          unpause()
        }
      }
    }
  }

  def unpause() {
    assert(behavior.isDefined)
    countdown = Some(0)
    owner.behavior match {
      // Good, restore the original behavior
      case _: NullBehavior => owner.behavior = behavior.get
      // Changed behaviors in the meantime, don't touch em
      case _ =>
    }
    behavior = None
  }

  def pause(time: Option[Float]) {
    countdown = time
    owner.speed_x = 0
    owner.speed_y = 0
    if (!behavior.isDefined) {
      behavior = Some(owner.behavior)
      owner.behavior = new NullBehavior(owner)
    }
  }

  def is_paused = behavior.isDefined
}

class Health(owner: Sprite, initial: Int) extends Component with GeometryFactory {
  private var current = initial
  // Can only be hurt when cooldown is 0
  private var cooldown = 0f
  // Use for flashing red
  private var pain_counter = 0

  def change(delta: Int) {
    if (delta > 0 || cooldown == 0) {
      current += delta
      // immune for 3 seconds
      cooldown = 3
      if (current <= 0) {
        owner.destroy()
      }
    }
  }

  override def update(dt: Float) {
    cooldown = math.max(0, cooldown - dt)
  }

  override def render(gfx: DrawModule) {
    if (cooldown > 0) {
      pain_counter += 1
      if (pain_counter % 8 < 4) {
        gfx.set_translation(0, 0)
        val dims = owner.renderer.get_dims
        val pts = translate(rect(dims.x, dims.y), owner.position_x, owner.position_y)
        gfx.polygon(pts, true , true, RGB(255, 0, 0, 120))
      }
    }
  }
}

class ShootControls(player: Sprite) extends Component {
  private val shoot_key = new Keypress(Game.engine.keymap.key_shoot)

  override def update(dt_s: Float) {
    if (shoot_key.pressed) {
      val start = player.get_pos
      val bullet = Instance("spray_bullet", start, List("bullet=300"), "topdown").manifest(
        player.world, true
      )
      Game.engine.safely_add_component(bullet, player.world.level)

      val angle = player.direction_angle
      bullet.speed_x = (400 * math.cos(math.toRadians(angle))).toFloat
      bullet.speed_y = -(400 * math.sin(math.toRadians(angle))).toFloat
    }
  }
}

class DebugSprite() extends Component {
  private val off_key = new Keypress(Game.engine.keymap.key_editor_cancel)
  private var focus: Option[Sprite] = None

  override def update(dt: Float) {
    if (!focus.isDefined && Game.engine.mouse.button_down(Game.engine.mouse.btn_left)) {
      val all_candidates = scruffly.hotel.Hotel.get_current_level.all_hits(
        Game.engine.mouse.get_map_pos
      ).filter(s => scruffly.hotel.Hotel.all_chars.contains(s.name))
      for (candidate <- all_candidates.headOption) {
        focus = Some(candidate)
        focus.get.debug_me = true
        println(s"Debugging enabled for $candidate")
      }
    }
    if (focus.isDefined && off_key.pressed) {
      focus.get.debug_me = false
      println(s"Debugging disabled for ${focus.get}")
      focus = None
    }
  }
}

class GlowSprite(owner: Sprite) extends Component {
  // TODO similar z-order hack as InteractButton
  private var survive = 0

  override def update(dt: Float) {
    survive = math.max(0, survive - 1)
  }

  def glow() {
    survive = 3
  }

  override def render(gfx: DrawModule) {
    if (survive > 0) {
      gfx.camera_transforms()
      val box = owner.get_hitbox
      val center = owner.get_collider_pos
      // TODO actually glow and look cool
      gfx.unfilled_ellipse(box.width, box.height, RGB(255, 255, 255), center.x, center.y)
    }
  }

  // Always render just under the sprite
  // TODO the zorder doesnt seem to work! :(
  override def zorder = owner.zorder - 1
}
