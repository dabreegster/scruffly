package scruffly.components

import scruffly._
import scruffly.engine._
import scruffly.util._
import scruffly.tools.RunLevel
import scruffly.hotel.Hotel

trait CutsceneCue extends Component {
  def done(): Boolean
  // Should the cutscene manager nix this component after it's done? Should it add it in the first
  // place?
  def should_destroy(): Boolean
}

class Cutscene(first_level: String, seq: List[CutsceneCue]*) extends Component with GeometryFactory {
  private var queue: List[CutsceneCue] = seq.toList.flatten
  private var current_camera: Option[FollowSpriteCamera] = None

  // Disable certain components from all levels (since we could switch to any of them)
  private val disabled = Game.engine.all_components_from_all.filter(c => c match {
    case f: FollowSpriteCamera => true
    case t: TopdownControllerBehavior => true
    case p: PlatformerControllerBehavior => true
    case i: InteractButton => true
    case _ => false
  })

  override def update(dt: Float) {
    if (queue.head.done) {
      if (queue.head.should_destroy) {
        Game.engine.del_component(queue.head, Game.engine.current_level)
      }
      queue = queue.tail
      // TODO should_destroy also means should it be added? weird...
      if (queue.nonEmpty && queue.head.should_destroy) {
        Game.engine.add_component(queue.head, Game.engine.current_level)
        queue.head match {
          case f: FollowSpriteCue => {
            stop_camera()
            current_camera = Some(f.camera)
          }
          case u: UnfollowSpriteCue => stop_camera()
          case _ =>
        }
      }
    }

    if (done) {
      // Self-destruct. Anything blocking us can still poll done
      Game.engine.del_component(this, "game")
      stop_camera()
      disabled.foreach(c => c.event("cutscene_stop"))
      Cutscene.in_scene = false
      Hotel.clock.unpause()
    }
  }

  override def render(gfx: DrawModule) {
    val band_height = 100
    val color = RGB(0, 0, 0, 220)

    gfx.set_translation(gfx.x_offset, gfx.y_offset)
    gfx.polygon(rect(gfx.get_screen_width, band_height), true, true, color)

    gfx.set_translation(gfx.x_offset, gfx.y_offset + gfx.get_screen_height - band_height)
    gfx.polygon(rect(gfx.get_screen_width, band_height), true, true, color)
  }
  override def zorder = scruffly.hotel.Hotel.gui_zorder

  def start(focus: Option[Sprite], return_last: Option[String] = Some(first_level)): Cutscene = {
    assert(!Cutscene.in_scene)
    Cutscene.in_scene = true
    Hotel.clock.pause()

    // We ALWAYS have to make sure we're in the correct starting level
    if (Game.engine.current_level != first_level) {
      queue = Cutscene.dramatic_switch_levels(Game.engine.current_level, first_level, focus) ++ queue
    }

    // Return to the original level after the cutscene is finished
    if (return_last.isDefined && return_last.get != Game.engine.current_level) {
      val orig_level = Game.engine.current_level
      queue ++= List(
        new FadeOut(),
        new StageAction(() => {
          Game.engine.deactivate(return_last.last)
          Game.engine.activate(orig_level)
          // Focus on the player immediately so any components like lights that depend on a
          // reasonable window don't break
          val focus = scruffly.hotel.Hotel.get_player
          Game.engine.gfx.center(focus.get_center, Some(focus.world.bounds))
        }),
        new FadeIn()
      )
    }

    Game.engine.add_component(queue.head, Game.engine.current_level)
    disabled.foreach(c => c.event("cutscene_start"))

    // The cutscene can safely span multiple levels
    Game.engine.add_component(this, "game")

    return this
  }

  private def stop_camera() {
    for (old_cam <- current_camera) {
      Game.engine.del_component(old_cam, old_cam.owner.world.level)
    }
    current_camera = None
  }

  def done = queue.isEmpty
}

class StageAction(thunk: () => Unit) extends CutsceneCue {
  private var ran = false

  override def update(dt: Float) {
    if (!ran) {
      thunk()
      ran = true
    }
  }

  override def done = true
  override def should_destroy = true
}

object Cutscene {
  // Can only do one cutscene at a time
  var in_scene = false

  def say(who: Sprite, msg: String) =
    say_async(who, msg) ++ List(who.speech)

  def say_async(who: Sprite, msg: String) = act(() => {
    who.ez_say(msg)
  })

  // Focus on the speaker first
  def say_focus(who: Sprite, msg: String) =
    List(new CinematicCamera(who, 500f)) ++ say(who, msg)

  def just_switch_levels(old_level: String, new_level: String, new_focus: Option[Sprite]) = act(() => {
    Game.engine.deactivate(old_level)
    if (Game.engine.has_state(new_level)) {
      Game.engine.activate(new_level)
    } else {
      RunLevel.setup(new_level, Nil)
    }
    // Some components are sensitive to viewing window
    for (target <- new_focus) {
      Game.engine.gfx.center(target.get_center, Some(target.world.bounds))
    }
  })
  def dramatic_switch_levels(old_level: String, new_level: String, new_focus: Option[Sprite]) =
    List(new FadeOut()) ++
    just_switch_levels(old_level, new_level, new_focus) ++
    List(new FadeIn())

  def act(thunk: () => Unit) = List(new StageAction(thunk))

  def follow(target: Sprite) = List(new CinematicCamera(target, 500f), new FollowSpriteCue(target))
  def unfollow() = List(new UnfollowSpriteCue())
  def pause(who: Sprite) = act(() => { who.pauser.pause(None) })
  def unpause(who: Sprite) = act(() => { who.pauser.unpause })
}

trait Disableable extends Component {
  protected var enabled = true

  override def event(ev: String) {
    if (ev == "cutscene_start") {
      enabled = false
    }
    if (ev == "cutscene_stop") {
      enabled = true
    }
  }
}

// TODO nearly StageAction :(
class FollowSpriteCue(target: Sprite) extends CutsceneCue {
  private var ran = false

  val camera = new FollowSpriteCamera(target)

  override def update(dt: Float) {
    if (!ran) {
      Game.engine.add_component(camera, target.world.level)
      ran = true
    }
  }

  override def done = true
  override def should_destroy = true
}
class UnfollowSpriteCue() extends CutsceneCue {
  override def done = true
  override def should_destroy = true
}

class SpeechBubble(owner: Sprite) extends CutsceneCue with GeometryFactory {
  private val skip_key = new Keypress(Game.engine.keymap.key_skip)
  protected var current_text: Option[String] = None

  override def update(dt: Float) {
    if (skip_key.pressed) {
      current_text = None
    }
  }

  def say(msg: String) {
    if (current_text.getOrElse("") != msg) {
      current_text = Some(msg)
    }
  }

  def is_speaking = current_text.isDefined
  def text_now = current_text

  override def render(gfx: DrawModule) {
    for (text <- current_text) {
      val width = gfx.text_width(text)
      val height = gfx.text_height(text)

      gfx.set_translation(owner.position_x, owner.position_y - height)
      gfx.polygon(rect(width, height), true, true, RGB(255, 255, 255, 120))
      gfx.text(text, 0, 0, RGB(0, 0, 0))
    }
  }
  override def zorder = scruffly.hotel.Hotel.gui_zorder

  // TODO will block forever if character triggers some out-of-cutscene condition causing them to
  // always speak
  override def done = !current_text.isDefined
  override def should_destroy = false
}

class CinematicCamera(to: Sprite, speed: Float) extends CutsceneCue {
  private val skip_key = new Keypress(Game.engine.keymap.key_skip)
  private var first_step = true
  private var target_x: Float = 0
  private var target_y: Float = 0

  override def update(dt: Float) {
    if (first_step) {
      first_step = false
      target_x = Game.engine.gfx.centered_pt.x
      target_y = Game.engine.gfx.centered_pt.y
    }

    val dx = to.get_center.x - target_x
    val dy = to.get_center.y - target_y
    val dist = math.sqrt((dx * dx) + (dy * dy))
    if (dist > 0) {
      // TODO this math probably isn't right :P
      if (skip_key.pressed) {
        target_x += dx
        target_y += dy
      } else {
        target_x += ((dx / dist) * math.min(dist, speed * dt)).toFloat
        target_y += ((dy / dist) * math.min(dist, speed * dt)).toFloat
      }
    }

    Game.engine.gfx.center(Pt(target_x, target_y), Some(to.world.bounds))
  }

  override def done = math.abs(to.get_center.x - target_x) <= 1 && math.abs(to.get_center.y - target_y) <= 1
  override def should_destroy = true
}

class FadeOut(speed: Float = 300f) extends CutsceneCue {
  protected val skip_key = new Keypress(Game.engine.keymap.key_skip)
  protected var radius = math.min(Game.engine.gfx.get_screen_width, Game.engine.gfx.get_screen_height) / 2f
  override def update(dt: Float) {
    radius -= dt * speed
    if (skip_key.pressed) {
      radius = 0
    }
  }

  override def render(gfx: DrawModule) {
    val mask = gfx.asInstanceOf[SlickDrawModule].make_circle_overlay(radius)
    gfx.set_translation(gfx.x_offset, gfx.y_offset)
    mask.draw(0, 0)
  }

  override def done = radius <= 0
  override def should_destroy = true
}

class FadeIn(speed: Float = 300f) extends FadeOut(speed) {
  private val max_radius = radius
  radius = 0

  override def update(dt: Float) {
    radius += dt * speed
    if (skip_key.pressed) {
      radius = max_radius
    }
  }

  override def done = radius >= max_radius
}

class ShakeScreen(time: Float) extends CutsceneCue {
  private var orig_x = 0f
  private var orig_y = 0f
  private var countdown = time

  override def update(dt: Float) {
    if (countdown == time) {
      orig_x = Game.engine.gfx.x_offset
      orig_y = Game.engine.gfx.y_offset
    }
    countdown -= dt
    if (countdown > 0) {
      Game.engine.gfx.x_offset = orig_x + RNG.int(-10, 10)
      Game.engine.gfx.y_offset = orig_y + RNG.int(-10, 10)
    } else {
      Game.engine.gfx.x_offset = orig_x
      Game.engine.gfx.y_offset = orig_y
    }
  }

  override def done = countdown <= 0
  override def should_destroy = true
}

class Pause(time: Float) extends CutsceneCue {
  private var countdown = time

  override def update(dt: Float) {
    countdown -= dt
  }

  override def done = countdown <= 0
  override def should_destroy = true
}

class PauseUntil(condition: () => Boolean) extends CutsceneCue {
  override def done = condition()
  override def should_destroy = true
}

class Menu(query: String, choices: List[(String, String)]) extends CutsceneCue with GeometryFactory {
  private var final_choice: Option[String] = None
  private var current_choice = 0
  private val up = new Keypress(Game.engine.keymap.key_up)
  private val down = new Keypress(Game.engine.keymap.key_down)
  private val choose = new Keypress(Game.engine.keymap.key_choose)

  override def done = final_choice.isDefined
  override def should_destroy = true
  def get_choice = final_choice.get

  override def update(dt: Float) {
    if (!final_choice.isDefined) {
      if (up.pressed && current_choice != 0) {
        current_choice -= 1
      }
      if (down.pressed && current_choice != choices.size - 1) {
        current_choice += 1
      }
      if (choose.pressed) {
        final_choice = Some(choices(current_choice)._2)
      }
    }
  }

  override def render(gfx: DrawModule) {
    gfx.screen_transforms()

    // Occupy 80% of the width and height
    val w = gfx.get_screen_width
    val h = gfx.get_screen_height
    gfx.polygon(
      translate(rect(.8f * w, .8f * h), .1f * w, .1f * h),
      true, true, RGB(255, 255, 255, 120)
    )

    val line_height = gfx.text_height("")
    gfx.polygon(
      translate(
        rect(.8f * w, line_height),
        .1f * w, .15f * h + line_height * (2 + current_choice)
      ), true, true, RGB(255, 0, 0, 120)
    )

    gfx.text(
      (List(query, "") ++ choices.map(_._1)).mkString("\n"),
      .15f * w, .15f * h, RGB(0, 0, 0)
    )

    gfx.camera_transforms()
  }
  override def zorder = scruffly.hotel.Hotel.gui_zorder
}
