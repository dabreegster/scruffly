package scruffly.components

import scruffly._
import scruffly.engine._
import scruffly.util._

import scala.collection.mutable

class ShowMouse() extends Component {
  override def render(gfx: DrawModule) {
    val at = Game.engine.mouse.get_map_pos
    gfx.circle(5, RGB(255, 0, 0), at.x, at.y)
  }
  override def zorder = scruffly.hotel.Hotel.gui_zorder
}

class SelectObject(
  world: World, rect_spawner: RectangleSpawner, poly_spawner: PolygonSpawner
) extends Component {
  private val lock_key = new Keypress(Game.engine.keymap.key_editor_lock)
  private val form_key = new Keypress(Game.engine.keymap.key_editor_form)

  def cur_obj = if (locked) current else None

  private var locked = false
  private var current: Option[Sprite] = None
  private var form: Option[InputForm] = None
  override def update(dt_s: Float) {
    if (locked) {
      if (lock_key.pressed) {
        locked = false
        println(s"Unlocked from ${current.get}")
      }
      if (form_key.pressed) {
        val cur_fx = current.get.instance.effects
        form = Some(new InputForm(cur_fx.mkString(", ")))
        Game.engine.add_component(form.get, "editor")
      }
      if (form.flatMap(_.result).isDefined) {
        val new_fx = form.get.result.get.split(", ").toList
        for (s <- current) {
          s.instance = s.instance.copy(effects = new_fx)
        }
        Game.engine.del_component(form.get, "editor")
        form = None
      }
    } else if (!rect_spawner.active && !poly_spawner.active) {
      current = world.find_hits(Game.engine.mouse.get_map_pos)
      if (current.isDefined && (lock_key.pressed || Game.engine.mouse.button_down(Game.engine.mouse.btn_left))) {
        locked = true
        println(s"Locked to ${current.get}")
      }
    }
  }

  override def render(gfx: DrawModule) {
    gfx.set_translation(0, 0)
    for (obj <- current) {
      gfx.lines(obj.get_collider.get_lines, RGB(0, 255, 0))
    }
  }

  def unlock() {
    locked = false
  }
}

class DragObject(select: SelectObject) extends Component {
  private var last_pos: Pt = null
  private val cancel_key = new Keypress(Game.engine.keymap.key_editor_cancel)

  override def update(dt_s: Float) {
    for (cur <- select.cur_obj) {
      if (Game.engine.mouse.button_down(Game.engine.mouse.btn_left)) {
        if (last_pos != null) {
          cur.position_x += Game.engine.mouse.get_map_pos.x - last_pos.x
          cur.position_y += Game.engine.mouse.get_map_pos.y - last_pos.y
        }
        last_pos = Game.engine.mouse.get_map_pos
      } else {
        last_pos = null
      }

      if (cancel_key.pressed) {
        cur.position_x = cur.instance.start.x
        cur.position_y = cur.instance.start.y
      }
    }
  }
}

class TransformObject(select: SelectObject, world: World) extends Component {
  private val delete_key = new Keypress(Game.engine.keymap.key_editor_delete)

  override def update(dt_s: Float) {
    for (cur <- select.cur_obj) {
      val speed = if (Game.engine.keymap.keydown(Game.engine.keymap.key_slow)) 1 else 10
      if (Game.engine.keymap.keydown(Game.engine.keymap.key_editor_left)) {
        cur.position_x -= speed
      }
      if (Game.engine.keymap.keydown(Game.engine.keymap.key_editor_right)) {
        cur.position_x += speed
      }
      if (Game.engine.keymap.keydown(Game.engine.keymap.key_editor_up)) {
        cur.position_y -= speed
      }
      if (Game.engine.keymap.keydown(Game.engine.keymap.key_editor_down)) {
        cur.position_y += speed
      }
      if (delete_key.pressed) {
        select.unlock()
        Game.engine.del_component(cur, "editor")
        world.destroy(cur)
      }
    }
  }
}

class Scroller() extends Component {
  val base_speed = 10

  override def update(dt_s: Float) {
    val speed = base_speed / Game.engine.gfx.scale
    if (Game.engine.keymap.keydown(Game.engine.keymap.key_left)) {
      Game.engine.gfx.x_offset -= speed
    }
    if (Game.engine.keymap.keydown(Game.engine.keymap.key_right)) {
      Game.engine.gfx.x_offset += speed
    }
    if (Game.engine.keymap.keydown(Game.engine.keymap.key_up)) {
      Game.engine.gfx.y_offset -= speed
    }
    if (Game.engine.keymap.keydown(Game.engine.keymap.key_down)) {
      Game.engine.gfx.y_offset += speed
    }
  }
}

class SaveWorld(name: String, world: World) extends Component {
  private val save_key = new Keypress(Game.engine.keymap.key_editor_save)
  override def update(dt_s: Float) {
    if (save_key.pressed) {
      world.to_spec.save(name)
      println(s"Saved $name!")
    }
  }
}

class Spawner(world: World, select: SelectObject) extends Component {
  private val spawn_key = new Keypress(Game.engine.keymap.key_editor_spawn)
  private var chooser: Option[Chooser] = None

  override def update(dt_s: Float) {
    chooser match {
      case Some(c) if c.done => {
        Game.engine.del_component(c, "editor")
        chooser = None
      }
      case _ =>
    }
    if (!chooser.isDefined && spawn_key.pressed) {
      chooser = Some(new Chooser(world, select.cur_obj))
      select.unlock()
      Game.engine.add_component(chooser.get, "editor")
      println("Press TAB to cycle between objects and ENTER to finalize the object")
    }
  }
}
class Chooser(world: World, start: Option[Sprite]) extends Component {
  var done = false
  private val asset = (
    start.map(s => Stream(s.instance.asset)).getOrElse(Stream()) ++
    Stream.continually(Game.assets.names).flatten
  ).iterator

  private var cur_obj: Sprite = null
  private val next_key = new Keypress(Game.engine.keymap.key_editor_next)
  private val done_key = new Keypress(Game.engine.keymap.key_editor_done)

  private var copy_effects = start match {
    case Some(s) => s.instance.effects
    case _ => Nil
  }

  override def update(dt_s: Float) {
    assert(!done)
    if (next_key.pressed && cur_obj != null) {
      cur_obj.destroy()
      cur_obj = null
    }

    if (cur_obj == null) {
      val this_asset = asset.next
      val physics_type = Game.assets(this_asset, Nil, false).recommend_physics
      val cur = Instance(this_asset, Game.engine.gfx.center_map, copy_effects, physics_type).manifest(
        world, false
      )
      Game.engine.add_component(cur, world.level)
      cur_obj = cur
      copy_effects = Nil
    }

    if (done_key.pressed) {
      done = true
      println(s"Done spawning a $cur_obj")
    }
  }
}

class RectangleSpawner(world: World) extends Component with GeometryFactory {
  private var topleft_corner: Option[Pt] = None
  private var bottomright_corner: Option[Pt] = None

  def active = topleft_corner.isDefined

  override def update(dt: Float) {
    // TODO dont start when SelectObject is active
    if (Game.engine.keymap.keydown(Game.engine.keymap.key_editor_rectangle)) {
      if (Game.engine.mouse.button_down(Game.engine.mouse.btn_left)) {
        if (!active) {
          topleft_corner = Some(Game.engine.mouse.get_map_pos)
        }
        bottomright_corner = Some(Game.engine.mouse.get_map_pos)
      }
    } else if (active) {
      val w = bottomright_corner.get.x - topleft_corner.get.x
      val h = bottomright_corner.get.y - topleft_corner.get.y
      val asset = "rectangle_" + w + "_" + h
      val new_obj = Instance(asset, topleft_corner.get, Nil, "topdown").manifest(
        world, false
      )
      Game.engine.add_component(new_obj, "editor")
      topleft_corner = None
      bottomright_corner = None
    }
  }

  override def render(gfx: DrawModule) {
    if (topleft_corner.isDefined) {
      val x1 = topleft_corner.get.x
      val y1 = topleft_corner.get.y
      val x2 = bottomright_corner.get.x
      val y2 = bottomright_corner.get.y
      gfx.set_translation(x1, y1)
      gfx.polygon(rect(x2 - x1, y2 - y1), true, true, RGB(255, 0, 0))
    }
  }
}

class PolygonSpawner(world: World) extends Component with GeometryFactory {
  private val points = new mutable.ListBuffer[Pt]()
  private val draw_button = new Buttonpress(Game.engine.mouse.btn_left)

  def active = points.size > 0

  override def update(dt: Float) {
    // TODO dont start when SelectObject is active
    if (Game.engine.keymap.keydown(Game.engine.keymap.key_editor_polygon)) {
      if (draw_button.pressed) {
        points += Game.engine.mouse.get_map_pos
      }
    } else if (points.size > 2) {
      val center = calc_center(points.toList)
      val asset = "polygon_" + translate(points.toList, -center.x, -center.y).map(pt => pt.x + "/" + pt.y).mkString("_")
      val new_obj = Instance(asset, center, Nil, "topdown").manifest(world, false)
      Game.engine.add_component(new_obj, "editor")
      points.clear()
    }
  }

  override def render(gfx: DrawModule) {
    for (pt <- points) {
      gfx.circle(5, RGB(255, 0, 0), pt.x, pt.y)
    }
  }
}
