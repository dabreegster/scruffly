package scruffly.components

import scruffly._
import scruffly.engine._
import scruffly.util._

import scala.collection.mutable
import scala.util.Random

class Fire(val source: Sprite, val flames: Sprite) extends Component {
  private val height = flames.renderer.get_dims.y
  private var added_flames = false

  override def update(dt: Float) {
    if (!added_flames) {
      added_flames = true
      if (!Game.engine.all_components(flames.world.level).contains(flames)) {
        Game.engine.safely_add_component(flames, flames.world.level)
      }
    }
    flames.warp(source.get_pos.delta(0, -height))

    for (z <- source.current_zones.filter(_.get("zone") == "flammable")) {
      if (!z.ez_on_fire) {
        Game.assets.apply_effect("fire", z)
      }
    }
  }

  def extinguished() {
    Game.engine.del_component(this, source.world.level)
    flames.destroy()
    source.fated_others --= List(this, flames)
  }
}

/*
// TODO could also be rotating at some speed
// These're circle-shaped for now
case class Particle(
  pos_x: Float, pos_y: Float, vel_x: Float, vel_y: Float, base_radius: Float, source: Pt
) {
  def dist = Pt(pos_x, pos_y).dist(source) / 150

  def color = dist match {
    case d if d < 0.1 => RGB(255, 255, 255)
    case d if d < 0.2 => RGB(255, 255, 0)
    case d if d < 0.7 => RGB(255, 0, 0)
    case _ => RGB(120, 120, 120)
  }
  def radius = dist match {
    case d if d < 0.3 => base_radius
    case d if d < 0.6 => base_radius / 1.5
    case _ => base_radius / 2
  }
}

class Fire(spawner: Sprite) extends Component with GeometryFactory {
  private var particles = new mutable.ListBuffer[Particle]()
  private val num_particles = 500

  override def update(dt: Float) {
    // Update particles
    particles = particles.map(p => p.copy(
      pos_x = p.pos_x + p.vel_x * dt,
      pos_y = p.pos_y + p.vel_y * dt
    ))
    particles = particles.filter(p => p.dist < 1)

    // Spawn particles
    val box = spawner.get_hitbox
    val center = Pt((box.x1 + box.x2) / 2, (box.y1 + box.y2) / 2)
    for (i <- Range(0, num_particles - particles.size)) {
      val y = RNG.int(box.y1, box.y2)
      particles += Particle(
        pos_x = RNG.int(box.x1, box.x2),
        pos_y = y,
        vel_x = RNG.float(-25, 25),
        vel_y = RNG.float(-100, -50),
        base_radius = RNG.int(3, 5),
        source = center
      )
    }
  }

  override def render(gfx: DrawModule) {
    for (p <- particles) {
      gfx.circle(p.radius, p.color, p.pos_x, p.pos_y)
    }
  }
}
*/
