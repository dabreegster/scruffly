package scruffly.components

import scruffly._
import scruffly.engine._
import scruffly.util._

import org.newdawn.slick

class WaterEffect() extends Component {
  private val width = 64
  private val height = 64
  private val depth = 32
  private val buffer = new slick.ImageBuffer(width, height)
  private var img: slick.Image = null

  private val frequency = 0.1f
  private var timer = frequency

  // Compute once, re-use
  private val perlin_noise = make_noise

  override def update(dt: Float) {
    timer += dt
    if (timer > frequency) {
      timer = 0

      render_frame()
      if (img != null) {
        img.destroy()
      }
      img = buffer.getImage
    }
  }

  def texture(scenery: Platform) {
    if (img != null) {
      // TODO slick only!
      val slick_gfx = Game.engine.gfx.asInstanceOf[SlickDrawModule].get_gfx

      val poly = new slick.geom.Polygon()
      poly.setClosed(true)
      for (pt <- scenery.get_points) {
        poly.addPoint(pt.x, pt.y)
      }
      // Have to reset to white, else the image gets distorted
      Game.engine.gfx.set_translation(0, 0)
      slick_gfx.setColor(new slick.Color(255, 255, 255))
      slick_gfx.texture(poly, img, 1f / 2f, 1f / 2f, true)
    }
  }

  private var z_pointer = 0
  private def render_frame() {
    z_pointer += 1

    // Find min/max of each component
    val scale_r = new Scaler()
    val scale_g = new Scaler()
    val scale_b = new Scaler()
    for (x <- Range(0, width); y <- Range(0, height)) {
      val alpha = perlin_noise(x)(y)(z_pointer % depth)
      scale_r(Util.lerp(0, 255, alpha).toInt)
      scale_g(Util.lerp(0, 255, alpha).toInt)
      scale_b(Util.lerp(0, 255, alpha).toInt)
    }

    val color1 = RGB(150, 255, 255) // light
    val color2 = RGB(0, 95, 100)   // dark
    for (x <- Range(0, width); y <- Range(0, height)) {
      val alpha = perlin_noise(x)(y)(z_pointer % depth)
      val r = Util.lerp(0, 255, alpha).toInt
      val g = Util.lerp(0, 255, alpha).toInt
      val b = Util.lerp(0, 255, alpha).toInt
      buffer.setRGBA(
        x, y,
        Util.lerp(color1.r, color2.r, scale_r.normalize(r)).toInt,
        Util.lerp(color1.g, color2.g, scale_g.normalize(g)).toInt,
        Util.lerp(color1.b, color2.b, scale_b.normalize(b)).toInt,
        255
      )
    }
  }

  class Scaler() {
    var min = 255
    var max = 0

    def apply(x: Int) {
      min = math.min(min, x)
      max = math.max(max, x)
    }

    def normalize(x: Int) = (x - min).toDouble / (max - min).toDouble
  }

  // TODO try simplex noise for speed and no directional artifacts
  // http://stackoverflow.com/questions/18279456/any-simplex-noise-tutorials-or-resources
  private def make_noise(): Array[Array[Array[Double]]] = {
    // Raw noise
    val base_noise = Array.fill(width, height, depth)(RNG.double(0, 1))

    // Generate some octaves
    val octaves = Range(0, 7).map(octave => {
      val noise = Array.fill(width, height, depth)(0.0)
      val sample_period = math.pow(2, octave).toInt
      val sample_frequency = 1.0 / sample_period

      for (x <- Range(0, width)) {
        val sample_x1 = math.floor(x / sample_period).toInt * sample_period
        val sample_x2 = (sample_x1 + sample_period) % width
        val horizontal_blend: Double = (x - sample_x1) * sample_frequency

        for (y <- Range(0, height)) {
          val sample_y1 = math.floor(y / sample_period).toInt * sample_period
          val sample_y2 = (sample_y1 + sample_period) % height
          val vertical_blend: Double = (y - sample_y1) * sample_frequency

          for (z <- Range(0, depth)) {
            val sample_z1 = math.floor(z / sample_period).toInt * sample_period
            val sample_z2 = (sample_z1 + sample_period) % depth
            val depth_blend: Double = (z - sample_z1) * sample_frequency

            // For depth 1
            val top1 = Util.lerp(
              base_noise(sample_x1)(sample_y1)(sample_z1),
              base_noise(sample_x2)(sample_y1)(sample_z1),
              horizontal_blend
            )
            val bottom1 = Util.lerp(
              base_noise(sample_x1)(sample_y2)(sample_z1),
              base_noise(sample_x2)(sample_y2)(sample_z1),
              horizontal_blend
            )
            val lerped1 = Util.lerp(top1, bottom1, vertical_blend)
            // For depth 2
            val top2 = Util.lerp(
              base_noise(sample_x1)(sample_y1)(sample_z2),
              base_noise(sample_x2)(sample_y1)(sample_z2),
              horizontal_blend
            )
            val bottom2 = Util.lerp(
              base_noise(sample_x1)(sample_y2)(sample_z2),
              base_noise(sample_x2)(sample_y2)(sample_z2),
              horizontal_blend
            )
            val lerped2 = Util.lerp(top2, bottom2, vertical_blend)

            noise(x)(y)(z) = Util.lerp(lerped1, lerped2, depth_blend)
          }
        }
      }

      octave -> noise
    }).toMap

    // Blend the octaves
    val final_noise = Array.fill(width, height, depth)(0.0)
    val persistence = 0.5

    var amplitude = 1.0
    var total_amplitude = 0.0
    for (octave <- Range(0, 7).reverse) {
      amplitude *= persistence
      total_amplitude += amplitude

      for (x <- Range(0, width); y <- Range(0, height); z <- Range(0, depth)) {
        final_noise(x)(y)(z) += amplitude * octaves(octave)(x)(y)(z)
      }
    }

    // Normalize
    for (x <- Range(0, width); y <- Range(0, height); z <- Range(0, depth)) {
      final_noise(x)(y)(z) /= total_amplitude
    }

    return final_noise
  }
}
