package scruffly.components

import scruffly._
import scruffly.engine._
import scruffly.util._

abstract class NPCBaseBehavior(sprite: Sprite) extends Behavior(sprite) {
  protected var goal: TileCoordinate = null // TODO ew
  protected val map = sprite.world.get_tilemap.get

  private val physics = sprite.physics.asInstanceOf[TilePhysicsModule]
  private val debug_color = RGB(RNG.int(0, 255), RNG.int(0, 255), RNG.int(0, 255), 120)

  // TODO bit silly that we do this, but...
  private var last_aligned = cur_tile

  override def update(dt: Float) {
    recompute_timer += dt

    if (last_aligned != cur_tile) {
      // Don't cut corners; first align perfectly with the current tile
      physics.align(cur_tile, dt)
      if (physics.aligned(cur_tile)) {
        last_aligned = cur_tile
      }
    }
    if (last_aligned == cur_tile && path.size > 1) {
      // Go for the next tile now
      physics.align(path.tail.head, dt)
    }
  }

  override def render(gfx: DrawModule) {
    for (tile <- path) {
      map.debug_tiles(tile) = debug_color
    }
  }

  protected def cur_tile = map.unpixel(sprite.get_collider_pos_topleft)

  // All the stuff that can actually pathfind

  protected val must_avoid =
    scruffly.hotel.Hotel.must_avoid(sprite) ++ sprite.world.must_avoid.toSet
  protected val should_avoid =
    scruffly.hotel.Hotel.should_avoid(sprite) ++ sprite.world.should_avoid.toSet

  private var recompute_timer = 0f
  private var cached_path: List[TileCoordinate] = Nil
  private def repath = map.path(cur_tile, goal, must_avoid, should_avoid)
  private def path(): List[TileCoordinate] = {
    // Recompute infrequently, or immediately if we change tiles
    if (cached_path.isEmpty || recompute_timer > .5f || (cached_path.size > 1 && cur_tile == cached_path.tail.head)) {
      recompute_timer = 0
      if (map.in_bounds(goal)) {
        //if (!map.ok_goto(goal, must_avoid)) {
          //Game.errors.warn(s"$sprite can't goto $target because of scenery or $must_avoid")
        //}
        cached_path = repath
        if (cached_path.isEmpty) {
          Game.errors.warn(s"No path to $goal for $sprite (@ ${sprite.get_collider_pos})")
          cached_path = Nil
        }
      } else {
        Game.errors.warn(s"$sprite wants to goto $goal, out of bounds")
        cached_path = Nil
      }
    }
    return cached_path
  }
}

class NPCGotoStaticBehavior(sprite: Sprite, destination: Sprite) extends NPCBaseBehavior(sprite) {
  goal = map.unpixel(destination.get_collider_pos)
}

abstract class PauseNPCBaseBehavior(sprite: Sprite, pause: Float) extends NPCBaseBehavior(sprite) {
  goal = select_new_target

  protected def select_new_target(): TileCoordinate

  // Wait to select a new target
  private var countdown: Option[Float] = None

  override def update(dt: Float) {
    super.update(dt)

    if (cur_tile == goal) {
      countdown match {
        case Some(time) => {
          countdown = Some(math.max(0, time - dt))
          if (countdown.get == 0) {
            // New goal!
            goal = select_new_target
            countdown = None
          }
        }
        case None => countdown = Some(pause)
      }
    }
  }
}

class RoamTileBehavior(
  sprite: Sprite, region: Sprite, exclude_region: Option[Sprite], pause: Float
) extends PauseNPCBaseBehavior(sprite, pause) {
  override def select_new_target(): TileCoordinate = {
    val candidates = sprite.world.get_tilemap.get.select_all
      .filter(tile => map.ok_goto(tile, must_avoid, None))
      .map(tile => map.mid_pixel(tile))
      .filter(pt => Game.engine.collisions.point_in_polygon(region.get_collider.get_lines, pt))
      .filter(pt => exclude_region match {
        case Some(exclude) => !Game.engine.collisions.point_in_polygon(exclude.get_collider.get_lines, pt)
        case None => true
      })
      .map(pt => map.unpixel(pt))
    // TODO some tiles are inside big scenery that aren't marked as non-permeable
    return RNG.rand.shuffle(candidates).take(5)
      .find(tile => map.path(cur_tile, tile, must_avoid, should_avoid).nonEmpty)
      .getOrElse(cur_tile)  // stuck :(
  }
}

class RandomPatrolTileBehavior(
  sprite: Sprite, waypoint_factory: () => List[Sprite], pause: Float
) extends PauseNPCBaseBehavior(sprite, pause) {
  override def select_new_target(): TileCoordinate = {
    val waypts = waypoint_factory()
    if (waypts.isEmpty) {
      return cur_tile
    } else {
      return map.unpixel(RNG.choose(waypts).get_collider_pos)
    }
  }
}

class BreadcrumbChaseTileBehavior(
  sprite: Sprite, waypoint_factory: () => Option[Sprite]
) extends PauseNPCBaseBehavior(sprite, 0) {
  override def select_new_target() = map.unpixel(waypoint_factory().getOrElse(sprite).get_collider_pos)
}

class TilePhysicsModule(
  lines: List[Line], sprite: Sprite
) extends MovingPhysicsModule(lines, sprite) {
  private val map = sprite.world.get_tilemap.get

  // pixels/second, each direction separately
  var speed = TilePhysicsStyles.default_npc

  def align(tile: TileCoordinate, dt: Float) {
    val at = sprite.get_collider_pos_topleft
    val goal = map.topleft_pixel(tile)

    // Make nice big jumps. This works because dt is fixed.
    val dx = goal.x - at.x
    val dy = goal.y - at.y
    sprite.speed_x = Util.smart_min(dx / dt, speed)
    sprite.speed_y = Util.smart_min(dy / dt, speed)
  }

  // TODO dont think we need to allow a lil slack, right?
  def aligned(tile: TileCoordinate) = sprite.get_collider_pos_topleft == map.topleft_pixel(tile)
}

object TilePhysicsStyles {
  val default_npc = 100
  val player_cutscene = 250
}
