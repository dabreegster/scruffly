package scruffly.components

import scruffly._
import scruffly.engine._
import scruffly.util._

case class Light(
  at: Pt, color: RGB = RGB(255, 255, 255), radius: Float = 200,
  // in counter-clockwise order or something
  cast_rays: List[Pt] = Nil
)

class Lighting(
  raw_lights: List[() => Light], ambient_light: RGB, raycast: Boolean, world: scruffly.World,
  alpha_gradient: (Int, Int) = (120, 0)
) extends Component {
  private var time = 0f
  override def update(dt_s: Float) {
    time += dt_s
  }
  override def render(gfx: DrawModule) {
    // TODO slick only
    val lights = raw_lights.map(l => l() match {
      case Light(center, color, radius, _) => {
        val jittery_radius = radius + (5 * math.sin(time * 15)).toFloat + RNG.int(-2, 2)
        val arc_points =
          if (raycast) {
            // Prune the search to the screen. Can't just use the lines on-screen, want to cast the ray
            // TOWARDS things off-screen.
            val all_objs = world.select_scenery(gfx.get_window).map(_.obj).toSet.toList
            val all_lines = all_objs.flatMap(c => c.get_collider.get_lines)

            val angles = all_lines.flatMap(_.points).map(
              pt => canonicalize(math.atan2(pt.y - center.y, pt.x - center.x))
            ).flatMap(a => List(a - .01, a, a + .01)).sorted
            // TODO go to the bottom of the storeroom where the long horizontal walls are. need some
            // intermediate values.

            angles.map(
              angle => Game.engine.collisions.raycast(center, angle, all_lines, jittery_radius)
            )
          } else {
            List(0, 45, 90, 135, 180, 225, 270, 315, 360).map(
              angle => Game.engine.collisions.raycast(center, angle, Nil, jittery_radius)
            )
          }

        Light(
          gfx.map_to_screen(center), color, jittery_radius, arc_points.map(gfx.map_to_screen).toList
        )
      }
    })

    if (!gfx.debug) {
      val mask = gfx.asInstanceOf[SlickDrawModule].make_light_mask(lights, ambient_light, alpha_gradient)
      gfx.set_translation(gfx.x_offset, gfx.y_offset)
      mask.draw(0, 0)
    } else {
      for (light <- lights) {
        for (pt <- light.at :: light.cast_rays) {
          val rad = 5f
          val half = rad / 2
          gfx.circle(rad, RGB(255, 0, 0), gfx.x_offset + pt.x - half, gfx.y_offset + pt.y - half)
        }
        gfx.set_translation(gfx.x_offset, gfx.y_offset)
        for ((pt1, pt2) <- light.cast_rays.zip(light.cast_rays.tail)) {
          gfx.polygon(List(light.at, pt1, pt2), false, true, RGB(0, 255, 0))
        }
      }
    }
  }

  private def canonicalize(rads: Double): Double = {
    val degs = math.toDegrees(rads)
    return if (degs < 0) degs + 360 else degs
  }
}
