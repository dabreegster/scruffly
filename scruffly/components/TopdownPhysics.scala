package scruffly.components

import scruffly._
import scruffly.engine._

import scala.collection.mutable

// All the parameters are supposed to be in pixels/second
case class MovementStyle(
  accel_forward: Float,
  accel_backward: Float,
  deaccel: Float,
  max_speed: Float
)

object MovementStyles {
  val default = MovementStyle(.2f, .2f, .3f, 5f)

  val player_default = MovementStyle(.5f, .5f, .9f, 4f)
  val player_slippery = MovementStyle(1f, .07f, .1f, 10f)
  val player_sticky = MovementStyle(.07f, .07f, .3f, 1.7f)

  val slow = MovementStyle(.07f, .07f, .3f, 1.7f)
  val very_slow = MovementStyle(.02f, .02f, .03f, .5f)
  val fast = MovementStyle(.6f, .6f, .9f, 15f)

  val bullet = default.copy(deaccel = 0)
  val mud_monster_bouncey = default.copy(deaccel = 0)

  def apply(style: String) = style match {
    case "slow" => slow
    case "veryslow" => very_slow
    case "bouncey" => mud_monster_bouncey
  }
}

class TopdownPhysicsModule(
  lines: List[Line], sprite: Sprite
) extends MovingPhysicsModule(lines, sprite)
{
  var mvmnt = MovementStyles.default

  private def accel_forward = mvmnt.accel_forward * (param_scale * param_scale)
  private def accel_backward = mvmnt.accel_backward * (param_scale * param_scale)
  private def deaccel = mvmnt.deaccel * (param_scale * param_scale)
  private def max_speed = mvmnt.max_speed * param_scale

  override def adjust_speed(dt: Float, move_requested_x: Boolean, move_requested_y: Boolean) {
    if (!move_requested_x) {
      if (sprite.speed_x < 0) {
        sprite.speed_x += deaccel * dt
      }
      if (sprite.speed_x > 0) {
        sprite.speed_x -= deaccel * dt
      }

      // Don't travel at slow speed forever
      if (sprite.speed_x > 0 && sprite.speed_x < deaccel * dt) {
        sprite.speed_x = 0
      }
      if (sprite.speed_x < 0 && sprite.speed_x > -deaccel * dt) {
        sprite.speed_x = 0
      }
    }
    if (!move_requested_y) {
      if (sprite.speed_y < 0) {
        sprite.speed_y += deaccel * dt
      }
      if (sprite.speed_y > 0) {
        sprite.speed_y -= deaccel * dt
      }

      // Don't travel at slow speed forever
      if (sprite.speed_y > 0 && sprite.speed_y < deaccel * dt) {
        sprite.speed_y = 0
      }
      if (sprite.speed_y < 0 && sprite.speed_y > -deaccel * dt) {
        sprite.speed_y = 0
      }
    }

    sprite.speed_x = math.min(sprite.speed_x, max_speed)
    sprite.speed_x = math.max(sprite.speed_x, -max_speed)
    sprite.speed_y = math.min(sprite.speed_y, max_speed)
    sprite.speed_y = math.max(sprite.speed_y, -max_speed)
  }

  def request_left(dt: Float) {
    val accel = if (sprite.speed_x <= 0) accel_forward else accel_backward
    sprite.speed_x -= accel * dt
  }

  def request_right(dt: Float) {
    val accel = if (sprite.speed_x >= 0) accel_forward else accel_backward
    sprite.speed_x += accel * dt
  }

  def request_up(dt: Float) {
    val accel = if (sprite.speed_y <= 0) accel_forward else accel_backward
    sprite.speed_y -= accel * dt
  }

  def request_down(dt: Float) {
    val accel = if (sprite.speed_y >= 0) accel_forward else accel_backward
    sprite.speed_y += accel * dt
  }
}

class TopdownControllerBehavior(physics: TopdownPhysicsModule)
  extends Behavior(physics.sprite) with Disableable
{
  override def update(dt: Float) {
    if (enabled) {
      if (Game.engine.keymap.keydown(Game.engine.keymap.key_left)) {
        physics.request_left(dt)
      }
      if (Game.engine.keymap.keydown(Game.engine.keymap.key_right)) {
        physics.request_right(dt)
      }
      if (Game.engine.keymap.keydown(Game.engine.keymap.key_up)) {
        physics.request_up(dt)
      }
      if (Game.engine.keymap.keydown(Game.engine.keymap.key_down)) {
        physics.request_down(dt)
      }
    }
  }
}

// Apply sticky and slippery effects to sprite
class SurfaceModifiers(world: World, asset: String) extends Component {
  private var orig: Option[MovementStyle] = None
  private var prev_zones: Set[String] = Set()

  override def update(dt: Float) {
    for (sprite <- world.find(asset)) {
      sprite.physics match {
        case physics: TopdownPhysicsModule => {
          val new_zones = sprite.current_zones.map(_.get("zone")).toSet

          if (new_zones.contains("slippery") && !prev_zones.contains("slippery")) {
            if (!orig.isDefined) {
              orig = Some(physics.mvmnt)
            }
            physics.mvmnt = MovementStyles.player_slippery
          } else if (new_zones.contains("sticky") && !prev_zones.contains("sticky")) {
            if (!orig.isDefined) {
              orig = Some(physics.mvmnt)
            }
            physics.mvmnt = MovementStyles.player_sticky
          }

          // Restore originals?
          if (orig.isDefined && !new_zones.contains("sticky") && !new_zones.contains("slippery")) {
            physics.mvmnt = orig.get
            orig = None
          }

          prev_zones = new_zones
        }
        case _ =>
      }
    }
  }
}
