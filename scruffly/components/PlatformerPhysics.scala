package scruffly.components

import scruffly._
import scruffly.engine._

class PlatformerPhysicsModule(
  lines: List[Line], sprite: Sprite
) extends MovingPhysicsModule(lines, sprite)
{
  // Can't jump upon start
  var is_jumping = true

  var accel_x_forward = 0.3f * (param_scale * param_scale)
  var accel_x_backward = 0.3f * (param_scale * param_scale)
  var deaccel_x = 0.9f * (param_scale * param_scale)
  var max_speed_x = 7.5f * param_scale
  var max_speed_y = 20f * param_scale
  var jump_start_speed = 20f * param_scale
  var accel_gravity = .73f * (param_scale * param_scale) //.3

  override def adjust_speed(dt: Float, move_requested_x: Boolean, move_requested_y: Boolean) {
    sprite.speed_y += accel_gravity * dt

    if (!move_requested_x) {
      if (sprite.speed_x < 0) {
        sprite.speed_x += deaccel_x * dt
      }
      if (sprite.speed_x > 0) {
        sprite.speed_x -= deaccel_x * dt
      }

      // Don't travel at slow speed forever
      if (sprite.speed_x > 0 && sprite.speed_x < deaccel_x * dt) {
        sprite.speed_x = 0
      }
      if (sprite.speed_x < 0 && sprite.speed_x > -deaccel_x * dt) {
        sprite.speed_x = 0
      }
    }

    sprite.speed_x = math.min(sprite.speed_x, max_speed_x)
    sprite.speed_x = math.max(sprite.speed_x, -max_speed_x)
    sprite.speed_y = math.max(sprite.speed_y, -max_speed_y)
  }

  /*override def handle_contact(top: Boolean, bottom: Boolean, left: Boolean, right: Boolean) {
    if (bottom) {
      is_jumping = false
    }
  }*/

  def request_left(dt: Float) {
    val accel = if (sprite.speed_x <= 0) accel_x_forward else accel_x_backward
    sprite.speed_x -= accel * dt
  }

  def request_right(dt: Float) {
    val accel = if (sprite.speed_x >= 0) accel_x_forward else accel_x_backward
    sprite.speed_x += accel * dt
  }
}

class PlatformerControllerBehavior(physics: PlatformerPhysicsModule)
  extends Behavior(physics.sprite) with Disableable
{
  private val jump_key = new Keypress(Game.engine.keymap.key_jump)

  override def update(dt: Float) {
    if (enabled) {
      if (Game.engine.keymap.keydown(Game.engine.keymap.key_left)) {
        physics.request_left(dt)
      }
      if (Game.engine.keymap.keydown(Game.engine.keymap.key_right)) {
        physics.request_right(dt)
      }
      if (jump_key.pressed && !physics.is_jumping) {
        physics.is_jumping = true
        physics.sprite.speed_y = -physics.jump_start_speed
      }
    }
  }
}
