package scruffly.hotel

import scruffly._
import scruffly.engine._
import scruffly.util._
import scruffly.components._

import scala.collection.mutable

abstract class Controller(var npc_name: String, starting_level: String) extends Component with Savable {
  var current_level: String = starting_level
  var state: State = null
  var plan = new_plan
  val flags = new mutable.HashMap[String, Boolean]()

  def self = Hotel.get_level(current_level).get(npc_name)
  def new_plan = new Plan(this, Nil)

  override def save(): String = {
    val flag_dump = flags.keys.toList.sorted.map(k => k + "=" + flags(k))
    val values = (List(npc_name + ":" + state.name + ":" + current_level) ++ flag_dump)
    return values.mkString(", ")
  }

  override def load(raw: String) {
    val values = raw.split(", ")
    val header = values.head.split(":")

    npc_name = header(0)
    current_level = header(2)

    for (kv <- values.tail) {
      val flag = kv.split("=")
      flags(flag(0)) = flag(1) == "true"
    }
  }
  // TODO Bit hacky, but have to do this after the rest of everyone's state has been set, in case
  // the state ctor sets stuff up based on other state
  def load_state(raw: String) {
    val state_name = raw.split(", ").head.split(":")(1)
    state = Class
      .forName(state_name)
      .getDeclaredConstructor(this.getClass)
      .newInstance(this)
      .asInstanceOf[State]
  }

  override def update(dt: Float) {
    if (Game.engine.current_level != current_level) {
      // TODO bit hacky
      if (current_level != "nowhere") {
        self.update(dt)
      }
    }

    plan.update(dt)
    state.transition(dt)
  }

  def interact(query: String, scene: (Sprite) => List[List[CutsceneCue]]) {
    if (current_level == Game.engine.current_level && !Cutscene.in_scene) {
      for (player <- self.world.find(Hotel.current_player)) {
        if (player.can_see(npc_name)) {
          val interact = Game.engine.ez_interact(current_level)
          if (interact.is_first_action(query)) {
            self.get_child("glow").asInstanceOf[GlowSprite].glow()
          }
          if (interact.pressed(player, query)) {
            val old_behavior = self.behavior
            new Cutscene(
              current_level,
              Cutscene.pause(self),
              scene(player).flatten,
              Cutscene.unpause(self)
            ).start(None)
          }
        }
      }
    }
  }

  def talk(player_line: String, npc_line: String) {
    interact(s"Talk to $npc_name?", (player: Sprite) => List(
      Cutscene.say(player, player_line),
      Cutscene.say(self, npc_line)
    ))
  }

  def listen(lines: String*) {
    val npc_lines = lines.toList
    if (!Cutscene.in_scene) {
      self.speech.say(npc_lines.head)
    }
    if (npc_lines.size > 1) {
      interact(s"Listen to $npc_name?", (player: Sprite) => List(
        npc_lines.tail.flatMap(line => Cutscene.say(self, line))
      ))
    }
  }
}

// Anything in the constructors must be idempotent, in the sense that it'll run correctly when first
// entering the state or when loading into the state.
class State {
  def name = this.getClass.getName
  def transition(dt: Float) {}
}

// TODO probably should save in some form
class Plan(owner: Controller, tasks: List[Task]) extends Component {
  private var setup = false

  override def update(dt: Float) {
    if (tasks.nonEmpty) {
      if (!setup) {
        setup = true
        tasks.head.setup()
      }
      tasks.head.update(dt)
      if (tasks.head.done) {
        owner.plan = new Plan(owner, tasks.tail)
      }
    }
  }

  // Fluent API
  // TODO maybe more should use maybe_, think about saves
  def goto(zone: Sprite) = append(new GotoTask(owner, zone))
  def maybe_goto(layer: String) = append(new MaybeGotoTask(owner, layer))
  def wait_for(layer: String) = append(new WaitForTask(owner, layer))
  def goto_lvl(level: String) = append(
    new GotoLevelTask(owner, "levels/" + level.stripPrefix("levels/"))
  )
  def chase(target: Sprite) = append(new ChaseTask(owner, target))
  def maybe_chase(layer: String) = append(new MaybeChaseTask(owner, layer))
  def roam(region: Sprite, pause: Float, exclude: Option[Sprite] = None) = append(
    new RoamTask(owner, region, pause, exclude)
  )
  def patrol(layer: String) = append(new PatrolTask(owner, layer))
  def pause(delay: Float) = append(new PauseTask(owner, delay))
  private def append(task: Task) = new Plan(owner, tasks ++ List(task))
}

trait Task extends Component {
  def setup() {}
  def done(): Boolean
  // Can't add tasks after one of these that returns false
  def ever_done(): Boolean
}

class GotoTask(owner: Controller, region: Sprite) extends Task {
  override def setup() {
    owner.self.ez_goto(region)
  }

  override def done = owner.self.in_zone(region.get("zone"))
  override def ever_done = true
}
class MaybeGotoTask(owner: Controller, layer: String) extends Task {
  private var zone: Option[String] = None
  override def setup() {
    for (region <- Hotel.get_level(owner.current_level).find(layer)) {
      owner.self.ez_goto(region)
      zone = Some(region.get("zone"))
    }
  }

  override def done = zone match {
    case Some(z) => owner.self.in_zone(z)
    case None => true
  }
  override def ever_done = true
}
class WaitForTask(owner: Controller, layer: String) extends Task {
  override def done = Hotel.get_level(owner.current_level).find(layer).isDefined
  override def ever_done = true
}
class ChaseTask(owner: Controller, target: Sprite) extends Task {
  override def setup() {
    owner.self.ez_chase(target)
  }
  override def done = false
  override def ever_done = false
}
class MaybeChaseTask(owner: Controller, layer: String) extends Task {
  override def setup() {
    owner.self.behavior = new BreadcrumbChaseTileBehavior(
      owner.self, () => Hotel.get_level(owner.current_level).find(layer)
    )
  }
  override def done = false
  override def ever_done = false
}
class RoamTask(
  owner: Controller, region: Sprite, pause: Float, exclude: Option[Sprite]
) extends Task {
  override def setup() {
    owner.self.ez_roam(region, pause, exclude)
  }
  override def done = false
  override def ever_done = false
}
class PatrolTask(
  owner: Controller, layer: String
) extends Task {
  override def setup() {
    owner.self.ez_patrol(layer)
  }
  override def done = false
  override def ever_done = false
}
class GotoLevelTask(owner: Controller, goal_level: String) extends Task {
  // Maybe have to go through intermediate levels first
  private var current_target_level = ""
  private def choose_target() = (owner.current_level, goal_level) match {
    case ("levels/floor", "levels/garden") => "levels/atrium"
    case ("levels/garden", "levels/floor") => "levels/atrium"
    // We never involve storeroom in long trips
    case _ => goal_level
  }

  override def setup() {
    if (!done) {
      current_target_level = choose_target
      owner.self.ez_goto(Hotel.get_portal(owner.current_level, current_target_level))
    }
  }

  override def update(dt: Float) {
    val npc = owner.self
    if (npc.current_zones.exists(_.get("level") == current_target_level)) {
      npc.destroy()
      val new_npc = Instance(
        owner.npc_name, Hotel.get_portal_start_pos(current_target_level, owner.current_level, npc),
        Nil, "tile"
      ).manifest(Hotel.get_level(current_target_level), true)
      Game.engine.safely_add_component(new_npc, current_target_level)
      owner.current_level = current_target_level

      if (current_target_level != goal_level) {
        // Start the next step
        setup()
      }
    }
  }

  override def done = owner.current_level == goal_level
  override def ever_done = true
}
class PauseTask(owner: Controller, delay: Float) extends Task {
  override def setup() {
    owner.self.ez_pause(delay)
  }
  override def done = owner.self.pauser.is_paused
  override def ever_done = true
}
