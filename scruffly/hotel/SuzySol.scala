package scruffly.hotel

import scruffly._
import scruffly.engine._
import scruffly.util._
import scruffly.components._

import scala.collection.mutable

class SuzySolController() extends Controller("suzysol", "levels/atrium") {
  flags("destroyer") = false
  flags("firebug") = false
  state = new Brunch()

  class Brunch() extends State {
    override def transition(dt: Float) {
      talk(
        "Good morning, little girl! Enjoying your orange juice?",
        "Yes, mister! Ain't got no parents,\nbut I sure do have orange juice~~"
      )

      if (Hotel.javoozy.current_level == "levels/garden") {
        self.ez_say("Playtime!")
        state = new PlayGarden()
      }
    }
  }

  class PlayGarden() extends State {
    private var health = 3  // Don't bother saving

    plan = new_plan.goto_lvl("garden").patrol("shrub")

    // TODO too easy to hit, give immunity for a bit after the shaking
    // TODO share with leon
    private val hit_handlers = List(
      CollisionHandler("seed", npc_name, (hit: Collision) => {
        hit.pusher.destroy()
        hit.other.ez_shake(2)
        if (health == 3) {
          hit.other.ez_say(
            if (Hotel.javoozy.flags("drunk")) "Adults drink some STRONG juice..." else "SUGAR SUGAR SUGAR"
          )
        }
        health -= 1
        Ghost()
      })
    )
    Garden.world.add_collisions(hit_handlers)

    private val shrubs = Garden.world.all_sprites.filter(_.layer == "shrub")
    // Not important enough to save/load
    private val visited_shrubs = new mutable.HashSet[Sprite]()

    override def transition(dt: Float) {
      if (current_level == "levels/garden" && !Cutscene.in_scene) {
        for (shrub <- shrubs if !visited_shrubs.contains(shrub) && self.nearby(shrub, 100)) {
          visited_shrubs += shrub
          self.ez_pause(5)
          self.ez_say("Are you my mommy?")
        }
      }

      if (!Cutscene.in_scene) {
        if ((health <= 0 && Hotel.javoozy.flags("drunk")) || Schedule.suzy_sleepy_time.after) {
          Garden.world.del_collisions(hit_handlers)
          new Cutscene("levels/garden",
            Cutscene.pause(self),
            Cutscene.say_focus(self, "I'm all sleepy..."),
            Cutscene.unpause(self),
            List(new Pause(2)),
            Cutscene.act(() => {
              state = new TryToSleep()
            })
          ).start(Some(self))
        } else if (health <= 0 && Hotel.javoozy.flags("caffeinated")) {
          // Supercharge the plants
          new Cutscene("levels/garden",
            Cutscene.follow(self),
            Cutscene.say(self, "I control the plants now!"),
            Cutscene.act(() => {
              for (shrub <- shrubs) {
                shrub.ez_shake(2)
              }
            }),
            List(new Pause(4)),
            Cutscene.act(() => {
              state = new ConductChaos()
            })
          ).start(Some(self))
        }
      }
    }
  }

  class TryToSleep() extends State {
    plan = new_plan.goto_lvl("floor")

    override def transition(dt: Float) {
      if (current_level == "levels/floor" && !Cutscene.in_scene) {
        val maid = Floor.world.get("maid")
        new Cutscene("levels/floor",
          Cutscene.say_focus(self, "Ehh... am I dreaming?"),
          // TODO make maid kinda dance?
          Cutscene.follow(maid),
          Cutscene.say(maid, "Dancing with~"),
          Cutscene.say(maid, "Dancing with~\nmy linen..."),
          Cutscene.say(maid, "Dancing with~\nmy linen LOVER"),
          Cutscene.say(maid, "So silky smooth~"),
          Cutscene.say(maid, "His skin ripples when he caresses me~"),
          Cutscene.say(maid, "Creepy things~"),
          Cutscene.say(maid, "Oh creepy things~"),
          Cutscene.unfollow(),
          Cutscene.say_focus(self, "My mother's cheating on daddy...\nwith that robe man!") ++
          Cutscene.act(() => {
            self.ez_shake(3)
            state = new LinenDestroyer()
          }) ++
          List(new Pause(3))
        ).start(Some(self))
      }
    }
  }

  class ConductChaos() extends State {
    override def transition(dt: Float) {
      for (guest <- victims if guest.in_zone("plant_plate")) {
        // Sucks to suck.
        Hotel.get_generic(guest).eaten()
        self.ez_say("Mwuhahaha, caught another one!")
      }

      if (Hotel.game.flags("floor_open")) {
        state = new TellJokes()
      }
    }

    private def victims = Garden.world.all_sprites.filter(g => g.layer.startsWith("guest"))
  }

  class LinenDestroyer() extends State {
    flags("destroyer") = true
    Game.assets.apply_effect("fire", Floor.cart)
    self.behavior = new RandomPatrolTileBehavior(self, victims _, 10)

    override def transition(dt: Float) {
      for (guest <- victims) {
        if (self.nearby(guest, 100)) {
          if (guest.ez_on_fire) {
            if (!self.speech.is_speaking) {
              self.ez_say("Is it hot in here or what?")
            }
          } else {
            // TODO cutscene for the first one?
            self.ez_say("Burn, faceless non-parent")
            Game.assets.apply_effect("fire", guest)
          }
        }
      }

      if (Schedule.piano_time.after) {
        state = new ListenPiano()
      }
    }

    // Include the cart so she doesn't stop by the elevator when nobody's around and snipe guests
    // coming in
    private def victims = Floor.world.all_sprites.filter(
      g => g.layer.startsWith("guest") && !g.layer.endsWith("door")
    ) ++ List(Floor.world.get("cart"))
  }

  class TellJokes() extends State {
    plan = new_plan.goto_lvl("floor").maybe_chase("maid")

    // Don't save
    private val visited_doors = new mutable.HashSet[Sprite]()

    override def transition(dt: Float) {
      for (door <- self.current_zones.find(z => z.data("zone") == "room" && !visited_doors.contains(z))) {
        visited_doors += door
        self.ez_say("Have a joke for " + door.data("text"))
        self.ez_pause(3)
      }

      if (Schedule.jokes_to_firebug_time.after) {
        state = new Firebug()
      }
    }
  }

  class Firebug() extends State {
    flags("firebug") = true

    plan = new_plan.roam(Floor.floor, 15)

    // TODO copies LinenDestroyer stuff a bit
    override def transition(dt: Float) {
      for (guest <- victims) {
        if (self.nearby(guest, 100)) {
          if (guest.ez_on_fire) {
            if (!self.speech.is_speaking) {
              self.ez_say("Sorry!")
            }
          } else {
            // TODO do it less often?
            self.ez_say("Heehee, fire's fun!")
            Game.assets.apply_effect("fire", guest)
          }
        }
      }

      if (Schedule.piano_time.after) {
        state = new ListenPiano()
      }
    }

    private def victims = Floor.world.all_sprites.filter(
      g => g.layer.startsWith("guest") && !g.layer.endsWith("door")
    )
  }

  class ListenPiano() extends State {
    plan = new_plan.goto_lvl("atrium").goto(Atrium.piano)

    override def transition(dt: Float) {
      if (Schedule.story_time.after) {
        if (flags("destroyer")) {
          // TODO amelia scared
          listen("RAWR!")
        } else {
          self.ez_say("Let me tell you this joke...")
          state = new ListenStorytime()
        }
      }
    }
  }

  class ListenStorytime() extends State {
    plan = new_plan.chase(Atrium.world.get("amelia"))
  }
}
