package scruffly.hotel

import scruffly._
import scruffly.engine._
import scruffly.util._
import scruffly.components._
import scruffly.tools.RunLevel

import java.io.File

object Common extends GeometryFactory {
  def setup_all(load: Option[String]) {
    for (lvl <- List("levels/atrium", "levels/floor", "levels/garden", "levels/storeroom")) {
      assert(!Game.engine.has_state(lvl))
      // TODO dont use runlevel
      RunLevel.setup(lvl, load.map(dir => dir + "/" + lvl).toList)
      Game.engine.deactivate(lvl)
      //Hotel.get_level(lvl).check_collisions()
    }
    if (!load.isDefined) {
      Game.engine.activate("levels/atrium")
    }

    Hotel.inventory = new HotelInventory()
    Hotel.game = new GameController()
    Hotel.aquastazia = new AquastaziaController()
    Hotel.leon = new LeonController()
    Hotel.javoozy = new JavoozyController()
    Hotel.amelia = new AmeliaController()
    Hotel.suzysol = new SuzySolController()
    Hotel.cat = new CatController()
    Hotel.generic_guests =
      (Range(1, 6).map(n => new GuestController("guest" + n, "levels/atrium")) ++
       Range(6, 11).map(n => new GuestController("guest" + n, "levels/garden"))
      ).toList

    for (c <- Hotel.all_controllers) {
      Game.engine.add_component(c, "game")
    }
    Game.engine.add_component(new HotelWizard(), "game")
    Game.engine.add_component(new HotelLevelSwitcher(), "game")
    Game.engine.add_component(Game.errors, "game")

    for (fn <- load) {
      val last_level = Hotel.load(fn)
      Game.engine.activate(last_level)
    }
  }

  def make_assets(): AssetManager = {
    val assets = new AssetManager() {
      // for the storeroom shelves and generic guests
      override def dynamic_asset(name: String, effects: List[String], use_effects: Boolean): Asset = {
        if (name.startsWith("guest")) {
          return this.apply("guest", effects, use_effects).copy(name = name, layer = name)
        } else {
          val asset = super.dynamic_asset(name, effects, use_effects)
          if (effects.contains("zone=food")) {
            val item = effects.find(fx => fx.startsWith("item=")).get.split("=").last
            val fn = "hotel/food/" + item + ".png"
            val use_fn = if (new File("assets/" + fn).exists) fn else "hotel/food/generic.png"
            return asset.copy(
              renderer = TextureRenderer(asset.topdown_lines.flatMap(_.points), use_fn)
            )
          }
          return asset
        }
      }
    }
    register_characters()
    register_misc()
    register_effects()

    return assets
  }

  private def register_characters() {
    val assets = Game.assets
    assets.add(Asset(
      "butler", "butler", ImageRenderer("hotel/chars/butler.png"),
      hitbox((0, 48), (133, 150)), Nil
    ))

    assets.add(Asset(
      "maid", "maid", ImageRenderer("hotel/chars/maid.png"),
      hitbox((0, 50), (130, 150)), Nil
    ))

    assets.add(Asset(
      "guest", "guest", SpritesheetRenderer(
        new SlickFileSpriteSheet(
          Game.engine.assets.asInstanceOf[SlickAssetsModule], "hotel/chars/guest.png", 128, 128,
          List("270", "225", "315", "180", "90", "135", "45", "0")
        ), "270", .4f
      ), hitbox((50, 78), (85, 105)), Nil
    ))

    assets.add(Asset(
      "suzysol", "suzysol", ImageRenderer("hotel/chars/suzysol.png"),
      hitbox((32, 52), (135, 146)), Nil
    ))
    assets.add(Asset(
      "amelia", "amelia", ImageRenderer("hotel/chars/amelia.png"),
      hitbox((8, 57), (130, 150)), Nil
    ))
    assets.add(Asset(
      "cat", "cat", ImageRenderer("hotel/chars/cat.png"),
      hitbox((30, 70), (30, 50)), Nil
    ))
    assets.add(Asset(
      "javoozy", "javoozy", ImageRenderer("hotel/chars/javoozy.png"),
      hitbox((0, 48), (130, 150)), Nil
    ))
    assets.add(Asset(
      "aquastazia", "aquastazia", ImageRenderer("hotel/chars/aquastazia.png"),
      hitbox((10, 60), (130, 150)), Nil
    ))
    assets.add(Asset(
      "aquastazia_swimming", "aquastazia_swimming", ImageRenderer("hotel/chars/aquastazia_swimming.png"),
      hitbox((0, 48), (0, 23)), Nil
    ))
    assets.add(Asset(
      "leon", "leon", ImageRenderer("hotel/chars/leon.png"),
      hitbox((0, 49), (130, 150)), Nil
    ))
    assets.add(Asset(
      "leon_swimming", "leon_swimming", ImageRenderer("hotel/chars/leon_swimming.png"),
      hitbox((0, 48), (0, 23)), Nil
    ))
  }

  private def register_misc() {
    val assets = Game.assets
    // Hitbox is overridden by LineOfSight
    assets.add(Asset(
      "sightzone", "sightzone", NullRenderer(), hitbox((0, 1), (0, 1)), Nil
    ))
    assets.add(Asset(
      "waypt", "waypt", ImageRenderer("hotel/waypt.png"),
      hitbox((0, 100), (0, 100)), Nil
    ))

    assets.add(Asset(
      "bar", "scenery", ImageRenderer("hotel/atrium/bar.png"),
      points_to_closed_polygon(List(
        Pt(0, 0), Pt(220, 0), Pt(220, 239), Pt(85, 239), Pt(0, 86)
      )), Nil
    ))
    assets.add(Asset(
      "desk_left", "scenery", ImageRenderer("hotel/atrium/desk_left.png"),
      points_to_closed_polygon(List(
        Pt(0, 30), Pt(37, 95), Pt(315, 95), Pt(296, 30)
      )), Nil
    ))
    assets.add(Asset(
      "desk_right", "scenery", ImageRenderer("hotel/atrium/desk_right.png"),
      points_to_closed_polygon(List(
        Pt(314, 30), Pt(275, 95), Pt(0, 95), Pt(20, 30)
      )), Nil
    ))
    // TODO misnomer
    assets.add(Asset(
      "fishbait", "fishbait", ImageRenderer("hotel/fishbait_icon.png"),
      hitbox((0, 200), (0, 200)), Nil
    ))
    assets.add(Asset(
      "atrium_layout", "scenery", ImageRenderer("hotel/atrium/atrium_layout.png"),
      points_to_closed_polygon(List(
        Pt(45, 1192), Pt(343, 1192), Pt(648, 669), Pt(830, 673), Pt(830, 745),
        Pt(854, 748), Pt(870, 501), Pt(576, 501), Pt(301, 993), Pt(49, 991),
        Pt(448, 323), Pt(1495, 318), Pt(1957, 988), Pt(1741, 995),
        Pt(1646, 765), Pt(1558, 777), Pt(1381, 506), Pt(1094, 497), Pt(1108, 750),
        Pt(1132, 750), Pt(1128, 670), Pt(1315, 674), Pt(1603, 1174), Pt(1986, 1177),
        Pt(1267, 2414), Pt(749, 2415)
      )), Nil
    ))
    assets.add(Asset(
      "atrium_railing", "railing", ImageRenderer("hotel/atrium/atrium_railing.png"),
      List(Line(Pt(0, 0), Pt(1, 1))), Nil
    ))
    val fountain_pts = ellipse_arc(200, 100, 0, 360, segments = 15)
    assets.add(Asset(
      "fountain", "scenery", PolygonRenderer(fountain_pts, true, true, RGB(0, 0, 255)),
      points_to_closed_polygon(fountain_pts), Nil
    ))
    assets.add(Asset(
      "armchair1", "armchair", ImageRenderer("hotel/atrium/armchair1.png"),
      isobox(List(Pt(70, 25), Pt(140, 80), Pt(70, 130), Pt(0, 80))), Nil
    ))
    assets.add(Asset(
      "armchair2", "armchair", ImageRenderer("hotel/atrium/armchair2.png"),
      isobox(List(Pt(75, 25), Pt(145, 75), Pt(75, 130), Pt(0, 80))), Nil
    ))
    assets.add(Asset(
      "armchair3", "armchair", ImageRenderer("hotel/atrium/armchair3.png"),
      isobox(List(Pt(60, 50), Pt(135, 120), Pt(70, 170), Pt(0, 115))), Nil
    ))
    assets.add(Asset(
      "armchair4", "armchair", ImageRenderer("hotel/atrium/armchair4.png"),
      isobox(List(Pt(5, 120), Pt(80, 60), Pt(80, 170), Pt(120, 120))), Nil
    ))

    assets.add(Asset(
      "hall_layout", "scenery", ImageRenderer("hotel/floor/hall_layout.png"),
      points_to_closed_polygon(List(
      // outer perimeter
        Pt(153, 381), Pt(3840, 381),     // top
        Pt(2790, 2189), Pt(2790, 2406), // right
        Pt(3840, 4204), Pt(153, 4204),  // bottom
        Pt(1188, 2406), Pt(1188, 2189)  // left
      )) ++ points_to_closed_polygon(List(
      // upper bit
        Pt(484, 592), Pt(3492, 592), Pt(2573, 2228), Pt(1402, 2228)
      )) ++ points_to_closed_polygon(List(
      // lower bit
        Pt(1392, 2392), Pt(2582, 2392), Pt(3496, 3989), Pt(484, 3989)
      )), Nil
    ))
    assets.add(Asset(
      "cart", "cart", ImageRenderer("hotel/floor/cart.png"),
      hitbox((0, 150), (0, 130)), Nil
    ))
    assets.add(Asset(
      "tiara", "tiara", ImageRenderer("hotel/tiara.png"),
      hitbox((0, 205), (0, 162)), Nil
    ))

    assets.add(Asset(
      "garden_layout", "scenery", ImageRenderer("hotel/garden/garden_layout.png"),
      points_to_closed_polygon(List(
      // outer perimeter
        Pt(300, 564), Pt(611, 471), Pt(1028, 505), Pt(1301, 692), Pt(1420, 862), Pt(1492, 868),
        Pt(1492, 1007), Pt(1426, 1007), Pt(1164, 1320), Pt(773, 1388), Pt(546, 1380), Pt(273, 1319),
        Pt(7, 1011), Pt(7, 868), Pt(223, 589)
      )), Nil
    ))
    assets.add(Asset(
      "pool", "scenery", NullRenderer(),
      points_to_closed_polygon(ellipse_arc(230, 120, 0, 360)), Nil
    ))
    assets.add(Asset(
      "booze_seed", "seed", ImageRenderer("hotel/garden/booze_seed.png"),
      hitbox((0, 30), (0, 30)), Nil
    ))
    assets.add(Asset(
      "coffee_seed", "seed", ImageRenderer("hotel/garden/coffee_seed.png"),
      hitbox((0, 30), (0, 30)), Nil
    ))
    assets.add(Asset(
      "spray_bullet", "spray_bullet", ImageRenderer("hotel/water_spray.png"),
      hitbox((0, 100), (0, 100)), Nil
    ))
    assets.add(Asset(
      "fire", "fire", ImageRenderer("hotel/fire.png"),
      hitbox((0, 100), (0, 142)), Nil
    ))

    for (n <- Range(0, 8 + 1)) {
      assets.add(Asset(
        "shrub" + n, "shrub", ImageRenderer("hotel/garden/shrub" + n + ".png"),
        hitbox((0, 70), (0, 40)), Nil
      ))
    }

    assets.add(Asset(
      "spider", "spider", SpritesheetRenderer(
        new SlickFileSpriteSheet(
          Game.engine.assets.asInstanceOf[SlickAssetsModule], "hotel/spider.png", 70, 70,
          List("270", "180", "0", "90")
        ), "270", .1f
      ), hitbox((0, 70), (0, 70)), Nil
    ))

    assets.add(Asset(
      "mud", "mud", ImageRenderer("hotel/mud.png"),
      hitbox((30, 70), (30, 70)), Nil
    ))
    assets.add(Asset(
      "muk", "muk", ImageRenderer("hotel/muk.png"),
      hitbox((25, 160), (90, 130)), Nil
    ))
    assets.add(Asset(
      "chandelier", "chandelier", ImageRenderer("hotel/atrium/chandelier.png"),
      hitbox((0, 150), (0, 70)), Nil
    ))
    assets.add(Asset(
      "crashed_chandelier", "crashed_chandelier", ImageRenderer("hotel/atrium/crashed_chandelier.png"),
      hitbox((0, 150), (0, 150)), Nil
    ))
    assets.add(Asset(
      "potted_plant", "potted_plant", ImageRenderer("hotel/atrium/potted_plant.png"),
      hitbox((0, 100), (150, 186)), Nil
    ))
    assets.add(Asset(
      "carpet", "carpet", CarpetRenderer(),
      hitbox((0, 1), (0, 1)), Nil
    ))
    assets.add(Asset(
      "wood", "wood", ImageRenderer("hotel/wood.jpg"),
      hitbox((0, 150), (0, 90)), Nil
    ))
    assets.add(Asset("door", "zone", DoorRenderer(), hitbox((0, 120), (0, 50)), Nil))

    assets.add(Asset(
      "cellar_layout", "scenery", ImageRenderer("hotel/storeroom/cellar_layout.png"),
      points_to_closed_polygon(List(
        Pt(0, 600), Pt(414, 297), Pt(1240, 297), Pt(1700, 635), Pt(1700, 1474), Pt(1250, 1802),
        Pt(406, 1804), Pt(0, 1510)
      )), Nil
    ))
    assets.add(Asset(
      "shelf1", "scenery", ImageRenderer("hotel/storeroom/shelf1.png"),
      hitbox((0, 141), (83, 127)), Nil
    ))
    assets.add(Asset(
      "shelf3", "scenery", ImageRenderer("hotel/storeroom/shelf3.png"),
      points_to_closed_polygon(List(
        Pt(1, 152), Pt(77, 92), Pt(125, 106), Pt(46, 165)
      )), Nil
    ))
    assets.add(Asset(
      "shelf4", "scenery", ImageRenderer("hotel/storeroom/shelf4.png"),
      points_to_closed_polygon(List(
        Pt(1, 109), Pt(80, 165), Pt(125, 153), Pt(44, 90)
      )), Nil
    ))
  }

  private def register_effects() {
    val assets = Game.assets
    assets.add_effect("player", (sprite: Sprite) => {
      sprite.physics match {
        case p: PlatformerPhysicsModule =>
          sprite.behavior = new PlatformerControllerBehavior(p)
        case t: TopdownPhysicsModule => {
          sprite.behavior = new TopdownControllerBehavior(t)
          t.mvmnt = MovementStyles.player_default
        }
      }
      sprite.child_components += (("camera", new FollowSpriteCamera(sprite)))
    })
    assets.add_effect("bullet", (sprite: Sprite, param: String) => {
      // Don't let the bullet slow down
      sprite.physics.asInstanceOf[TopdownPhysicsModule].mvmnt = MovementStyles.bullet
      // Reap this bullet
      Game.engine.safely_add_component(new Component() {
        override def update(dt_s: Float) {
          if (sprite.get_pos.dist(sprite.instance.start) > param.toFloat) {
            sprite.destroy()
            Game.engine.del_component(this, sprite.world.level)
          }
        }
      }, sprite.world.level)
    })
    assets.add_effect("waypt", (sprite: Sprite, param: String) => {
      sprite.data("waypt") = param
      sprite.renderer = NullRenderer()
    })
    assets.add_effect("health", (sprite: Sprite, param: String) => {
      sprite.child_components += (("health", new Health(sprite, param.toInt)))
    })
    assets.add_effect("shooter", (sprite: Sprite) => {
      sprite.child_components += (("shoot", new ShootControls(sprite)))
    })
    assets.add_effect("mvmnt", (sprite: Sprite, param: String) => {
      sprite.physics.asInstanceOf[TopdownPhysicsModule].mvmnt = MovementStyles(param)
    })
    assets.add_effect("fire", (sprite: Sprite) => {
      // TODO multiple asset names will break when loading. will need IDs.
      val id = "flames_" + sprite.name
      val loaded_flames = sprite.world.all_sprites.find(x => x.get("id") == id)
      val flames = loaded_flames.getOrElse(
        Instance("fire", Pt(0, 0), List("id=" + id), "topdown").manifest(sprite.world, true)
      )
      val fire = new Fire(sprite, flames)
      // Just add the fire, not the flames -- it'll take care of adding the flames if needed
      Game.engine.safely_add_component(fire, sprite.world.level)
      sprite.fated_others ++= List(flames, fire)

      // So that things lit on fire later in life retain that when saved...
      if (!sprite.instance.effects.contains("fire")) {
        sprite.instance = sprite.instance.copy(effects = sprite.instance.effects ++ List("fire"))
      }
    })
    assets.add_effect("sight", (sprite: Sprite) => {
      // TODO multiple asset names will break when loading. will need IDs.
      val id = "sight_" + sprite.name
      val loaded_zone = sprite.world.all_sprites.find(x => x.get("layer") == id)
      val zone = loaded_zone.getOrElse(
        Instance("sightzone", Pt(0, 0), List("zone=sight", "layer=" + id), "topdown").manifest(
          sprite.world, true
        )
      )
      val los = new LineOfSight(sprite, zone)
      zone.owner = sprite
      // Just add the LOS, not the zone -- it'll take care of adding the zone if needed
      Game.engine.safely_add_component(los, sprite.world.level)
      sprite.fated_others ++= List(zone, los)
    })
    assets.add_effect("texture", (sprite: Sprite, param: String) => {
      sprite.renderer = TextureRenderer(
        sprite.physics.get_collision_lines.flatMap(_.points), param
      )
    })

    assets.add_effect("water", (scenery: Sprite) => {
      scenery.renderer = new Renderer() {
        private val water = Game.engine.all_components(scenery.world.level).find(c => c match {
          case w: WaterEffect => true
          case _ => false
        }).get.asInstanceOf[WaterEffect]

        override def draw(obj: Sprite) {
          water.texture(obj.get_collider)
        }
        override def get_dims() = throw new UnsupportedOperationException()
        override def cloneme = throw new UnsupportedOperationException()
      }
    })
  }
}
