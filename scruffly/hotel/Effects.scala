package scruffly.hotel

import scruffly._
import scruffly.engine._
import scruffly.util._
import scruffly.components._

object Mud {
  val fx = List("health=2")

  def spread(hit: Collision): CollisionResponse = {
    if (!hit.pusher.has_child("mud")) {
      hit.pusher.child_components += (("mud", new MudSpread(hit.pusher)))
    }
    return Ghost()
  }
}

class MudSpread(carrier: Sprite) extends Component {
  private val mud_dist = 100

  override def update(dt: Float) {
    // Don't be nearby any other mud
    if (carrier.world.get_layer("mud", carrier.get_collider_pos.around(mud_dist)).isEmpty) {
      Game.engine.safely_add_component(
        Instance("mud", carrier.get_pos, Mud.fx, "topdown").manifest(carrier.world, true),
        carrier.world.level
      )
      carrier.remove_child("mud")
    }

    // TODO have a self-destruct that only triggers once we're away from the new mud too
  }
}
