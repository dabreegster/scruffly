package scruffly.hotel

import scruffly._
import scruffly.engine._
import scruffly.util._
import scruffly.components._

object Garden extends GeometryFactory {
  private val level = "levels/garden"
  val tile_size = Hotel.char_hitbox

  // Shortcuts
  lazy val world = Hotel.get_level(level)
  lazy val to_atrium = Hotel.garden.all_sprites.find(_.get("level") == "levels/atrium").get
  lazy val ground = Hotel.garden.get_zone("ground")
  lazy val pool = Hotel.garden.get_zone("pool")
  lazy val pool_entrance = Hotel.garden.get_zone("pool_entrance")
  lazy val march_start = Hotel.garden.get_zone("start_march")

  def add(c: Component) {
    Game.engine.safely_add_component(c, level)
  }
  def del(c: Component) {
    Game.engine.del_component(c, level)
  }

  def setup(load: String) {
    // Must be added super early so we can apply the water effect to the scenery
    add(new WaterEffect())

    val world = WorldSpec.load(load).make_tile_world(
      // TODO shrub would be "must", except NPCs patrol them
      tile_size, Set(), Set("shrub")
    )
    Hotel.garden = world

    // Components
    world.all_sprites.foreach(s => add(s))
    add(world.get_tilemap.get)
    add(new InteractButton())

    // Collisions
    world.handle_collisions(
      Hotel.all_chars,
      Set("scenery"),
      CollisionHandlers.normal_hit _
    )
    world.handle_collisions(
      Hotel.all_chars,
      Set("zone", "fishbait"),
      CollisionHandlers.ghost _
    )
    world.handle_collisions(Hotel.player_sights, Hotel.seeables, CollisionHandlers.ghost _)
    world.handle_collisions(Hotel.seeables, Hotel.player_sights, CollisionHandlers.ghost _)
    // N rolls up his sleeves...
    world.handle_collisions("butler", Hotel.all_npcs, (hit: Collision) => {
      hit.pusher.ez_say("Excuse me, you need to be over here...")
      CollisionHandlers.push_with_key(hit)
    })
    world.handle_collisions(Set("leon_swimming", "aquastazia_swimming"), "scenery", CollisionHandlers.bounce _)

    world.handle_collision("seed", "butler", (hit: Collision) => {
      hit.pusher.destroy()
      val bodypart = RNG.choose((Set(
        "leg", "nose", "esophagus", "elbow", "little toe", "pristinely waxed end of my moustache",
        "tuxedo", "bald head, unprotected by the likes of a top hat", "dignity"
      ) -- Set(hit.other.speech.text_now).flatten).toList)
      hit.other.ez_say(s"Ow! My $bodypart!")
      hit.other.ez_pause(1)
      Ghost()
    })
    world.handle_collisions("seed", Hotel.all_generics, (hit: Collision) => {
      hit.pusher.destroy()
      hit.other.ez_say("Ouch! What in tarnation")
      Ghost()
    })
  }
}

class PlantShooter(
  owner: Sprite, seed: String, target_factory: () => List[Sprite]
) extends Component with GeometryFactory {
  private val cooldown_time = 3f  // Can only shoot this often
  private val wiggle_time = 0.4f
  private val bullet_speed = 500
  private val wiggle_amount = 10

  // After cooldown, enter wiggle phase, then shoot
  private var cooldown = 0f
  private var wiggle = 0f
  private var wiggle_counter = 0

  override def update(dt: Float) {
    cooldown = math.max(0, cooldown - dt)

    if (cooldown == 0) {
      // Only wiggle/shoot if somebody's in sight
      choose_target match {
        case Some(target) => {
          wiggle = math.max(0, wiggle - dt)
          if (wiggle == 0) {
            val angle = owner.data.get("angle") match {
              case Some(a) => a.toFloat
              case None => math.toDegrees(math.atan2(
                // invert y...
                owner.get_collider_pos.y - target.get_collider_pos.y,
                target.get_collider_pos.x - owner.get_collider_pos.x
              ))
            }
            shoot(angle)
          } else {
            // Spazz!
            wiggle_counter += 1
            if (wiggle_counter % 8 < 4) {
              owner.position_x -= wiggle_amount
            } else {
              owner.position_x += wiggle_amount
            }
          }
        }
        case None => wiggle = wiggle_time
      }
    }
  }

  private def shoot(angle: Double) {
    cooldown = cooldown_time
    wiggle = wiggle_time

    val bullet = Instance(seed, owner.get_pos, List("bullet=300"), "topdown").manifest(
      owner.world, true
    )
    Garden.add(bullet)
    bullet.speed_x = (bullet_speed * math.cos(math.toRadians(angle))).toFloat
    bullet.speed_y = -(bullet_speed * math.sin(math.toRadians(angle))).toFloat
  }

  private def choose_target = target_factory().find(x => owner.nearby(x))
}
