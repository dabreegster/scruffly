package scruffly.hotel

import scruffly._
import scruffly.engine._
import scruffly.util._
import scruffly.components._

class AmeliaController() extends Controller("amelia", "levels/atrium") {
  flags("played_piano") = false
  state = new Brunch()

  class Brunch() extends State {
    override def transition(dt: Float) {
      talk(
        "And how's your cat doing this morning, Ms. Amelia?",
        "Nice and behaved as always"
      )

      if (!Cutscene.in_scene) {
        for (bait <- Hotel.get_current_level.find("fishbait")) {
          val butler = Hotel.get_player
          new Cutscene("levels/atrium",
            Cutscene.say_focus(self, "Hey, that might attract my poor snookums!"),
            Cutscene.say(self, "How dare you try to sow the seeds of chaos!"),
            Cutscene.say_focus(butler, "Sorry, ma'am"),
            Cutscene.act(() => {
              bait.destroy()
            })
          ).start(None)
        }
      }

      if (Hotel.aquastazia.flags("crowd_drawn")) {
        state = new ChaseCat()
      }
    }
  }

  class ChaseCat() extends State {
    private var chasing_level = Hotel.cat.current_level
    plan = new_plan.goto_lvl(chasing_level).maybe_chase("cat")

    override def transition(dt: Float) {
      if (chasing_level != Hotel.cat.current_level) {
        chasing_level = Hotel.cat.current_level
        plan = new_plan.goto_lvl(chasing_level).maybe_chase("cat")
      }

      for (cat <- Hotel.get_level(current_level).find("cat")) {
        if (Hotel.cat.flags("free") && self.nearby(cat, 200) && !self.speech.is_speaking) {
          self.ez_say(
            RNG.choose((Set(
              "Almost got you, you scoundrel!",
              "Why are my arms so short?!",
              "Get back here!",
              "Boy you'd really think I'd have\na leash for this problem",
              "This reminds me when we were on that safari...\nExcept now I'M the cheetah!"
            ) -- Set(self.speech.text_now).flatten).toList)
          )
        }

        if (!Hotel.cat.flags("free") && self.nearby(cat, 200) && !Cutscene.in_scene) {
          new Cutscene("levels/atrium",
            Cutscene.follow(self),
            Cutscene.say(
              self, "Oh thank you sir,\nmy poor snookums can't survive on his own!"
            ),
            Cutscene.act(() => {
              self.ez_goto(Atrium.bathe_fountain)
              cat.ez_goto(Atrium.bathe_fountain)
            }),
            Cutscene.say_async(self, "Time for a bath!"),
            List(new PauseUntil(() => self.in_zone("bathe_fountain") && cat.in_zone("bathe_fountain"))),
            Cutscene.act(() => {
              state = new BatheCat()
              Hotel.cat.bath_time()
            })
          ).start(Some(self))
        }
      }

      if (Schedule.piano_time.after) {
        self.ez_say("Bah, I need a break...")
        state = new PlayPiano()
      }
    }
  }

  class BatheCat() extends State {
    override def transition(dt: Float) {
      if (Schedule.piano_time.after) {
        listen("Bath time~", "Gonna scrub you punk cat", "Clean up all that could've been...")
        state = new PlayPiano()
      }
    }
  }

  class PlayPiano() extends State {
    plan = new_plan.goto_lvl("atrium").goto(Atrium.piano)

    override def transition(dt: Float) {
      if (!flags("played_piano") && self.in_zone("piano") && !Cutscene.in_scene) {
        // Wait for others
        val aqua_ready = Atrium.world.get("aquastazia").in_zone("piano") || !Hotel.aquastazia.piano_bound
        val doc_ready = Atrium.world.get("javoozy").in_zone("piano")
        val suzy_ready = Atrium.world.find("suzysol").map(_.in_zone("piano")).getOrElse(false)

        if (aqua_ready && doc_ready && suzy_ready) {
          flags("played_piano") = true
          // TODO a little more flash.
          if (Hotel.cat.flags("free")) {
            new Cutscene("levels/atrium",
              Cutscene.say_focus(self, "Oh, I'm terrible at this..."),
              Atrium.world.find("suzysol").map(
                suzy => Cutscene.say_focus(suzy, "I'm still the same!")
              ).getOrElse(Nil)
            ).start(Some(self))
          } else {
            new Cutscene("levels/atrium",
              Cutscene.say_focus(self, "Piano cat is adorable!"),
              Atrium.world.find("suzysol").map(suzy => {
                if (Hotel.suzysol.flags("destroyer")) {
                  Hotel.suzysol.flags("destroyer") = false
                  Hotel.suzysol.flags("firebug") = true
                  Cutscene.say_focus(suzy, "My heart is warmed by the cutes! Less fire")
                } else {
                  Hotel.suzysol.flags("firebug") = false
                  Cutscene.say_focus(suzy, "I'm cute and fireless again!")
                }
              }).getOrElse(Nil)
            ).start(Some(self))
          }
        } else {
          listen("I just get this odd feeling...", "... that I should warm up a little more")
        }
      }

      if (Schedule.amelia_faint_time.after && Hotel.cat.flags("free")) {
        state = new Rest()
      }
      if (Schedule.need_tiara_time.after) {
        state = new Awake()
      }
    }
  }

  class Rest() extends State {
    // Stop moving
    self.behavior = new NullBehavior(self)

    override def transition(dt: Float) {
      listen("Zzz...")

      if (Schedule.need_tiara_time.after) {
        state = new Awake()
      }
    }
  }

  def trapped_by_chandelier() {
    state = new Trapped()
  }
  def rescued() {
    state = new Awake()
  }
  class Trapped() extends State {
    self.warp(Atrium.world.get("crashed_chandelier").get_collider_pos)

    override def transition(dt: Float) {
      listen(
        "Help!",
        "I'm petting my cat,\nwhy is it so metallic?"
      )
    }
  }

  class Awake() extends State {
    override def transition(dt: Float) {
      listen("Context-dependent chatter...")

      if (Schedule.story_time.after) {
        if (!Hotel.suzysol.flags("destroyer")) {
          state = new Storytime()
        } else if (!Hotel.game.flags("cat_carpet") && !Cutscene.in_scene) {
          // Grab any mud
          val outcome = Atrium.world.find("mud") match {
            case Some(mud) => {
              val monster = Instance("muk", mud.get_pos, List("mvmnt=bouncey"), "topdown")
                .manifest(Hotel.atrium, true)

              Cutscene.say(self, "Eekum, okum, splattery splat! RISE!") ++
              Cutscene.unfollow() ++
              List(new CinematicCamera(mud, 500f)) ++
              Cutscene.act(() => {
                mud.ez_shake(2)
              }) ++
              List(new Pause(2)) ++
              Cutscene.act(() => {
                Atrium.add(monster)
              }) ++
              Cutscene.say(monster, "Blraaawrgh!") ++
              Cutscene.act(() => {
                monster.speed_x = RNG.choose(List(1, -1)) * RNG.int(15, 30)
                monster.speed_y = RNG.choose(List(1, -1)) * RNG.int(15, 30)
              })
            }
            case None => {
              Cutscene.say(self, "Oh... bugger, this spell can't survive\nin too clean a room!")
            }
          }

          new Cutscene("levels/atrium",
            Cutscene.act(() => {
              self.ez_goto(Atrium.bathe_fountain)
            }),
            Cutscene.follow(self),
            Cutscene.say(self, "Alright, this damn cat won't sit still"),
            Cutscene.say(self, "And this devil girl won't play nice"),
            Cutscene.say(self, "Time to try something I learned in the Orient..."),
            List(new PauseUntil(() => self.in_zone("bathe_fountain"))),
            outcome,
            Cutscene.act(() => {
              state = new Voodoo()
            })
          ).start(Some(self))
        }
      }
      if (Schedule.dinner_time.after) {
        state = new Dinner()
      }
    }
  }

  class Storytime() extends State {
    if (Hotel.cat.flags("free")) {
      // Chase it again
      plan = new_plan.chase(Atrium.world.get("cat"))
    } else {
      // Cat will follow
      // TODO go to couch or something
    }

    override def transition(dt: Float) {
      if (Hotel.cat.flags("free")) {
        listen(
          "Jaguary story...",
          "I'm the hunter",
          "That cat is my PREY"
        )
      } else {
        listen(
          "Calm story...",
          "I'm a traveler, after all"
        )
      }

      if (Schedule.dinner_time.after) {
        state = new Dinner()
      }
    }
  }

  class Voodoo() extends State {
    plan = new_plan.chase(Atrium.world.get("muk"))

    override def transition(dt: Float) {
      listen("Oh my...", "Look at my creation!", "Telling you a story!", "Don't inch away!")

      // TODO respond to being chased by cat-monster
      // TODO mourn the loss of it if the flood occurs

      if (Schedule.dinner_time.after) {
        state = new Dinner()
      }
    }
  }

  class Dinner() extends State {}
}
