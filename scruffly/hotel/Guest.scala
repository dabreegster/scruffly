package scruffly.hotel

import scruffly._
import scruffly.engine._
import scruffly.util._
import scruffly.components._

class GuestController(id: String, main_level: String) extends Controller(id, main_level) {
  state = if (current_level == "levels/atrium") new IntroScene() else new WanderGarden()

  class IntroScene() extends State {
    override def transition(dt: Float) {
      if (Hotel.game.flags("intro_scene_done")) {
        state = new WanderAtrium()
      }
    }
  }

  class WanderAtrium() extends State {
    plan = new_plan.roam(Atrium.floor, RNG.int(3, 10))

    override def transition(dt: Float) {
      if (Hotel.game.flags("floor_open") && floor_bound) {
        state = new RoamFloor()
      }
    }
  }

  class WanderGarden() extends State {
    plan = new_plan.roam(Garden.ground, RNG.int(3, 10), Some(Garden.pool))

    override def transition(dt: Float) {
      if (Hotel.game.flags("floor_open") && floor_bound) {
        state = new RoamFloor()
      }
    }
  }

  class RoamFloor() extends State {
    plan = new_plan.goto_lvl("floor").roam(Floor.floor, 5)

    override def transition(dt: Float) {
      if (self.ez_on_fire) {
        state = new OnFire()
      }

      // TODO have conversations with each other...
      // "I like the new look!"
      // "Let's grab drinks if our fingers haven't burnt to a crisp!"
    }
  }

  class OnFire() extends State {
    override def transition(dt: Float) {
      for (maid <- self.world.find("maid") if self.nearby(maid, 100)) {
        self.ez_say("Why'd you hurt that poor\ninnocent girl?!")
        val p = self.physics.asInstanceOf[TopdownPhysicsModule]
        self.ez_pause(1)
        // TODO a hack to make herding work. change roam behavior to something else?
        val speed = 10 * dt
        if (maid.position_x < self.position_x) {
          p.request_right(speed)
        } else if (maid.position_x > self.position_x) {
          p.request_left(speed)
        }
        if (maid.position_y < self.position_y) {
          p.request_down(speed)
        } else if (maid.position_y > self.position_y) {
          p.request_up(speed)
        }
      }
      for (suzy <- self.world.find("suzy") if self.nearby(suzy, 100)) {
        self.ez_say("I don't even blame you")
      }

      if (self.current_zones.exists(z => z.get("level") == "levels/atrium")) {
        // TODO retain fire state! how? copy current effects somehow?
        plan = new_plan.goto_lvl("atrium").roam(Atrium.floor, 3)
      }

      if (self.in_zone("fountain")) {
        self.ez_say("Ahh, no more fire!")
        val fire = self.fated_others.find(c => c match {
          case _: Fire => true
          case _ => false
        }).get.asInstanceOf[Fire]
        fire.extinguished()

        state = new WanderAtrium()
      }

      if (self.in_zone("railing")) {
        new Cutscene("levels/atrium",
          Cutscene.pause(self),
          Cutscene.say_focus(self, "Ahh she pushed me over the railing!"),
          Cutscene.act(() => {
            state = new Dead()
          })
        ).start(None)
      }
    }
  }

  class Dead() extends State {
    self.destroy()
    current_level = "nowhere"
  }

  def eaten() {
    state = new Dead()
  }

  private def floor_bound = id.stripPrefix("guest").toInt % 2 == 0
}
