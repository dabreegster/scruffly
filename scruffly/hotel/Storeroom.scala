package scruffly.hotel

import scruffly._
import scruffly.engine._
import scruffly.util._
import scruffly.components._

import scala.collection.mutable

object Storeroom extends GeometryFactory {
  private val level = "levels/storeroom"
  val tile_size = Hotel.char_hitbox

  var grocery_list = new ShoppingList()

  // Shortcuts
  lazy val world = Hotel.get_level(level)
  lazy val to_atrium = Hotel.storeroom.all_sprites.find(_.get("level") == "levels/atrium").get
  lazy val butler = Hotel.storeroom.get("butler")

  def add(c: Component) {
    Game.engine.safely_add_component(c, level)
  }
  def del(c: Component) {
    Game.engine.del_component(c, level)
  }

  def setup(load: String) {
    val world = WorldSpec.load(load).make_tile_world(tile_size, Set(), Set())
    Hotel.storeroom = world

    // Components
    world.all_sprites.foreach(s => add(s))
    add(world.get_tilemap.get)
    add(new InteractButton())
    add(new SurfaceModifiers(world, "butler"))
    add(grocery_list)

    val spiders = world.all_sprites.filter(_.layer == "spider")
    add(new Lighting(
      List(() => Light(butler.get_collider_pos, radius = 300, color = RGB(255, 254, 195))) ++
      spiders.map(s => () => Light(s.get_collider_pos, radius = 200, color = RGB(255, 0, 0))),
      RGB(0, 0, 0, 220), true, world
    ))

    // Collisions
    world.handle_collision("butler", "zone", CollisionHandlers.ghost)
    world.handle_collision("butler", "scenery", (hit: Collision) => {
      if (hit.other.get("zone") != "food") {
        // TODO attract the monster?
        hit.pusher.ez_say("Ah, gotta be quiet...")
        hit.other.ez_shake(.5f)
      }
      Correct()
    })
    world.handle_collision("spider", "scenery", CollisionHandlers.normal_hit)
    // TODO this one fails?
    world.handle_collision("spider", "butler", (hit: Collision) => spider_hit(hit.pusher, hit.other))
    world.handle_collision("butler", "spider", (hit: Collision) => spider_hit(hit.other, hit.pusher))
  }

  private def spider_hit(spider: Sprite, butler: Sprite): CollisionResponse = {
    if (!spider.pauser.is_paused) {
      spider.ez_shake(5)
      spider.ez_pause(5)
      val stolen = grocery_list.steal()
      butler.ez_say(s"Ow! Nicked $stolen")
    }
    return Correct()
  }
}

class ShoppingList() extends Component {
  private val inv = new mutable.HashSet[String]()
  private val items = List("dairy", "meat", "bakery", "herbs", "bananas")

  private lazy val interact = Game.engine.ez_interact("levels/storeroom")

  override def update(dt_s: Float) {
    for (z <- Storeroom.butler.current_zones.filter(_.data("zone") == "food")) {
      val item = z.data("item")
      if (interact.pressed(Storeroom.butler, s"Grab $item from the shelves")) {
        inv += item
      }
    }
  }

  override def render(gfx: DrawModule) {
    if (Hotel.game.flags("entered_storeroom")) {
      gfx.set_translation(gfx.x_offset, gfx.y_offset)
      gfx.text(
        items.map(i => "[%s] %s".format((if (inv.contains(i)) "X" else " "), i)).mkString("\n"),
        10, 10, RGB(255, 0, 0)
      )
    }
  }

  def steal(): String = {
    val item = inv.headOption.getOrElse("nothing")
    inv -= item
    return item
  }
}
