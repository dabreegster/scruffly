package scruffly.hotel

import scruffly._
import scruffly.engine._
import scruffly.util._
import scruffly.components._

class AquastaziaController() extends Controller("aquastazia", "levels/atrium") {
  flags("crowd_drawn") = false
  flags("happy_splash_time") = false
  state = new DrawCrowd()

  class DrawCrowd() extends State {
    override def transition(dt: Float) {
      // Triggered at a time or if the butler tries to go to the garden early
      val butler = Atrium.world.get("butler")
      val butler_early = butler.current_zones.contains(Atrium.to_garden)

      if (!Cutscene.in_scene && (Schedule.draw_crowd_time.after || butler_early)) {
        if (butler_early) {
          Hotel.clock.fast_forward(Schedule.draw_crowd_time)
        }

        val aqua = Instance(
          "aquastazia", Atrium.to_garden.get_collider_pos, Nil, "tile"
        ).manifest(Hotel.atrium, true)
        Game.engine.safely_add_component(aqua, "levels/atrium")

        val menu = new Menu(
          "What should you serve Dr. Javoozy?",
          List(("a nice stiff drink", "booze"), ("a strong cup of joe", "coffee"))
        )
        val butler_physics = butler.physics
        val butler_controls = butler.behavior
        new Cutscene("levels/atrium",
          Cutscene.say_focus(self, "Making me get on land just to rile\nup a crowd!"),
          Cutscene.act(() => {
            plan = new_plan.goto_lvl("garden")
          }),
          Cutscene.say_focus(Hotel.atrium.get("cat"), "Mreow"),
          Cutscene.say(Hotel.atrium.get("amelia"), "No kitty, wait!"),
          Cutscene.act(() => {
            flags("crowd_drawn") = true
          }),
          Cutscene.say_focus(
            Hotel.atrium.get("javoozy"), "Hrmm, guess I'll have one last drink..."
          ),
          Cutscene.say(Hotel.atrium.get("javoozy"), "EHEM, what drink will you serve me?"),
          Cutscene.say_focus(butler, "On my way, sir"),
          Cutscene.act(() => {
            butler.physics = new TilePhysicsModule(butler.physics.get_collision_lines, butler)
            butler.physics.asInstanceOf[TilePhysicsModule].speed = TilePhysicsStyles.player_cutscene
            butler.ez_goto(Hotel.atrium.get("sight_javoozy"))
          }),
          Cutscene.follow(butler),
          List(new PauseUntil(() => butler.in_sight_zone("javoozy"))),
          List(menu),
          Cutscene.act(() => {
            Hotel.javoozy.flags("drunk") = menu.get_choice == "booze"
            Hotel.javoozy.flags("caffeinated") = menu.get_choice == "coffee"
            butler.behavior = butler_controls
            butler.physics = butler_physics
            state = new ReturnSwimming()
          })
        ).start(None)
      }
    }
  }

  class ReturnSwimming() extends State {
    plan = new_plan.goto(Garden.pool_entrance)

    override def transition(dt: Float) {
      if (self.in_zone("pool_entrance")) {
        self.destroy()
        val new_aqua = Instance(
          "aquastazia_swimming", Garden.pool.get_collider_pos, Nil, "tile"
        ).manifest(Hotel.garden, true)
        Game.engine.safely_add_component(new_aqua, "levels/garden")
        npc_name = "aquastazia_swimming"

        state = new Swim()
      }
    }
  }

  class Swim() extends State {
    plan = new_plan.roam(Garden.pool, 1)

    override def transition(dt: Float) {
      if (Garden.world.find("leon_swimming").isDefined) {
        if (Hotel.game.flags("pool_boozed")) {
          flags("happy_splash_time") = true
          listen("Splash splash splash...\nBite me, darling!")
        } else {
          listen("Get away, icky shark!")
        }
      }

      if (Schedule.aqua_judge_time.after) {
        exit_pool()
        self.ez_say("Well, time for a nice drink!")
        state = new JudgeCarpet()
      }
    }
  }

  class JudgeCarpet() extends State {
    plan = new_plan.goto_lvl("atrium")

    override def transition(dt: Float) {
      if (!Cutscene.in_scene && current_level == "levels/atrium") {
        // How's the carpet?
        val covered_zones = Hotel.atrium.all_sprites.filter(_.layer == "potted_plant").flatMap(_.current_zones)
        val covers_bar = covered_zones.exists(_.get("zone") == "bar")
        val covers_garden = covered_zones.exists(_.get("level") == "levels/garden")
        val covers_both = covers_bar && covers_garden

        val line =
          if (flags("happy_splash_time"))
            "Don't need red carpet,\ngot attention already"
          else if (!covers_both)
            "Where is my red carpet!"
          else if (Hotel.game.flags("carpet_dirty"))
            "Why is my red carpet dirty!"
          else
            "Drip drip drip on this nice clean carpet!"
        new Cutscene("levels/atrium",
          Cutscene.say_focus(self, line),
          Cutscene.act(() => {
            state = new IdleAtBar()
          })
        ).start(Some(self))
      }
    }
  }

  class IdleAtBar() extends State {
    plan = new_plan.goto(Atrium.bar)

    override def transition(dt: Float) {
      if (self.in_zone("bar")) {
        var near_doc = false
        for (doc <- Atrium.world.find("javoozy") if doc.in_zone("bar")) {
          near_doc = true
          if (flags("happy_splash_time") || Hotel.javoozy.flags("caffeinated")) {
            if (Schedule.piano_time.after) {
              self.ez_say("Let's go mix notes on the piano")
              state = new ListenPiano()
            } else {
              listen("Oh, how manly...\nMake me a drink?\n-wink-\nOh, and where's my tiara?")
            }
          } else {
            listen("Bug off, I need my tiara...")
          }
        }

        if (!near_doc) {
          if (Schedule.need_tiara_time.after) {
            listen(
              "I REALLY NEED MY TIARA YO...",
              "REALLY, PLEASE GET IT",
              "I CAN'T STAND ON LAND LONG WITHOUT IT"
            )
          } else {
            listen(
              "Could somebody bring me my tiara?",
              "I need it to survive on land."
            )
          }
        }

        if (Schedule.pipesmash_time.after && no_tiara) {
          state = new PipeSmash()
        }

        if (Schedule.dinner_time.after) {
          state = new Dinner()
        }
      }
    }
  }

  def piano_bound = state match {
    case _: ListenPiano => true
    case _ => false
  }
  class ListenPiano() extends State {
    plan = new_plan.goto(Atrium.piano)

    override def transition(dt: Float) {
      if (self.in_zone("piano")) {
        listen("Flirty music merpunk jokes")
      }

      if (Schedule.pipesmash_time.after && no_tiara) {
        state = new PipeSmash()
      }

      if (Schedule.dinner_time.after) {
        state = new Dinner()
      }
    }
  }

  class PipeSmash() extends State {
    override def transition(dt: Float) {
      // TODO the flood

      if (Schedule.dinner_time.after) {
        state = new Dinner()
      }
    }
  }

  class Dinner() extends State {}

  private def exit_pool() {
    assert(npc_name == "aquastazia_swimming")
    assert(current_level == "levels/garden")
    self.destroy()
    val new_aqua = Instance(
      "aquastazia", Garden.pool_entrance.get_collider_pos, Nil, "tile"
    ).manifest(Hotel.garden, true)
    Game.engine.safely_add_component(new_aqua, "levels/garden")
    npc_name = "aquastazia"
  }

  private def no_tiara =
    !Hotel.javoozy.flags("returned_tiara") && !Hotel.leon.flags("returned_tiara")
}
