package scruffly.hotel

import scruffly._
import scruffly.engine._
import scruffly.util._
import scruffly.components._

import scala.collection.mutable

// TODO this is a mix btwn game scenes and N's controller
class GameController() extends Component with Savable {
  val flags = new mutable.HashMap[String, Boolean]()

  flags("intro_scene_started") = false
  flags("intro_scene_done") = false
  flags("plants_scene_done") = false
  flags("floor_open") = false
  flags("carpet_dirty") = false
  flags("pool_boozed") = false
  flags("plants_shooting") = false
  flags("cat_carpet") = false
  flags("linens_delivered") = false
  flags("butler_chased_maid") = false
  flags("maid_naked") = false
  flags("chandelier_crashed") = false
  flags("entered_storeroom") = false
  flags("exited_storeroom") = false

  override def save(): String = {
    val flag_dump = flags.keys.toList.sorted.map(k => k + "=" + flags(k))
    return flag_dump.mkString(", ")
  }

  override def load(raw: String) {
    for (kv <- raw.split(", ")) {
      val flag = kv.split("=")
      flags(flag(0)) = flag(1) == "true"
    }
  }

  private val drop_key = new Keypress(Game.engine.keymap.key_drop)

  override def update(dt: Float) {
    intro_scene()
    lure_cat()
    drinking_plants()
    maid_takeover()
    manage_plants()
    manage_fires()
    manage_storeroom()
  }

  private def intro_scene() {
    if (!flags("intro_scene_started") && !Cutscene.in_scene) {
      flags("intro_scene_started") = true

      val maid = Hotel.atrium.get("maid")
      val butler = Hotel.atrium.get("butler")
      val leon = Hotel.atrium.get("leon")
      val in1 = Hotel.atrium.all_sprites.find(_.name == "guest1").get
      val in1_ctor = Hotel.get_generic(in1)
      val in2 = Hotel.atrium.all_sprites.find(_.name == "guest2").get
      val in2_ctor = Hotel.get_generic(in2)
      val out1 = Hotel.atrium.all_sprites.find(_.name == "guest_out1").get
      val out2 = Hotel.atrium.all_sprites.find(_.name == "guest_out2").get
      val out3 = Hotel.atrium.all_sprites.find(_.name == "guest_out3").get

      val butler_physics = butler.physics
      val butler_controls = butler.behavior

      def in_seq(sprite: Sprite, ctor: Controller, line: String) = List(
        Cutscene.act(() => {
          sprite.ez_goto(Atrium.checkin)
        }),
        List(new PauseUntil(() => sprite.in_zone("checkin"))),
        Cutscene.say(butler, line),
        Cutscene.act(() => {
          ctor.plan = ctor.new_plan
            .goto(Atrium.checkout).goto(Atrium.bar)
        }),
        List(new PauseUntil(() => sprite.in_zone("checkout")))
      ).flatten

      def out_seq(sprite: Sprite, line: String) = List(
        Cutscene.act(() => {
          sprite.ez_goto(Atrium.checkout)
        }),
        List(new PauseUntil(() => sprite.in_zone("checkout"))),
        Cutscene.say(butler, line),
        Cutscene.act(() => {
          sprite.ez_goto(Atrium.exit)
        }),
        List(new PauseUntil(() => sprite.in_zone("exit"))),
        Cutscene.act(() => {
          sprite.destroy()
        })
      ).flatten

      new Cutscene("levels/atrium",
        Cutscene.follow(butler),
        Cutscene.say(butler, "The crowds sure are building out here..."),
        Cutscene.say(maid, "You handle them; I'll clean upstairs."),
        Cutscene.say(maid, "Oh, and let's try not to screw anything up this time."),
        // Just get her out of the way
        Cutscene.act(() => {
          maid.ez_goto(Atrium.bar)
        }),
        List(new LogoEffect("hotel/logo.png", 5)),
        Cutscene.act(() => {
          butler.physics = new TilePhysicsModule(butler.physics.get_collision_lines, butler)
          butler.physics.asInstanceOf[TilePhysicsModule].speed = TilePhysicsStyles.player_cutscene
          butler.ez_goto(Atrium.behind_desk)
        }),
        List(new PauseUntil(() => butler.in_zone("behind_desk"))),
        Cutscene.act(() => {
          // Focus on the center of the desk area
          Game.engine.gfx.center(Atrium.checkout.get_collider_pos, Some(Atrium.world.bounds))
        }),
        Cutscene.say(butler, "Form an orderly line please...\nOne at a time"),

        out_seq(out1, "Hope you had a nice stay!"),
        in_seq(in1, in1_ctor, "Welcome. You'll be in room 1401"),
        out_seq(out2, "Mr. Johnson, I'm so terribly sorry about your poodle... Please accept this ch-- oh, he's gone."),
        in_seq(in2, in2_ctor, "Oh, the **** ***** suite? Right this way, sir."),

        // leon
        Cutscene.act(() => {
          leon.ez_goto(Atrium.checkout)
        }),
        List(new PauseUntil(() => leon.in_zone("checkout"))),
        Cutscene.say(butler, "You're not scheduled to leave yet"),
        Cutscene.say(leon, "I thought I could check out anytime! I wasn't even trying to leave."),
        Cutscene.act(() => {
          leon.ez_goto(Atrium.bar)
        }),

        out_seq(out3, "Some people have no control. Others? Well..."),
        List(new ShowControls("hotel/controls.png")),
        Cutscene.act(() => {
          // Don't need her anymore
          maid.destroy()
          butler.behavior = butler_controls
          butler.physics = butler_physics
          flags("intro_scene_done") = true
        })
      ).start(None)
    }
  }

  private def lure_cat() {
    if (Hotel.inventory.has_fishbait && drop_key.pressed) {
      Hotel.inventory.drop()
      val bait = Instance(
        "fishbait", Hotel.get_player.get_collider_pos,
        List("zone=fishbait"), "topdown"
      ).manifest(Hotel.get_current_level, true)
      Game.engine.add_component(bait, Game.engine.current_level)
    }
  }

  private def drinking_plants() {
    if (!flags("plants_scene_done") && !Cutscene.in_scene && Schedule.plants_watered_time.after) {
      val shrubs = Garden.world.all_sprites.filter(_.layer == "shrub")
      for (doc <- Garden.world.find("javoozy")) {
        flags("plants_scene_done") = true
        new Cutscene("levels/garden",
          Cutscene.pause(doc),
          Cutscene.say_focus(doc, "Attention, everybody!"),
          Cutscene.say(doc, "The plants have now been watered."),
          Cutscene.say(doc, "Please enjoy the garden."),
          Cutscene.unpause(doc),
          Cutscene.act(() => {
            shrubs.foreach(s => s.ez_shake(2))
          }),
          List(new Pause(2)),
          Cutscene.act(() => {
            flags("plants_shooting") = true
          })
        ).start(Some(doc))
      }
    }
  }

  private def maid_takeover() {
    if (!flags("floor_open") && !Cutscene.in_scene && Schedule.rooms_open_time.after) {
      flags("floor_open") = true
      // Calm them down finally
      flags("plants_shooting") = false

      // TODO destroy the butler less violently :\
      Hotel.get_player.destroy()
      Atrium.add(Instance(
        "butler", Atrium.bar.get_collider_pos.delta(100, 0), Nil, "tile"
      ).manifest(Hotel.atrium, true))

      // Can't be in the floor this early, so can be lazy in how we nuke the maid
      Floor.world.get("maid").destroy()
      val maid = Instance(
        "maid", Atrium.to_floor.get_collider_pos, List("player", "shooter"), "topdown"
      ).manifest(Hotel.atrium, true)
      Atrium.add(maid)
      Hotel.current_player = "maid"

      new Cutscene("levels/atrium",
        Cutscene.say_focus(maid, "I've finished cleaning,\nthe rooms are open")
      ).start(Some(maid), return_last = None)
    }
  }

  private var plants_shooting_now = false
  private def manage_plants() {
    if (!plants_shooting_now && flags("plants_shooting")) {
      plants_shooting_now = true
      for (shrub <- Garden.world.all_sprites.filter(_.layer == "shrub")) {
        val affected = Set("butler", "suzysol", "leon") ++ Hotel.all_generics
        shrub.child_components += (("plant_shooter", new PlantShooter(
          shrub, if (Hotel.javoozy.flags("drunk")) "booze_seed" else "coffee_seed",
          () => Garden.world.all_sprites.filter(s => affected.contains(s.layer))
        )))
      }
    }

    if (plants_shooting_now && !flags("plants_shooting")) {
      plants_shooting_now = false
      for (shrub <- Garden.world.all_sprites.filter(_.layer == "shrub")) {
        shrub.remove_child("plant_shooter")
      }
    }
  }

  private def manage_fires() {
    if (Hotel.current_player == "maid" && drop_key.pressed) {
      for (maid <- Hotel.find_player) {
        Hotel.inventory.drop()
        // TODO find fire better
        val fires = Game.engine.all_components(Game.engine.current_level).filter(c => c match {
          case f: Fire if f.source.nearby(maid, 100) => true
          case _ => false
        })
        if (Hotel.inventory.has_coffee) {
          // Extinguish!
          for (raw_fire <- fires) {
            raw_fire.asInstanceOf[Fire].extinguished()
          }
        } else if (fires.nonEmpty) {
          maid.ez_say("Er, turns out fires like alcohol...")
        }
      }
    }
  }

  private def manage_storeroom() {
    if (!flags("entered_storeroom") && Schedule.enter_storeroom_time.after && !Cutscene.in_scene) {
      // Don't set the flag just yet...

      val maid = Hotel.get_player
      val butler = Atrium.world.get("butler")
      val new_butler = Storeroom.world.get("butler")
      new Cutscene("levels/atrium",
        Cutscene.say_focus(maid, "Guess it's not my turn anymore"),
        Cutscene.say_focus(butler, "The guests will get hungry soon."),
        Cutscene.follow(butler),
        Cutscene.act(() => {
          Hotel.current_player = "butler"
          butler.ez_goto(Atrium.elevator)
        }),
        List(new PauseUntil(() => butler.in_zone("elevator"))),
        Cutscene.say(butler, "It's time to pursue an Abstract Noun."),
        Cutscene.dramatic_switch_levels("levels/atrium", "levels/storeroom", Some(new_butler)),
        Cutscene.say(new_butler, "Let's see, where's that ingredient list..."),
        Cutscene.act(() => {
          // This'll let the shopping list render
          flags("entered_storeroom") = true
          // Nix the old butler in the atrium too while we're at it
          butler.destroy()
          // TODO destroy the maid less violently? move her somewhere?
          maid.destroy()

          // Set up storeroom action
          for (spider <- Storeroom.world.all_sprites.filter(_.layer == "spider")) {
            spider.ez_chase(new_butler)
          }
        }),
        Cutscene.say(new_butler, "Ah, yes. Hunter-gatherer joke? Yep.")
      ).start(None, return_last = None)
    }

    if (!flags("exited_storeroom") && Schedule.forced_exit_storeroom_time.after && !Cutscene.in_scene) {
      new Cutscene("levels/storeroom",
        Cutscene.say_focus(Hotel.get_player, "Hmm, I should go back upstairs now..."),
        Cutscene.act(() => {
          flags("exited_storeroom") = true
        })
      ).start(None)
    }

    if (flags("exited_storeroom") && Game.engine.current_level == "levels/storeroom") {
      if (!Cutscene.in_scene) {
        Hotel.clock.fast_forward(Schedule.exit_storeroom_time)
        Game.engine.all_components("game").find(c => c match {
          case h: LevelSwitcher => true
          case _ => false
        }).get.asInstanceOf[LevelSwitcher].do_switch(Storeroom.world.get("butler"), "levels/atrium")
      }
    }
  }

  def butler_caught_maid(hit: Collision): CollisionResponse = {
    if (!flags("maid_naked")) {
      flags("maid_naked") = true
      new Cutscene("levels/floor",
        Cutscene.say_focus(hit.pusher, "Sorry, we must clean these..."),
        Cutscene.say_focus(hit.other, "Ahhh, my clothes!"),
        Cutscene.act(() => {
          hit.pusher.destroy()
          hit.other.renderer = SpritesheetRenderer(
            new SlickFileSpriteSheet(
              Game.engine.assets.asInstanceOf[SlickAssetsModule], "hotel/chars/maid_naked.png", 96, 144,
              List("270", "180", "0", "90")
            ), "270", .1f
          )
        })
      ).start(None)
    }
    return Correct()
  }
}

class LogoEffect(fn: String, time: Float) extends CutsceneCue with GeometryFactory {
  private val dims = Game.engine.assets.get_image_dims(fn)
  private var timer = 0f

  override def update(dt: Float) {
    timer += dt
  }

  override def render(gfx: DrawModule) {
    // Draw the logo centered
    gfx.set_translation(
      gfx.x_offset + ((gfx.get_screen_width - dims.x) / 2),
      gfx.y_offset + ((gfx.get_screen_height - dims.y) / 2)
    )
    gfx.image(fn)
    gfx.polygon(
      rect(dims.x, dims.y), true, true, RGB(255, 255, 255, Util.lerp(255, 0, timer / time).toInt)
    )
  }
  override def zorder = Hotel.gui_zorder

  override def done = timer >= time
  override def should_destroy = true
}

class ShowControls(fn: String) extends CutsceneCue {
  private val dims = Game.engine.assets.get_image_dims(fn)
  private var pressed = false

  private val done_key = new Keypress(Game.engine.keymap.key_interact)

  override def update(dt: Float) {
    if (done_key.pressed) {
      pressed = true
    }
  }

  override def render(gfx: DrawModule) {
    // Draw the picture centered
    gfx.set_translation(
      gfx.x_offset + ((gfx.get_screen_width - dims.x) / 2),
      gfx.y_offset + ((gfx.get_screen_height - dims.y) / 2)
    )
    gfx.image(fn)
  }
  override def zorder = Hotel.gui_zorder

  override def done = pressed
  override def should_destroy = true
}

class HotelInventory() extends Component with GeometryFactory with Savable {
  private var fishtank_has_bait = true

  private var current_item: Option[String] = None

  override def update(dt: Float) {
    if (!current_item.isDefined) {
      for (player <- Hotel.find_player) {
        val interact = Game.engine.ez_interact(Game.engine.current_level)
        if (player.in_zone("bar") && interact.pressed(player, "Grab a drink?")) {
          val maybe_fish = if (fishtank_has_bait) List(("a poor lil goldfish", "fishbait")) else Nil
          val menu = new Menu(
            "What do you want?",
            List(("a coffee", "coffee"), ("some booze", "booze")) ++ maybe_fish ++
            List(("nothing", "nothing"))
          )
          new Cutscene(Game.engine.current_level,
            List(menu),
            Cutscene.act(() => {
              if (menu.get_choice != "nothing") {
                current_item = Some(menu.get_choice)
              }
              if (menu.get_choice == "fishbait") {
                fishtank_has_bait = false
              }
            })
          ).start(None)
        }
      }
    }
  }

  override def render(gfx: DrawModule) {
    val extent = 100
    gfx.screen_transforms()
    val xo = gfx.get_screen_width - extent - 5
    val yo = 5
    gfx.polygon(translate(rect(extent, extent), xo, yo), false, true, RGB(255, 0, 0))
    for (item <- current_item) {
      gfx.image(s"hotel/${item}_icon.png", xo, yo, RGB(255, 255, 255, 200))
    }
    gfx.camera_transforms()
  }
  override def zorder = Hotel.gui_zorder

  def has_fishbait = current_item.getOrElse("") == "fishbait"
  def has_coffee = current_item.getOrElse("") == "coffee"
  def has_booze = current_item.getOrElse("") == "booze"
  def drop() {
    current_item = None
  }

  override def save() = s"inv: $current_item, $fishtank_has_bait"
  override def load(raw: String) {
    val values = raw.stripPrefix("inv: ").split(", ")
    current_item = values(0) match {
      case "None" => None
      case _ => Some(values(0).stripPrefix("Some(").stripSuffix(")"))
    }
    fishtank_has_bait = values(1) == "true"
  }
}
