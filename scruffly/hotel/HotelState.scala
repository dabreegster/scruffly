package scruffly.hotel

import scruffly.components._
import scruffly.engine._
import scruffly.util._
import scruffly._

import java.io.{File, PrintWriter, FileWriter}
import scala.io.Source

// Global mutable state for Hotel Solipcyst
object Hotel {
  var atrium: World = null
  var floor: World = null
  var garden: World = null
  var storeroom: World = null

  def get_level(lvl: String) = lvl match {
    case "levels/atrium" => atrium
    case "levels/floor" => floor
    case "levels/garden" => garden
    case "levels/storeroom" => storeroom
  }
  def get_current_level = get_level(Game.engine.current_level)
  def find_player = get_current_level.find(current_player)
  def get_player = find_player.get
  def get_portal(from: String, to: String) = (from, to) match {
    case ("levels/atrium", "levels/floor") => Atrium.to_floor
    case ("levels/atrium", "levels/garden") => Atrium.to_garden
    case ("levels/atrium", "levels/storeroom") => Atrium.elevator
    case ("levels/floor", "levels/atrium") => Floor.to_atrium
    case ("levels/garden", "levels/atrium") => Garden.to_atrium
    case ("levels/storeroom", "levels/atrium") => Storeroom.to_atrium
  }
  def get_portal_start_pos(from: String, to: String, sprite: Sprite) =
    get_portal(from, to).get_collider_pos.delta(
      sprite.position_x - sprite.get_collider_pos.x,
      sprite.position_y - sprite.get_collider_pos.y
    )

  val clock = new GameClock()
  var current_player = "butler"
  var inventory: HotelInventory = null
  var game: GameController = null

  // TODO fragile: cat has to be before amelia so she can chase it!
  var cat: CatController = null
  var aquastazia: AquastaziaController = null
  var leon: LeonController = null
  var javoozy: JavoozyController = null
  var amelia: AmeliaController = null
  var suzysol: SuzySolController = null
  var generic_guests: List[GuestController] = Nil

  def all_controllers = List(clock, game, inventory, cat, aquastazia, leon, javoozy, amelia, suzysol) ++ generic_guests
  def get_generic(guest: Sprite): GuestController = {
    val id = guest.layer.stripPrefix("guest").toInt
    return generic_guests(id - 1)
  }

  // More helpers?!
  val all_generics = Range(1, 21).map(n => "guest" + n).toSet
  val all_players = Set("butler", "maid")
  val all_specials = Set("javoozy", "amelia", "suzysol", "aquastazia", "leon", "cat")
  val all_npcs = all_specials ++ all_generics
  val all_chars = all_players ++ all_npcs
  val player_sights = Set("butler_sight", "maid_sight")
  val seeables = all_specials
  val char_hitbox = Pt(50, 20)
  val tile_zorder = 9000
  val gui_zorder = 9999
  var skip_key: scruffly.components.Keypress = null

  // Scenery is already implicit...
  def must_avoid(who: Sprite) = Set[String]()
  def should_avoid(who: Sprite) = all_chars -- Set(who.name)

  def save(dir: String) {
    new File(dir).mkdir()
    new File(dir + "/levels").mkdir()

    val w = new PrintWriter(new FileWriter(new File(dir + "/state")), true /* autoFlush */)
    w.println(Game.engine.current_level)
    w.println(current_player)
    all_controllers.foreach(c => w.println(c.save))
    w.close()

    for (level <- List(Hotel.atrium, Hotel.floor, Hotel.garden, Hotel.storeroom)) {
      level.to_spec.save(dir + "/" + level.level)
    }

    println(s"Game state all saved to $dir!")
  }

  // Returns the level to start in
  def load(dir: String): String = {
    val lines = Source.fromFile(dir + "/state").getLines.toList
    current_player = lines(1)

    for ((c, line) <- all_controllers.zip(lines.drop(2))) {
      c.load(line)
    }

    // TODO feeling more hacky :( blame the mead.
    for ((c, line) <- all_controllers.drop(3).zip(lines.drop(5))) {
      c.asInstanceOf[Controller]load_state(line)
    }

    println(s"Loaded game state from $dir!")
    return lines.head
  }
}

class GameClock() extends Component with Savable with GeometryFactory {
  // Start at 8:00:00
  private var current_time = 8 * 3600f
  private var running = true

  def after(time: Time) = current_time >= time.raw_time

  def fast_forward(time: Time) {
    assert(time.raw_time >= current_time)
    assert(running)
    current_time = time.raw_time
  }

  override def update(dt: Float) {
    if (running) {
      current_time += dt
    }
  }

  def pause() {
    assert(running)
    running = false
  }
  def unpause() {
    assert(!running)
    running = true
  }

  override def render(gfx: DrawModule) {
    val text_height = 50

    if (gfx.debug) {
      gfx.set_translation(gfx.x_offset, gfx.y_offset)
      gfx.polygon(
        translate(rect(100, text_height), 0, gfx.get_screen_height - text_height), true, true,
        RGB(0, 0, 0)
      )

      val hours = (current_time / 3600).toInt
      val mins = ((current_time - (hours * 3600)) / 60).toInt
      val secs = (current_time - (hours * 3600) - (mins * 60)).toInt
      gfx.text(
        Time.from_raw(current_time).prettyprint, 10, gfx.get_screen_height - text_height,
        RGB(0, 255, 0)
      )
    }
  }
  override def zorder = Hotel.gui_zorder

  override def save() = s"clock: $current_time"
  override def load(raw: String) {
    current_time = raw.stripPrefix("clock: ").toFloat
  }
}

class HotelWizard() extends Component {
  private val debug_key = new Keypress(Game.engine.keymap.key_debug_game)

  override def update(dt: Float) {
    if (Game.engine.keymap.keydown(Game.engine.keymap.key_fast)) {
      Hotel.clock.update(dt * 40)
    }
    if (Game.engine.keymap.keydown(Game.engine.keymap.key_very_fast)) {
      Hotel.clock.update(dt * 200)
    }

    if (debug_key.pressed && !Cutscene.in_scene) {
      Hotel.save("save")
    }
  }
}

class HotelLevelSwitcher() extends LevelSwitcher(Hotel.find_player _) {
  override def do_switch(owner: Sprite, target: String) {
    if (target == "levels/floor" && !Hotel.game.flags("floor_open")) {
      owner.ez_say("The maid's still cleaning...")
    } else if (target == "levels/storeroom") {
      owner.ez_say("It's not time to go there yet...")
    } else if (owner.world.level == "levels/floor" && !Hotel.game.flags("linens_delivered")) {
      owner.ez_say("The crowd will lynch me if I don't deliver more linens")
    } else if (owner.world.level == "levels/storeroom" && !Hotel.game.flags("exited_storeroom")) {
      val menu = new Menu("Leave the storeroom?", List(("yes", "yes"), ("no", "no")))
      new Cutscene(Game.engine.current_level,
        Cutscene.say(owner, "Hmm, do I have everything?"),
        List(menu),
        Cutscene.act(() => {
          if (menu.get_choice == "yes") {
            // We can't call do_switch here, since we're in a cutscene :(
            Hotel.game.flags("exited_storeroom") = true
          }
        })
      ).start(None)
    } else {
      super.do_switch(owner, target)
    }
  }
}

case class Time(hours: Int, minutes: Int, seconds: Int) {
  def after = Hotel.clock.after(this)
  def raw_time = (hours * 3600) + (minutes * 60) + seconds
  def prettyprint = "%02d:%02d:%02d".format(hours, minutes, seconds.toInt)
}
object Time {
  def from_raw(raw: Float): Time = {
    val hours = (raw / 3600).toInt
    val mins = ((raw - (hours * 3600)) / 60).toInt
    val secs = (raw - (hours * 3600) - (mins * 60)).toInt
    return Time(hours, mins, secs)
  }
}

object Schedule {
  val draw_crowd_time = Time(8, 4, 0)
  val plants_watered_time = Time(8, 10, 0)
  val leon_shark_time = Time(8, 15, 0)
  val pool_booze_march_time = Time(8, 20, 0)
  val leon_paparrazi_time = Time(8, 21, 30)
  val suzy_sleepy_time = Time(8, 22, 0)
  val aqua_judge_time = Time(8, 30, 0)
  val frat_doc_wakeup_time = Time(8, 40, 0)
  val rooms_open_time = Time(8, 50, 0)
  val jokes_to_firebug_time = Time(8, 55, 0)

  val piano_time = Time(9, 0, 0)
  val amelia_faint_time = Time(9, 2, 0)
  val need_tiara_time = Time(9, 5, 0)
  val story_time = Time(9, 30, 0)

  val javoozy_chill_time = Time(10, 20, 0)
  val chandelier_crash_time = Time(10, 21, 0)
  // after the cat may go up to get the tiara
  val leon_downstairs_time = Time(10, 25, 0)
  val enter_storeroom_time = Time(10, 50, 0)
  val forced_exit_storeroom_time = Time(11, 9, 30)
  val exit_storeroom_time = Time(11, 10, 0)

  val pipesmash_time = Time(11, 30, 0)

  val dinner_time = Time(12, 0, 0)
}
