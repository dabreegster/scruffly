package scruffly.hotel

import scruffly._
import scruffly.engine._
import scruffly.util._
import scruffly.components._

class LeonController() extends Controller("leon", "levels/atrium") {
  flags("has_linens") = false
  flags("has_tiara") = false
  flags("returned_tiara") = false
  flags("rescued_amelia") = false
  state = new IntroScene()

  class IntroScene() extends State {
    override def transition(dt: Float) {
      if (Hotel.game.flags("intro_scene_done")) {
        state = new Brunch()
      }
    }
  }

  class Brunch() extends State {
    plan = new_plan.roam(Atrium.floor, 25)

    override def transition(dt: Float) {
      talk(
        "Hello sir!",
        "Morning's odd, no particular role to play...\nWho am I, if all the world is an empty stage?"
      )

      if (Hotel.aquastazia.flags("crowd_drawn")) {
        self.ez_say("Guess I'll meander to the outermost of doors!")
        state = new PaceAnxiously()
      }
    }
  }

  class PaceAnxiously() extends State {
    plan = new_plan.goto_lvl("garden").roam(Garden.ground, 0, Some(Garden.pool))

    override def transition(dt: Float) {
      if (current_level == "levels/garden") {
        talk("What's wrong", "I want to bite her, but I'm nervous!")
      }

      if (Schedule.leon_shark_time.after && !Cutscene.in_scene) {
        new Cutscene("levels/garden",
          Cutscene.follow(self),
          Cutscene.say(self, "It's time to make a move"),
          Cutscene.act(() => {
            self.ez_goto(Garden.pool_entrance)
          }),
          List(new PauseUntil(() => self.in_zone("pool_entrance"))),
          Cutscene.say(self, "Rawr I'm a shark!"),
          Cutscene.act(() => {
            self.destroy()
            val new_leon = Instance(
              "leon_swimming", Garden.pool.get_collider_pos, Nil, "tile"
            ).manifest(Hotel.garden, true)
            Garden.add(new_leon)
            npc_name = "leon_swimming"
            state = new SharkMode()
          }),
          List(new Pause(1))
        ).start(Some(self))
      }
    }
  }

  class SharkMode() extends State {
    plan = new_plan.wait_for("aquastazia_swimming").maybe_chase("aquastazia_swimming")

    override def transition(dt: Float) {
      if (Garden.world.find("aquastazia_swimming").isDefined) {
        if (Hotel.game.flags("pool_boozed")) {
          listen("Chomp chomp")
        } else {
          listen("But I'm the Fabio of sharks!")

          // Give up later
          if (!Cutscene.in_scene && Schedule.leon_paparrazi_time.after) {
            exit_pool()
            state = new Paparrazi()
          }
        }
      } else {
        listen("Ahh, a nice relaxing swim...")
      }

      if (Hotel.game.flags("floor_open")) {
        exit_pool()
        state = new GotoRoom()
      }
    }
  }

  class Paparrazi() extends State {
    plan = new_plan.goto_lvl("atrium").patrol("potted_plant")

    // TODO a way that works with the plan?
    private var recruited_help = false  // Don't need to save

    override def transition(dt: Float) {
      if (current_level == "levels/atrium") {
        if (!recruited_help) {
          recruited_help = true
          for (guest <- Atrium.world.all_sprites.filter(_.layer.startsWith("guest"))) {
            guest.ez_chase(self)
          }
        }

        if (Hotel.game.flags("floor_open")) {
          state = new GotoRoom()
        } else {
          listen("Right this way!\nPress passes only!")
        }
      }
    }
  }

  class AwkwardFlirt() extends State {
    plan = new_plan.goto(Atrium.bar)

    override def transition(dt: Float) {
      if (self.in_zone("bar")) {
        listen("I was always a fan...", "Oh, you're ignoring me.")
      }

      if (Hotel.game.flags("floor_open")) {
        state = new GotoRoom()
      }
    }
  }

  class GotoRoom() extends State {
    plan = new_plan.goto_lvl("floor").goto(Floor.leon_room)

    override def transition(dt: Float) {
      if (self.in_zone("leon_room")) {
        self.destroy()
        current_level = "nowhere"
        state = new WaitInRoom()
      }
    }
  }

  def linens_delivered(door: Sprite, on_fire: Boolean): Boolean = {
    val in_room = state match {
      case _: WaitInRoom => true
      case _ => false
    }
    if (in_room) {
      if (on_fire) {
        door.ez_say("Wretched maid, these're a bit warm!")
      } else {
        door.ez_say("Thanks! These'll come in handy...")
        flags("has_linens") = true
      }
    } else {
      door.ez_say("Nobody's home...")
    }
    return in_room
  }
  class WaitInRoom() extends State {
    override def transition(dt: Float) {
      if (Schedule.leon_downstairs_time.after) {
        state = new ReturnDownstairs()
      }
    }
  }

  class ReturnDownstairs() extends State {
    if (!Floor.world.find("leon").isDefined) {
      Floor.add(
        Instance("leon", Floor.leon_room.get_collider_pos, Nil, "tile").manifest(Hotel.floor, true)
      )
      current_level = "levels/floor"
    }

    plan = new_plan.maybe_goto("tiara").goto_lvl("atrium")

    override def transition(dt: Float) {
      if (self.in_zone("tiara")) {
        Floor.world.get("tiara").destroy()
        flags("has_tiara") = true
      }

      if (current_level == "levels/atrium") {
        if (flags("has_tiara")) {
          state = new ReturnTiara()
        } else {
          state = new HandleChandelier()
        }
      }
    }
  }

  class ReturnTiara() extends State {
    plan = new_plan.chase(Atrium.world.get("aquastazia"))

    override def transition(dt: Float) {
      val aqua = Atrium.world.get("aquastazia")
      if (!Cutscene.in_scene && self.nearby(aqua, 100)) {
        new Cutscene("levels/atrium",
          Cutscene.say_focus(self, "Princess, I have _plenty_ of wood now!"),
          Cutscene.say(aqua, "-Schoolgirl giggle-"),
          Cutscene.act(() => {
            flags("returned_tiara") = true
            state = new HandleChandelier()
          })
        ).start(Some(self))
      }
    }
  }

  class HandleChandelier() extends State {
    if (Hotel.game.flags("chandelier_crashed")) {
      val amelia = Atrium.world.get("amelia")
      new Cutscene("levels/atrium",
        Cutscene.say_focus(self, "Daring rescue!"),
        Cutscene.say_focus(amelia, "Oh, thank you!"),
        Cutscene.act(() => {
          flags("rescued_amelia") = true
          Hotel.amelia.rescued()
          state = new GetWood()
        })
      ).start(Some(self))
    } else {
      state = new GetWood()
    }
  }

  class GetWood() extends State {
    // TODO what if we've already got it? should goto new state
    plan = new_plan.goto_lvl("garden").goto(Garden.world.get("wood"))

    private val bored = flags("returned_tiara") || flags("rescued_amelia") || flags("has_linens")

    override def transition(dt: Float) {
    }
  }

  private def exit_pool() {
    assert(npc_name == "leon_swimming")
    assert(current_level == "levels/garden")
    self.destroy()
    val new_leon = Instance(
      "leon", Garden.pool_entrance.get_collider_pos, Nil, "tile"
    ).manifest(Hotel.garden, true)
    Game.engine.safely_add_component(new_leon, "levels/garden")
    npc_name = "leon"
  }
}
