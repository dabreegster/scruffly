package scruffly.hotel

import scruffly._
import scruffly.engine._
import scruffly.util._
import scruffly.components._

class CatController() extends Controller("cat", "levels/atrium") {
  flags("free") = false
  flags("team_mud_monster") = false
  state = new Brunch()

  class Brunch() extends State {
    override def transition(dt: Float) {
      if (Hotel.aquastazia.flags("crowd_drawn")) {
        state = new PlayGarden()
      }
    }
  }

  class PlayGarden() extends State {
    plan = new_plan.goto_lvl("garden").roam(Garden.ground, 0, Some(Garden.pool))

    flags("free") = true

    override def transition(dt: Float) {
      for (bait <- Hotel.get_current_level.find("fishbait")) {
        state = new ChaseBait()
      }

      if (Hotel.game.flags("floor_open")) {
        state = new SpreadMud()
      }

      caught_by_carpet()
    }
  }

  class ChaseBait() extends State {
    // TODO this assumes the player doesnt drop the bait and super quickly disappear. be more
    // flexible and find the fishbait.
    plan = new_plan.goto_lvl(Game.engine.current_level).maybe_goto("fishbait")

    override def transition(dt: Float) {
      if (self.in_zone("fishbait")) {
        self.world.get("fishbait").destroy()
        self.ez_say("Nom nom nom~")
        state = new PlayGarden()
        self.ez_pause(3)
      }

      caught_by_carpet()
    }
  }

  def bath_time() {
    state = new Captured()
  }
  class Captured() extends State {
    plan = new_plan.chase(Atrium.world.get("amelia"))

    override def transition(dt: Float) {
      if (Hotel.game.flags("chandelier_crashed")) {
        state = new StealTiara()
      }

      if (Schedule.dinner_time.after) {
        state = new Dinner()
      }
    }
  }

  class SpreadMud() extends State {
    // TODO also spread it on the floor?
    plan = new_plan.goto_lvl("atrium").roam(Atrium.floor, 2)

    // TODO actually visit waypoints to spawn mud
    if (!Atrium.world.find("mud").isDefined) {
      for (waypt <- Atrium.world.all_waypts("first_mud")) {
        Atrium.add(Instance("mud", waypt.get_collider_pos, Mud.fx, "tile").manifest(Atrium.world, true))
      }
    }

    override def transition(dt: Float) {
      if (Schedule.need_tiara_time.after) {
        state = new Sleep()
      }
    }
  }

  class Sleep() extends State {
    // TODO rest by fire or a couch?
    self.behavior = new NullBehavior(self)

    override def transition(dt: Float) {
      listen("Zzz...")

      if (Schedule.exit_storeroom_time.after && Atrium.world.find("muk").isDefined) {
        state = new RideMudMonster()
      }

      if (Schedule.dinner_time.after) {
        state = new Dinner()
      }
    }
  }

  class RideMudMonster() extends State {
    private val monster = Atrium.world.get("muk")
    plan = new_plan.chase(monster)

    override def transition(dt: Float) {
      if (self.nearby(monster, 100) && !Cutscene.in_scene && !flags("team_mud_monster")) {
        flags("team_mud_monster") = true
        new Cutscene("levels/atrium",
          Cutscene.act(() => {
            monster.pauser.pause(None)
          }),
          Cutscene.say_focus(monster, "Gurgle"),
          Cutscene.say_focus(self, "MREOW!"),
          Cutscene.follow(monster),
          Cutscene.act(() => {
            monster.pauser.unpause()
            monster.ez_goto(Atrium.world.get("amelia"))
          })
        ).start(Some(monster))
      }
    }
  }

  class StealTiara() extends State {
    // TODO just double check we get it before javoozy and leon
    plan = new_plan.goto_lvl("floor").goto(Floor.world.get("tiara"))

    override def transition(dt: Float) {
      if (self.in_zone("tiara")) {
        self.ez_say("Meowhahaha!")
        Floor.world.get("tiara").destroy()
        state = new TeaseAqua()
      }
    }
  }

  class TeaseAqua() extends State {
    plan = new_plan.goto_lvl("atrium").chase(Atrium.world.get("aquastazia"))

    override def transition(dt: Float) {
      if (Schedule.exit_storeroom_time.after && Atrium.world.find("muk").isDefined) {
        state = new RideMudMonster()
      }

      if (Schedule.dinner_time.after) {
        state = new Dinner()
      }
    }
  }

  class Dinner() extends State {}

  private def caught_by_carpet() {
    val plants = Hotel.atrium.all_sprites.filter(_.layer == "potted_plant")
    val dist = Line(plants(0).get_collider_pos, plants(1).get_collider_pos).length
    if (flags("free") && self.in_zone("carpet") && dist < 350) {
      flags("free") = false
      Hotel.game.flags("cat_carpet") = true
      self.ez_say("Mreowgrumble...")
      // Let Amelia catch up...
      self.behavior = new NullBehavior(self)
    }
  }
}
