package scruffly.hotel

import scruffly._
import scruffly.engine._
import scruffly.util._
import scruffly.components._

import scala.collection.mutable

object Floor extends GeometryFactory {
  private val level = "levels/floor"
  val tile_size = Hotel.char_hitbox

  // Shortcuts
  lazy val world = Hotel.get_level(level)
  lazy val to_atrium = Hotel.floor.all_sprites.find(_.get("level") == "levels/atrium").get
  lazy val cart = Hotel.floor.get("cart")
  lazy val elevator = Hotel.floor.get_zone("elevator")
  lazy val floor = Hotel.floor.get_zone("ground")
  lazy val leon_room = Hotel.floor.get_zone("leon_room")

  def add(c: Component) {
    Game.engine.safely_add_component(c, level)
  }
  def del(c: Component) {
    Game.engine.del_component(c, level)
  }

  def setup(load: String) {
    val world = WorldSpec.load(load).make_tile_world(tile_size, Set(), Set("cart"))
    Hotel.floor = world

    // Components
    world.all_sprites.foreach(s => add(s))
    add(world.get_tilemap.get)
    add(new InteractButton())
    add(new LinenDelivery())

    // Collisions
    world.handle_collisions(
      Hotel.all_chars,
      "scenery",
      CollisionHandlers.normal_hit _
    )
    world.handle_collisions(
      Hotel.all_chars ++ Set("cart"),
      Set("zone"),
      CollisionHandlers.ghost _
    )
    world.handle_collisions(Hotel.all_npcs ++ Set("butler"), "cart", CollisionHandlers.normal_hit _)
    world.handle_collision("maid", "cart", CollisionHandlers.push_with_key)

    world.handle_collisions(
      Hotel.all_players, Hotel.all_specials, CollisionHandlers.normal_hit _,
      except = Set(("maid", "javoozy"))
    )
    world.handle_collisions(Hotel.all_generics, Hotel.all_generics, CollisionHandlers.bounce _)
    world.handle_collisions("maid", Hotel.all_generics, (hit: Collision) => {
      hit.pusher.ez_say("Ooh, excuse me sir!")
      CollisionHandlers.bounce(hit)
    })
    world.handle_collisions(Set("maid_sight"), Hotel.seeables, (hit: Collision) => {
      if (!hit.other.owner.speech.is_speaking) {
        hit.other.owner.speech.say("Hello sir")
      }
      Ghost()
    })

    // These're always true on the floor
    world.handle_collision("maid", "javoozy", Hotel.javoozy.spilled_maid)
    world.handle_collision("butler", "maid", Hotel.game.butler_caught_maid)
    world.handle_collision("maid", "sight_javoozy", Hotel.javoozy.see_naked_maid)
  }
}

case class DoorRenderer() extends Renderer {
  override def draw(obj: Sprite) {
    val num = obj.get("room")
    Game.engine.gfx.text(s"Room $num", 30, -200, RGB(255, 255, 255))
  }
  override def get_dims = throw new UnsupportedOperationException()
  override def cloneme = copy()
}

class LinenDelivery() extends Component {
  // Don't save
  private val visited = new mutable.HashSet[String]()
  private val needed_rooms = 5
  private val interact = Game.engine.ez_interact("levels/floor")

  override def update(dt_s: Float) {
    if (active) {
      for (maid <- Hotel.find_player) {
        for (cart <- maid.get_zone("cart")) {
          for (door <- cart.current_zones.find(_.data.contains("text"))) {
            val room = door.get("text")
            if (interact.pressed(maid, s"Drop off linens to $room?")) {
              if (visited.contains(room)) {
                // That's right, the door speaks
                door.ez_say("You already gave me linens!")
              } else {
                if (room == "Leon's room") {
                  // Can't deliver if he's not there...
                  if (Hotel.leon.linens_delivered(door, cart.ez_on_fire)) {
                    visited += room
                  }
                } else {
                  visited += room
                  if (cart.ez_on_fire) {
                    door.ez_say("Gee, thanks for warming up these linens!")
                  } else {
                    door.ez_say("What new stains will these have!")
                  }

                  if (visited.size >= needed_rooms) {
                    Hotel.game.flags("linens_delivered") = true
                  }
                }
              }
            }
          }
        }
      }
    }
  }

  override def render(gfx: DrawModule) {
    if (active) {
      gfx.set_translation(gfx.x_offset, gfx.y_offset)
      gfx.text(
        "%d/%d rooms".format(visited.size, needed_rooms),
        10, 10, RGB(255, 0, 0)
      )
    }
  }

  private def active = Hotel.current_player == "maid"
}
