package scruffly.hotel

import scruffly._
import scruffly.engine._
import scruffly.util._
import scruffly.components._

import scala.collection.mutable

object Atrium extends GeometryFactory {
  private val level = "levels/atrium"
  val tile_size = Hotel.char_hitbox

  // Shortcuts
  lazy val world = Hotel.get_level(level)
  lazy val to_garden = Hotel.atrium.all_sprites.find(_.get("level") == "levels/garden").get
  lazy val to_floor = Hotel.atrium.all_sprites.find(_.get("level") == "levels/floor").get
  lazy val bar = Hotel.atrium.get_zone("bar")
  lazy val floor = Hotel.atrium.get_zone("ground_floor")
  lazy val bathe_fountain = Hotel.atrium.get_zone("bathe_fountain")
  lazy val piano = Hotel.atrium.get_zone("piano")
  lazy val elevator = Hotel.atrium.get_zone("elevator")
  lazy val checkin = Hotel.atrium.get_zone("checkin")
  lazy val checkout = Hotel.atrium.get_zone("checkout")
  lazy val exit = Hotel.atrium.get_zone("exit")
  lazy val behind_desk = Hotel.atrium.get_zone("behind_desk")

  def add(c: Component) {
    Game.engine.safely_add_component(c, level)
  }
  def del(c: Component) {
    Game.engine.del_component(c, level)
  }

  def setup(load: String) {
    val furniture = Set("armchair", "potted_plant")

    // Must be added super early so we can apply the water effect to the scenery
    add(new WaterEffect())

    val world = WorldSpec.load(load).make_tile_world(tile_size, Set(), furniture)
    Hotel.atrium = world

    // Components
    world.all_sprites.foreach(s => add(s))
    add(world.get_tilemap.get)

    add(new InteractButton())
    val plants = world.all_sprites.filter(_.layer == "potted_plant")
    assert(plants.size == 2)
    val carpet = new RedCarpet(world.get("carpet"), plants(0), plants(1))
    add(carpet)
    add(new CarpetMud(world.all_sprites.filter(s => Hotel.all_chars.contains(s.layer))))
    add(new SurfaceModifiers(world, "butler"))
    add(new ChandelierSway(world.get("chandelier")))

    // Collisions
    world.handle_collisions(
      Hotel.all_chars,
      "scenery",
      CollisionHandlers.normal_hit _
    )
    world.handle_collisions(
      Hotel.all_chars ++ Set("potted_plant", "guest_out1", "guest_out2", "guest_out3"),
      Set("zone", "carpet", "fishbait"),
      CollisionHandlers.ghost _
    )
    world.handle_collisions(Hotel.player_sights, Hotel.seeables, CollisionHandlers.ghost _)
    world.handle_collisions(Hotel.seeables, Hotel.player_sights, CollisionHandlers.ghost _)
    world.handle_collisions(Hotel.all_players, Hotel.all_specials, CollisionHandlers.normal_hit _)
    world.handle_collisions(Hotel.all_chars, "mud", Mud.spread _)
    world.handle_collision("spray_bullet", "mud", (hit: Collision) => {
      hit.pusher.destroy()
      hit.other.get_child("health").asInstanceOf[Health].change(-1)
      Ghost()
    })

    world.handle_collisions(
      Hotel.all_players,
      furniture,
      CollisionHandlers.push_with_key _,
      except = Set(("butler", "potted_plant"))
    )
    world.handle_collisions(furniture, furniture, CollisionHandlers.push _)
    world.handle_collisions(furniture, Hotel.all_npcs, CollisionHandlers.normal_hit _)
    world.handle_collisions(
      furniture,
      "scenery",
      CollisionHandlers.normal_hit _
    )

    world.handle_collision("butler", "potted_plant", carpet.handle_push)

    world.handle_collisions("butler", Hotel.all_generics, (hit: Collision) => {
      hit.pusher.ez_say("Excuse me")
      hit.other.ez_pause(.5f)
      Correct()
    })
    world.handle_collisions(Hotel.all_npcs, Hotel.all_players, CollisionHandlers.bounce _)
    world.handle_collisions(Hotel.all_players, Hotel.all_players, CollisionHandlers.bounce _)
    world.handle_collision("muk", "scenery", CollisionHandlers.bounce)
    world.handle_collision("muk", "mud", Mud.spread _)
  }
}

class RedCarpet(carpet: Sprite, b1: Sprite, b2: Sprite) extends Component with GeometryFactory {
  private val length_limit = 2000
  private val half_width = 25

  private def get_sidelines(): (Line, Line) = {
    val base1 = b1.get_collider_pos
    val base2 = b2.get_collider_pos
    // TODO find other places using atan2, put angle in Line
    val angle = math.toDegrees(math.atan2(
      // invert y...
      base1.y - base2.y,
      base2.x - base1.x
    ))

    // Invert y everywhere
    val b1_corner1 = base1.delta(
      (half_width * math.cos(math.toRadians(angle + 90))).toFloat,
      (-half_width * math.sin(math.toRadians(angle + 90))).toFloat
    )
    val b1_corner2 = base1.delta(
      (half_width * math.cos(math.toRadians(angle - 90))).toFloat,
      (-half_width * math.sin(math.toRadians(angle - 90))).toFloat
    )
    val b2_corner1 = base2.delta(
      (half_width * math.cos(math.toRadians(angle + 90))).toFloat,
      (-half_width * math.sin(math.toRadians(angle + 90))).toFloat
    )
    val b2_corner2 = base2.delta(
      (half_width * math.cos(math.toRadians(angle - 90))).toFloat,
      (-half_width * math.sin(math.toRadians(angle - 90))).toFloat
    )

    return (Line(b1_corner1, b2_corner1), Line(b1_corner2, b2_corner2))
  }

  override def update(dt: Float) {
    val sidelines = get_sidelines
    val lines = points_to_closed_polygon(List(
      sidelines._1.pt1, sidelines._2.pt1, sidelines._2.pt2, sidelines._1.pt2
    ))
    carpet.world.before_move(carpet)
    carpet.physics = new TopdownPhysicsModule(lines, carpet)
    carpet.world.after_move(carpet)
  }

  def handle_push(hit: Collision): CollisionResponse = {
    val orig_at = hit.other.get_pos

    val result = CollisionHandlers.push_with_key(hit)
    val length = Line(b1.get_collider_pos, b2.get_collider_pos).length

    val sidelines = get_sidelines
    val no_hits = !carpet.world.select_scenery(carpet.get_hitbox).find(tester =>
      tester.hit_lines(sidelines._1).isDefined || tester.hit_lines(sidelines._2).isDefined
    ).isDefined

    if (length <= length_limit && no_hits) {
      return result
    } else {
      // Undo!
      hit.other.warp(orig_at)
      return Correct()
    }
  }
}

case class CarpetRenderer() extends Renderer with GeometryFactory {
  // TODO bit funky to have state :(
  var color = RGB(255, 0, 0)

  override def get_dims() = throw new UnsupportedOperationException()
  override def cloneme() = CarpetRenderer()

  override def draw(obj: Sprite) {
    Game.engine.gfx.polygon(obj.get_collider.get_lines.flatMap(_.points), true, true, color)
  }
}

class CarpetMud(sprites: List[Sprite]) extends Component {
  private var jerks = new mutable.HashSet[Sprite]()

  override def update(dt: Float) {
    for (s <- sprites if s.in_zone("carpet")) {
      if (!jerks.contains(s)) {
        jerks += s
        s.ez_say("Ehh this carpet can handle a little dirt...")
        val carpet = s.get_zone("carpet").get
        carpet.renderer.asInstanceOf[CarpetRenderer].color = jerks.size match {
          case 1 => RGB(161, 0, 0)
          case 2 => RGB(135, 0, 0)
          case _ => RGB(117, 0, 0)
        }
        Hotel.game.flags("carpet_dirty") = true
      }
    }
  }
}

class ChandelierSway(chandelier: Sprite) extends Component {
  private val orig_pos = chandelier.instance.start
  private val x_extent = 50
  private val speed = 5f

  chandelier.physics.asInstanceOf[TopdownPhysicsModule].mvmnt = MovementStyles.bullet
  chandelier.speed_x = speed

  override def update(dt: Float) {
    if (chandelier.position_x < orig_pos.x - x_extent) {
      chandelier.speed_x = speed
    }
    if (chandelier.position_x > orig_pos.x + x_extent) {
      chandelier.speed_x = -speed
    }
  }
}
