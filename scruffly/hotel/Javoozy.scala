package scruffly.hotel

import scruffly._
import scruffly.engine._
import scruffly.util._
import scruffly.components._

class JavoozyController() extends Controller("javoozy", "levels/atrium") {
  flags("drunk") = false
  flags("caffeinated") = false
  flags("mood_spazzy") = false
  flags("mood_crisis_averted") = false
  flags("mood_partied_out") = false
  flags("fainted_nudity") = false
  flags("returned_tiara") = false
  state = new Brunch()

  class Brunch() extends State {
    override def transition(dt: Float) {
      talk(
        "I never see you sitting still, Dr. Javoozy!",
        "This is the one time of day when\neverybody drinks coffee, isn't it wonderful!"
      )

      if (!Cutscene.in_scene) {
        if (flags("drunk") || flags("caffeinated")) {
          val line =
            if (flags("drunk"))
              "Dr. Javoozy is now woozy!"
            else
              "It's as if the strong hands of Joe\nsmacked me across the face!\nWaaaaooooo!"
          new Cutscene(current_level,
            Cutscene.say(self, line),
            Cutscene.act(() => {
              state = new WaterGarden()
            })
          ).start(None)
        }
      }
    }
  }

  class WaterGarden() extends State {
    plan = new_plan.goto_lvl("garden").patrol("shrub")

    override def transition(dt: Float) {
      talk("Are you OK?", if (flags("drunk")) "Woo!!! Garden party!" else "I'm hyper")

      if (Schedule.pool_booze_march_time.after && !Cutscene.in_scene) {
        if (flags("drunk")) {
          new Cutscene("levels/garden",
            Cutscene.pause(self),
            Cutscene.say_focus(self, "No booze left, gotta brew summore..."),
            Cutscene.unpause(self),
            Cutscene.act(() => {
              state = new PoolBooze()
            })
          ).start(Some(self))
        } else {
          flags("mood_spazzy") = true
          state = new ChillAtBar()
        }
      }
    }
  }

  class PoolBooze() extends State {
    plan = new_plan.goto(Garden.march_start).goto(Garden.pool_entrance)
    //self.physics.asInstanceOf[TopdownPhysicsModule].mvmnt = MovementStyles.slow

    override def transition(dt: Float) {
      if (Hotel.inventory.has_booze) {
        interact(
          "Give Dr. Javoozy a refreshing splash of whiskey?",
          (player: Sprite) => List(
            Cutscene.say_focus(self, "Booze so good, me make more! Arrgh!"),
            Cutscene.act(() => {
              Hotel.inventory.drop()
              //self.physics.asInstanceOf[TopdownPhysicsModule].mvmnt = MovementStyles.fast
            }),
            Cutscene.say_focus(player, "Oh dear, now he's more determined than ever!")
          )
        )
      } else if (Hotel.inventory.has_coffee) {
        interact(
          "Give Dr. Javoozy a sobering pot of espresso?",
          (player: Sprite) => List(
            Cutscene.act(() => {
              Hotel.inventory.drop()
              flags("drunk") = false
              flags("caffeinated") = true
              flags("mood_crisis_averted") = true
            }),
            Cutscene.say_focus(self, "Owww my eyes!\nWhat was I thinking?\nAlcohol is bad, kids."),
            Cutscene.say_focus(player, "Whew, crisis averted."),
            Cutscene.act(() => {
              //self.physics.asInstanceOf[TopdownPhysicsModule].mvmnt = MovementStyles.default
              state = new ChillAtBar()
            })
          )
        )
      }

      // TODO require us to pass the march start zone first? if we happen to cross on the way to
      // it...
      if (self.in_zone("pool_entrance") && !Cutscene.in_scene) {
        new Cutscene("levels/garden",
          // TODO mixed drink with chlorine...
          Cutscene.say_focus(
            self, "My 14 years at mixology school\n(partying)\nmean it's party time!"
          ),
          Cutscene.say(self, "Passing out... X_X"),
          Cutscene.act(() => {
            Hotel.game.flags("pool_boozed") = true
            state = new PassedOut()
          })
        ).start(Some(self))
      }
    }
  }

  class PassedOut() extends State {
    self.behavior = new NullBehavior(self)

    override def transition(dt: Float) {
      interact(
        "Wake up Dr. Javoozy, unconcious mixologist?",
        (player: Sprite) => List(
          Cutscene.say(player, "He's out cold...")
        )
      )

      if (!Cutscene.in_scene && Schedule.frat_doc_wakeup_time.after) {
        new Cutscene("levels/garden",
          Cutscene.say_focus(self, "Owww, my head...\nNeed a drink..."),
          Cutscene.act(() => {
            flags("mood_partied_out") = true
            state = new ChillAtBar()
          })
        ).start(Some(self))
      }
    }
  }

  class ChillAtBar() extends State {
    plan = new_plan.goto_lvl("atrium").goto(Atrium.bar)

    override def transition(dt: Float) {
      if (self.in_zone("bar")) {
        var near_aqua = false
        for (aqua <- Atrium.world.find("aquastazia") if aqua.in_zone("bar")) {
          near_aqua = true
          if (aqua_flirt) {
            listen("You know I'm a mixologist?", "Went to Harvard...")
          } else {
            listen("Take my number at least?", "... It's 3, by the way.")
          }
        }

        if (!near_aqua) {
          if (flags("mood_spazzy")) {
            talk(
              "You're having more coffee?!",
              "Years of study indicate\njitter is an element of\na finite field,\nso it'll wrap around soon..."
            )
          } else if (flags("mood_crisis_averted")) {
            talk(
              "You nearly drowned Princess Aquastazia in booze!",
              "I deeply regret my actions"
            )
          } else if (flags("mood_partied_out")) {
            talk(
              "Where'd you learn to party so hard?",
              "You don't want to know."
            )
          }
        }
      } else {
        talk("Are you OK?", "...")
      }

      if (Schedule.piano_time.after) {
        state = new ListenPiano()
      }
    }
  }

  class ListenPiano() extends State {
    plan = new_plan.goto(Atrium.piano)

    override def transition(dt: Float) {
      if (self.in_zone("piano") && Hotel.amelia.flags("played_piano")) {
        if (aqua_flirt) {
          listen("Music and drinks, make note")
        } else {
          listen("Makes me feel melancholy...")
        }
      }

      if (Schedule.need_tiara_time.after && !Cutscene.in_scene) {
        if (aqua_flirt) {
          new Cutscene("levels/atrium",
            Cutscene.say_focus(self, "What a tune!"),
            Cutscene.say_focus(Atrium.world.get("aquastazia"), "Say, could YOU go fetch my tiara?"),
            Cutscene.say_focus(self, "Why certainly!"),
            Cutscene.act(() => {
              state = new FindTiara()
            })
          ).start(Some(self))
        } else {
          self.ez_say("Too melancholy! Must pace! FORBODINGLY.")
          state = new WanderUpstairs()
        }
      }
    }
  }

  class WanderUpstairs() extends State {
    plan = new_plan.goto_lvl("floor").roam(Floor.floor, 10)

    override def transition(dt: Float) {
      talk("What's wrong?", "So melancholy!")

      if (Schedule.javoozy_chill_time.after) {
        state = new WaitForDinner()
      }
    }
  }

  class FindTiara() extends State {
    // The flowchart proves the tiara will be there to chase!
    plan = new_plan.goto_lvl("floor").goto(Floor.world.get("tiara"))

    override def transition(dt: Float) {
      if (self.in_zone("tiara")) {
        self.ez_say("Ah, here it is...")
        Floor.world.get("tiara").destroy()
        state = new ReturnTiara()
      }
    }
  }

  class ReturnTiara() extends State {
    plan = new_plan.goto_lvl("atrium").chase(Atrium.world.get("aquastazia"))

    override def transition(dt: Float) {
      val aqua = Atrium.world.get("aquastazia")
      if (!Cutscene.in_scene && current_level == "levels/atrium" && self.nearby(aqua, 100)) {
        new Cutscene("levels/atrium",
          Cutscene.say_focus(self, "Here you go, Princess!"),
          Cutscene.say(aqua, "Oh, thank you!"),
          Cutscene.act(() => {
            flags("returned_tiara") = true
            state = new WaitForDinner()
          })
        ).start(Some(self))
      }
    }
  }

  class WaitForDinner() extends State {
    plan = new_plan.goto_lvl("atrium").goto(Atrium.bar)

    override def transition(dt: Float) {
      if (Schedule.dinner_time.after) {
        state = new Dinner()
      }
    }
  }

  // Y'know, he just got... in a tizzy
  class Fainted() extends State {
    self.behavior = new NullBehavior(self)

    override def transition(dt: Float) {
      if (Schedule.dinner_time.after) {
        state = new Dinner()
      }
    }
  }

  class Dinner() extends State {}

  def spilled_maid(hit: Collision): CollisionResponse = {
    if (!Hotel.game.flags("butler_chased_maid")) {
      // Uh oh...
      Hotel.game.flags("butler_chased_maid") = true

      val butler = Instance("butler", Floor.elevator.get_collider_pos, Nil, "tile").manifest(
        Floor.world, true
      )
      Floor.add(butler)

      val maid = hit.pusher
      new Cutscene("levels/floor",
        Cutscene.say_async(maid, "Ahh, coffee on my uniform!"),
        Cutscene.pause(self),
        Cutscene.say(self, "Coffee's good for ya, y'know!"),
        Cutscene.unpause(self),
        Cutscene.say_focus(
          butler, "You know we can't serve our guests with dirty uniforms. Surrender your clothes."
        ),
        Cutscene.act(() => {
          butler.ez_goto(maid)
        })
      ).start(None)
    }
    return Correct()
  }

  def see_naked_maid(hit: Collision): CollisionResponse = {
    if (Hotel.game.flags("maid_naked") && !flags("fainted_nudity")) {
      flags("fainted_nudity") = true
      Hotel.clock.fast_forward(Schedule.chandelier_crash_time)

      val chandelier = Hotel.atrium.get("chandelier")
      new Cutscene("levels/floor",
        Cutscene.pause(self),
        Cutscene.say(hit.pusher, "Oh, uh, don't mind some skin!"),
        Cutscene.say(hit.other.owner, "Gasp -- you'r -- you're naked!"),
        Cutscene.unpause(self),
        Cutscene.act(() => {
          state = new Fainted()
        }),
        List(new ShakeScreen(5f)),
        Cutscene.dramatic_switch_levels("levels/floor", "levels/atrium", Some(chandelier)),
        Cutscene.act(() => {
          chandelier.ez_shake(2)
        }),
        List(new ShakeScreen(2f)),
        Cutscene.act(() => {
          chandelier.destroy()
          val crashed = Instance(
            "crashed_chandelier", chandelier.get_pos.delta(0, 150), Nil, "topdown"
          ).manifest(Hotel.atrium, true)
          Atrium.add(crashed)
          Hotel.game.flags("chandelier_crashed") = true
          Hotel.amelia.trapped_by_chandelier()
        }),
        List(new Pause(1)),
        Cutscene.dramatic_switch_levels("levels/atrium", "levels/floor", Some(hit.pusher)),
        Cutscene.say(hit.pusher, "Ehh, hope that didn't cause problems...")
      ).start(None)
    }
    return Ghost()
  }

  private def aqua_flirt = flags("caffeinated") || Hotel.aquastazia.flags("happy_splash_time")
}
