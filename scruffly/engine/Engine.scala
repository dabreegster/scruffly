package scruffly.engine

import scruffly.util.JavaPriorityQueue
import scala.collection.mutable

// All delta times are in seconds
trait Component {
  def update(dt: Float) {}
  def render(gfx: DrawModule) {}
  def event(ev: String) {}
  def zorder() = 0f
}

case class GameConfig(title: String, screen_width: Int, screen_height: Int, desired_fps: Int) {
  val dt = 1f / desired_fps
}

abstract class Engine(config: GameConfig) {
  assert(scruffly.Game.engine == null)
  scruffly.Game.engine = this

  private val active_components = new mutable.ListBuffer[Component]()
  private val components_per_state = new mutable.HashMap[String, mutable.ListBuffer[Component]]()
  var current_level = ""

  val gfx: DrawModule
  val assets: AssetsModule
  val collisions: CollisionModule
  val keymap: KeymapModule
  val mouse: MouseModule
  val sfx: SoundModule

  private var accumulated_dt = 0f

  def setup()
  def run()
  def update(dt: Float) {
    // Make every component see the same time-step size all the time
    accumulated_dt += dt
    while (accumulated_dt >= config.dt) {
      active_components.foreach(_.update(config.dt))
      accumulated_dt -= config.dt
    }
  }
  def render() {
    gfx.zordered.clear()
    for (c <- active_components) {
      gfx.zorder_render(c)
    }
    while (gfx.zordered.nonEmpty) {
      gfx.zordered.shift.render(gfx)
    }
  }

  // Component management
  def add_component(c: Component, state: String) {
    if (!components_per_state.contains(state)) {
      components_per_state(state) = new mutable.ListBuffer[Component]()
    }
    if (components_per_state(state).contains(c)) {
      throw new IllegalStateException(s"$state already has $c")
    }
    if (active_components.contains(c)) {
      throw new IllegalStateException(s"active_components already has $c")
    }
    components_per_state(state) += c
    active_components += c
  }
  def safely_add_component(c: Component, state: String) {
    add_component(c, state)
    if (current_level != state) {
      active_components -= c
    }
  }
  def del_component(c: Component, state: String) {
    components_per_state(state) -= c
    active_components -= c
  }
  def activate(state: String) {
    assert(current_level != state)
    active_components ++= components_per_state(state)
    current_level = state
  }
  def deactivate(state: String) {
    active_components --= components_per_state(state)
    current_level = ""
  }
  def has_state(state: String) = components_per_state.contains(state)
  // Recurses on sprites
  def all_components(state: String) = components_per_state(state).toList.flatMap(c => c match {
    case s: scruffly.Sprite => s.all_components
    case _ => List(c)
  })
  def all_components_from_all = components_per_state.keys.flatMap(
    state => all_components(state)
  ).toSet

  // TODO where to put helpers?
  def ez_interact(level: String) = all_components(level).find(_ match {
    case i: scruffly.components.InteractButton => true
    case _ => false
  }).get.asInstanceOf[scruffly.components.InteractButton]
}

// TODO make most (Float, Float) pairs be Pt. it's more displacement vector, but eh.
case class Pt(x: Float, y: Float) {
  def dist(other: Pt) = math.sqrt(math.pow(x - other.x, 2) + math.pow(y - other.y, 2)).toFloat
  def delta(dx: Float, dy: Float) = Pt(x + dx, y + dy)
  def delta(pt: Pt) = Pt(x + pt.x, y + pt.y)
  def around(r: Float) = PixelRectangle(x - r, y - r, x + r, y + r)
}
case class Line(pt1: Pt, pt2: Pt) {
  def x1 = pt1.x
  def y1 = pt1.y
  def x2 = pt2.x
  def y2 = pt2.y
  def points = List(pt1, pt2)
  def length = pt1.dist(pt2)
  def pt(alpha: Double) = Pt(
    scruffly.util.Util.lerp(pt1.x, pt2.x, alpha).toFloat,
    scruffly.util.Util.lerp(pt1.y, pt2.y, alpha).toFloat
  )
}
case class PixelRectangle(x1: Float, y1: Float, x2: Float, y2: Float) {
  def width = x2 - x1
  def height = y2 - y1
  // TODO not used?
  def fix_x =
    if (x1 > x2)
      PixelRectangle(x2, y1, x1, y2)
    else
      this
  def fix_y =
    if (y1 > y2)
      PixelRectangle(x1, y2, x2, y1)
    else
      this
  def around(r: Float) = PixelRectangle(x1 - r, y1 - r, x2 + r, y2 + r)
  def contains(pt: Pt) = pt.x >= x1 && pt.x <= x2 && pt.y >= y1 && pt.y <= y2
  def topleft = Pt(x1, y1)
  def topright = Pt(x2, y1)
  def bottomleft = Pt(x1, y2)
  def bottomright = Pt(x2, y2)
}
case class RGB(r: Int, g: Int, b: Int, alpha: Int = 255)

trait AssetsModule {
  def get_image_dims(fn: String): Pt
}

trait CollisionModule {
  def contains_point(lines: List[Line], x: Float, y: Float): Boolean
  def rect_contains_line(x1: Float, y1: Float, x2: Float, y2: Float, line: Line): Boolean
  def lines_cross(l1: Line, l2: Line): Option[(Float, Float)]
  def polygons_hit(l1: List[Line], l2: List[Line]): Boolean
  // Points from lines must be contig, otherwise all hell...
  def point_in_polygon(lines: List[Line], pt: Pt): Boolean
  def raycast(start: Pt, angle_degrees: Double, lines: List[Line], limit_range: Float): Pt
  def raycast_hit(
    start: Pt, angle_degrees: Double, lines: List[Line], limit_range: Float
  ): Boolean = {
    val angle_radians = math.toRadians(angle_degrees)
    val end = Pt(
      (start.x + limit_range * math.cos(angle_radians)).toFloat,
      (start.y + limit_range * math.sin(angle_radians)).toFloat
    )
    return raycast(start, angle_degrees, lines, limit_range) != end
  }
}

trait KeymapModule {
  // This module can be in one of two states. When we're recording input, keydown() returns false.
  protected var recording = false
  protected var input_buffer: String = ""

  def keydown(key: String): Boolean
  def accumulated_input() = input_buffer
  def start_recording(initial: String) {
    recording = true
    input_buffer = initial
  }
  def stop_recording() {
    recording = false
  }

  val key_left = "left"
  val key_right = "right"
  val key_up = "up"
  val key_down = "down"
  val key_jump = "space"
  val key_debug = "d"
  val key_debug_tile = "e"
  val key_debug_game = "h"
  val key_zoom_in = "]"
  val key_zoom_out = "["
  val key_reset_zoom = "r"
  val key_shoot = "z"
  val key_slow = "left_control"
  val key_interact = "space"
  val key_drop = ","
  val key_tune_down = ","
  val key_tune_up = "."
  val key_push = "left_control"
  val key_fast = "f"
  val key_very_fast = "g"
  val key_choose = "enter"
  val key_skip = "s"

  val key_editor_lock = "space"
  val key_editor_left = "h"
  val key_editor_right = "l"
  val key_editor_up = "k"
  val key_editor_down = "j"
  val key_editor_save = "s"
  val key_editor_delete = "x"
  val key_editor_spawn = "q"
  val key_editor_next = "tab"
  val key_editor_done = "enter"
  val key_editor_form = "w"
  val key_editor_rectangle = "t"
  val key_editor_polygon = "p"
  val key_editor_cancel = "backspace"
}

trait MouseModule {
  val btn_left = "left"
  val btn_right = "right"
  val btn_middle = "middle"

  def button_down(btn: String): Boolean
  def get_screen_pos(): Pt
  def get_map_pos(): Pt
  def scroll_since_last(): Int
}

// For now, a mutable context object
abstract class DrawModule {
  var x_offset = 0f
  var y_offset = 0f
  var scale = 1f
  var debug = false

  val zordered = new JavaPriorityQueue[Component](
    // Don't attempt to break ties the same every tick :\
    (c1: Component, c2: Component) => c1.hashCode.compare(c2.hashCode)
  )
  def zorder_render(c: Component) {
    if (c.zorder == 0) {
      c.render(this)
    } else {
      zordered.insert(c, c.zorder)
    }
  }

  def camera_transforms()
  def screen_transforms()
  def set_translation(x: Float, y: Float)
  def image(fn: String, x: Float = 0, y: Float = 0, filter: RGB = RGB(255, 255, 255))
  def circle(radius: Float, color: RGB, x: Float, y: Float)
  // has a thickness that's not controllable right now
  def unfilled_ellipse(x_radius: Float, y_radius: Float, color: RGB, x: Float, y: Float)
  def line(x1: Float, y1: Float, x2: Float, y2: Float, color: RGB)
  // TODO never closed... client has to pass in closed?
  def polygon(points: List[Pt], filled: Boolean, closed: Boolean, color: RGB)
  // TODO if image isn't a power of two, will have a border...
  def texture_polygon(points: List[Pt], fn: String)
  def lines(lines: Iterable[Line], color: RGB) {
    for (l <- lines) {
      line(l.pt1.x, l.pt1.y, l.pt2.x, l.pt2.y, color)
    }
  }
  def text(line: String, x: Float, y: Float, color: RGB)
  def text_width(line: String): Int
  def text_height(line: String): Int

  def get_screen_width(): Int
  def get_screen_height(): Int

  def get_window() = PixelRectangle(
    x_offset / scale, y_offset / scale,
    (x_offset + get_screen_width) / scale, (y_offset + get_screen_height) / scale
  )

  def screen_to_map(pt: Pt) = Pt(
    (x_offset + pt.x) / scale, (y_offset + pt.y) / scale
  )
  def map_to_screen(pt: Pt) = Pt(
    (pt.x - x_offset) * scale, (pt.y - y_offset) * scale
  )
  def center_map = screen_to_map(Pt(get_screen_width / 2, get_screen_height / 2))
  def center(pt: Pt, bounds: Option[PixelRectangle]) {
    x_offset = (scale * pt.x) - (get_screen_width / 2)
    y_offset = (scale * pt.y) - (get_screen_height / 2)

    for (limit <- bounds) {
      x_offset = math.max(x_offset, limit.x1)
      x_offset = math.min(x_offset, limit.x2 - get_screen_width)
      y_offset = math.max(y_offset, limit.y1)
      y_offset = math.min(y_offset, limit.y2 - get_screen_height)
    }
  }
  def centered_pt = screen_to_map(Pt(get_screen_width / 2, get_screen_height / 2))
}

abstract class SoundModule {
  def play(fn: String)
}
