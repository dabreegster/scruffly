package scruffly.engine

import scala.collection.mutable
import scruffly.components._
import scruffly.util._

import org.newdawn.slick

abstract class SlickEngine(config: GameConfig) extends Engine(config) {
  private val app: slick.AppGameContainer = new slick.AppGameContainer(new slick.BasicGame(config.title) {
    private var ready = false
    override def init(gc: slick.GameContainer) {
      SlickEngine.this.setup()
    }
    override def render(gc: slick.GameContainer, g: slick.Graphics) {
      SlickEngine.this.gfx.reset(gc, g)
      SlickEngine.this.render()
      ready = true
      app.setShowFPS(gfx.debug)
    }
    override def update(gc: slick.GameContainer, delta: Int) {
      if (ready) {
        // Don't have huge updates ever
        SlickEngine.this.update(math.min(.1f, delta.toFloat / 1000))
      }
    }

    override def mouseWheelMoved(change: Int) {
      SlickEngine.this.mouse.wheel_moved(change)
    }

    override def keyPressed(keycode: Int, key: Char) {
      if (key != 0) {
        SlickEngine.this.keymap.key_pressed(key)
      }
    }
  })

  val assets = new SlickAssetsModule()
  val collisions = new SlickCollisionModule()
  val gfx = new SlickDrawModule(assets, collisions)
  val keymap = new SlickKeymapModule(() => app.getInput)
  val mouse = new SlickMouseModule(() => app.getInput, gfx)
  val sfx = new SlickSoundModule()

  override def run() {
    app.setDisplayMode(config.screen_width, config.screen_height, false)
    app.setTargetFrameRate(config.desired_fps)
    app.setShowFPS(false)
    app.start()
  }
}

class SlickDrawModule(assets: SlickAssetsModule, collisions: SlickCollisionModule) extends DrawModule {
  private var gc: slick.GameContainer = null
  private var gfx: slick.Graphics = null

  // TODO tmp for water effect. a hack!
  def get_gfx = gfx

  def reset(container: slick.GameContainer, g: slick.Graphics) {
    gc = container
    gfx = g
    camera_transforms()
  }

  private def use(color: RGB) {
    gfx.setColor(new slick.Color(color.r, color.g, color.b, color.alpha))
  }
  override def camera_transforms() {
    gfx.resetTransform()
    // Account for the camera
    gfx.pushTransform()
    gfx.translate(-x_offset, -y_offset)
    gfx.scale(scale, scale)
    // Push an identity transformation on, since set_translation always pops
    gfx.pushTransform()
  }
  override def screen_transforms() {
    gfx.resetTransform()
  }
  override def set_translation(x: Float, y: Float) {
    gfx.popTransform()
    gfx.pushTransform()
    gfx.translate(x, y)
  }
  override def image(fn: String, x: Float, y: Float, filter: RGB) {
    assets.get_image(fn).draw(x, y, 1, new slick.Color(filter.r, filter.g, filter.b, filter.alpha))
  }
  override def circle(radius: Float, color: RGB, x: Float, y: Float) {
    use(color)
    set_translation(x, y)
    // Center it
    gfx.fillOval(-radius, -radius, 2 * radius, 2 * radius)
  }
  override def unfilled_ellipse(
    x_radius: Float, y_radius: Float, color: RGB, x: Float, y: Float
  ) {
    use(color)
    set_translation(x, y)
    // TODO resetLineWidth fails
    val orig_width = gfx.getLineWidth
    gfx.setLineWidth(5f)
    gfx.drawOval(-x_radius, -y_radius, x_radius * 2, y_radius * 2)
    gfx.setLineWidth(orig_width)
  }
  override def line(x1: Float, y1: Float, x2: Float, y2: Float, color: RGB) {
    use(color)
    gfx.drawLine(x1, y1, x2, y2)
  }
  override def polygon(points: List[Pt], filled: Boolean, closed: Boolean, color: RGB) {
    use(color)
    val poly = new slick.geom.Polygon()
    poly.setClosed(closed)
    for (pt <- points) {
      poly.addPoint(pt.x, pt.y)
    }
    if (filled) {
      gfx.fill(poly)
    } else {
      gfx.draw(poly)
    }
  }
  override def texture_polygon(points: List[Pt], fn: String) {
    val poly = new slick.geom.Polygon()
    poly.setClosed(true)
    for (pt <- points) {
      poly.addPoint(pt.x, pt.y)
    }
    // Have to reset to white, else the image gets distorted
    gfx.setColor(new slick.Color(255, 255, 255))
    // TODO npot textures fill have a gap. texture() with scaleX/scaleY is screwy.
    // TODO looks like it squishes down to 100x100...
    gfx.texture(poly, assets.get_image(fn))
  }
  override def text(line: String, x: Float, y: Float, color: RGB) {
    use(color)
    gfx.drawString(line, x, y)
  }
  override def text_width(line: String) = gfx.getFont.getWidth(line)
  override def text_height(line: String) =
    gfx.getFont.getLineHeight * (1 + line.count(x => x == '\n'))

  override def get_screen_width = gc.getWidth
  override def get_screen_height = gc.getHeight

  private var mask1_img: slick.Image = null
  private var mask1_g: slick.Graphics = null
  // All points in lights are already in screen coordinates
  def make_light_mask(lights: List[Light], ambient_light: RGB, alpha_gradient: (Int, Int)): slick.Image = {
    if (mask1_img == null) {
      mask1_img = new slick.Image(get_screen_width, get_screen_height)
      mask1_g = new slick.opengl.pbuffer.FBOGraphics(mask1_img)
    }

    // Background mask. Overwrite from there.
    mask1_g.setColor(new slick.Color(ambient_light.r, ambient_light.g, ambient_light.b, ambient_light.alpha))
    mask1_g.fillRect(0, 0, get_screen_width, get_screen_height)

    for (light <- lights) {
      use(light.color)
      // Draw triangles
      for ((arc1, arc2) <- (light.cast_rays.last, light.cast_rays.head) :: light.cast_rays.zip(light.cast_rays.tail)) {
        val gradient = new slick.ShapeFill() {
          override def colorAt(shape: slick.geom.Shape, x: Float, y: Float): slick.Color = {
            val dist = light.at.dist(Pt(x, y))
            val scale = dist / light.radius
            return new slick.Color(
              Util.lerp(light.color.r, ambient_light.r, scale).toInt,
              Util.lerp(light.color.g, ambient_light.g, scale).toInt,
              Util.lerp(light.color.b, ambient_light.b, scale).toInt,
              Util.lerp(alpha_gradient._1, alpha_gradient._2, scale).toInt
            )
          }
          override def getOffsetAt(shape: slick.geom.Shape, x: Float, y: Float) =
            new slick.geom.Vector2f(0, 0)
        }
        val triangle = new slick.geom.Polygon()
        triangle.addPoint(light.at.x, light.at.y)
        triangle.addPoint(arc1.x, arc1.y)
        triangle.addPoint(arc2.x, arc2.y)
        mask1_g.fill(triangle, gradient)
      }
    }

    return mask1_img
  }

  private var mask2_img: slick.Image = null
  private var mask2_g: slick.Graphics = null
  def make_circle_overlay(radius: Float): slick.Image = {
    if (mask2_img == null) {
      mask2_img = new slick.Image(get_screen_width, get_screen_height)
      mask2_g = new slick.opengl.pbuffer.FBOGraphics(mask2_img)
    }

    mask2_g.setColor(new slick.Color(0, 0, 0))
    mask2_g.fillRect(0, 0, get_screen_width, get_screen_height)
    // TODO have to use this result in multiply mode I think :(
    mask2_g.setColor(new slick.Color(255, 255, 255, 120))
    mask2_g.fillOval((get_screen_width / 2) - radius, (get_screen_height / 2) - radius, 2 * radius, 2 * radius)

    return mask2_img
  }
}

class SlickAssetsModule() extends AssetsModule {
  private val cache = new mutable.HashMap[String, slick.Image]()

  def get_image(partial_fn: String): slick.Image = {
    val fn = "assets/" + partial_fn
    if (!cache.contains(fn)) {
      cache(fn) = new slick.Image(fn)
    }
    return cache(fn)
  }

  override def get_image_dims(fn: String): Pt = {
    val img = get_image(fn)
    return Pt(img.getWidth, img.getHeight)
  }
}

class SlickCollisionModule() extends CollisionModule {
  override def contains_point(lines: List[Line], x: Float, y: Float): Boolean = {
    // TODO so slow :(
    for (l <- lines) {
      val line = new slick.geom.Line(l.pt1.x, l.pt1.y, l.pt2.x, l.pt2.y)
      val dist = line.distance(new slick.geom.Vector2f(x, y))
      // TODO works in practice.
      if (dist < 1) {
        return true
      }
    }
    return false
  }

  override def rect_contains_line(x1: Float, y1: Float, x2: Float, y2: Float, line_spec: Line): Boolean = {
    val line = new slick.geom.Line(line_spec.pt1.x, line_spec.pt1.y, line_spec.pt2.x, line_spec.pt2.y)
    val rect = new slick.geom.Rectangle(x1, y1, x2 - x1, y2 - y1)
    return rect.intersects(line) || rect.contains(line)
  }

  override def lines_cross(l1: Line, l2: Line): Option[(Float, Float)] = {
    val line1 = new slick.geom.Line(l1.x1, l1.y1, l1.x2, l1.y2)
    val line2 = new slick.geom.Line(l2.x1, l2.y1, l2.x2, l2.y2)
    return Option(line1.intersect(line2, true)).map(vec => (vec.getX, vec.getY))
  }

  override def point_in_polygon(lines: List[Line], pt: Pt): Boolean = {
    val poly = new slick.geom.Polygon()
    poly.setClosed(true)
    for (l <- lines) {
      poly.addPoint(l.pt1.x, l.pt1.y)
      poly.addPoint(l.pt2.x, l.pt2.y)
    }
    return poly.contains(pt.x, pt.y)
  }

  override def polygons_hit(l1: List[Line], l2: List[Line]): Boolean = {
    val poly1 = new slick.geom.Polygon()
    poly1.setClosed(true)
    for (l <- l1) {
      poly1.addPoint(l.pt1.x, l.pt1.y)
      poly1.addPoint(l.pt2.x, l.pt2.y)
    }

    val poly2 = new slick.geom.Polygon()
    poly2.setClosed(true)
    for (l <- l2) {
      poly2.addPoint(l.pt1.x, l.pt1.y)
      poly2.addPoint(l.pt2.x, l.pt2.y)
    }

    return poly1.intersects(poly2)
  }

  override def raycast(start: Pt, angle_degrees: Double, lines: List[Line], limit_range: Float): Pt = {
    val angle_radians = math.toRadians(angle_degrees)
    val end = Pt(
      (start.x + limit_range * math.cos(angle_radians)).toFloat,
      (start.y + limit_range * math.sin(angle_radians)).toFloat
    )

    val ray = new slick.geom.Line(start.x, start.y, end.x, end.y)

    val hits = end :: lines.flatMap(l => {
      val line = new slick.geom.Line(l.pt1.x, l.pt1.y, l.pt2.x, l.pt2.y)
      val hit = ray.intersect(line, true)
      if (hit != null) {
        Some(Pt(hit.x, hit.y))
      } else {
        None
      }
    })
    return hits.minBy(pt => pt.dist(start))
  }
}

class SlickKeymapModule(input: () => slick.Input) extends KeymapModule {
  override def keydown(key: String) = !recording && input().isKeyDown(key match {
    // special keys
    case "left" => slick.Input.KEY_LEFT
    case "right" => slick.Input.KEY_RIGHT
    case "up" => slick.Input.KEY_UP
    case "down" => slick.Input.KEY_DOWN
    case "space" => slick.Input.KEY_SPACE
    case "]" => slick.Input.KEY_RBRACKET
    case "[" => slick.Input.KEY_LBRACKET
    case "left_control" => slick.Input.KEY_LCONTROL
    case "," => slick.Input.KEY_COMMA
    case "." => slick.Input.KEY_PERIOD
    case "tab" => slick.Input.KEY_TAB
    case "enter" => slick.Input.KEY_ENTER
    case "backspace" => slick.Input.KEY_BACK

    // alphabet
    case "a" => slick.Input.KEY_A
    case "b" => slick.Input.KEY_B
    case "c" => slick.Input.KEY_C
    case "d" => slick.Input.KEY_D
    case "e" => slick.Input.KEY_E
    case "f" => slick.Input.KEY_F
    case "g" => slick.Input.KEY_G
    case "h" => slick.Input.KEY_H
    case "i" => slick.Input.KEY_I
    case "j" => slick.Input.KEY_J
    case "k" => slick.Input.KEY_K
    case "l" => slick.Input.KEY_L
    case "m" => slick.Input.KEY_M
    case "n" => slick.Input.KEY_N
    case "o" => slick.Input.KEY_O
    case "p" => slick.Input.KEY_P
    case "q" => slick.Input.KEY_Q
    case "r" => slick.Input.KEY_R
    case "s" => slick.Input.KEY_S
    case "t" => slick.Input.KEY_T
    case "u" => slick.Input.KEY_U
    case "v" => slick.Input.KEY_V
    case "w" => slick.Input.KEY_W
    case "x" => slick.Input.KEY_X
    case "y" => slick.Input.KEY_Y
    case "z" => slick.Input.KEY_Z

    // Numbers
    case "1" => slick.Input.KEY_1
    case "2" => slick.Input.KEY_2
    case "3" => slick.Input.KEY_3
    case "4" => slick.Input.KEY_4
    case "5" => slick.Input.KEY_5
    case "6" => slick.Input.KEY_6
    case "7" => slick.Input.KEY_7
    case "8" => slick.Input.KEY_8
    case "9" => slick.Input.KEY_9
    case "0" => slick.Input.KEY_0
  })

  def key_pressed(key: Char) {
    if (key.toInt == 13) {
      // Carriage return is weird
      input_buffer += "\n"
    } else if (key.toInt == 8 || key.toInt == 127) {
      // So is backspace
      input_buffer = input_buffer.dropRight(1)
    } else {
      input_buffer += key
    }
  }
}

class SlickMouseModule(input: () => slick.Input, gfx: DrawModule) extends MouseModule {
  private var accumulated_scroll = 0

  override def button_down(btn: String) = input().isMouseButtonDown(btn match {
    case `btn_left` => slick.Input.MOUSE_LEFT_BUTTON
    case `btn_right` => slick.Input.MOUSE_RIGHT_BUTTON
    case `btn_middle` => slick.Input.MOUSE_MIDDLE_BUTTON
  })
  override def get_screen_pos() = Pt(input().getMouseX, input().getMouseY)
  override def get_map_pos() = gfx.screen_to_map(get_screen_pos)
  override def scroll_since_last(): Int = {
    val scroll = accumulated_scroll
    accumulated_scroll = 0
    return scroll
  }

  def wheel_moved(delta: Int) {
    accumulated_scroll += delta
  }
}

class SlickSoundModule() extends SoundModule {
  override def play(fn: String) {
    new slick.Sound("assets/" + fn).play()
  }
}
