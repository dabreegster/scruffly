package scruffly

import scruffly.engine._
import scruffly.util._

abstract class PhysicsModule(val sprite: Sprite) {
  // Speeds/accelerations below are pixel/frame, and multiplying by this (the fps desired) makes
  // pixels/second
  protected val param_scale = 60f

  def adjust_speed(dt: Float, move_requested_x: Boolean, move_requested_y: Boolean) {}
  //def handle_contact(top: Boolean, bottom: Boolean, left: Boolean, right: Boolean) {}

  // Collision stuff
  def debug_render(gfx: DrawModule) {}
  def get_collider(): Platform
  def movement_hitbox(dx: Float, dy: Float): PixelRectangle
  def get_collider_pos(): Pt
  def get_collision_lines: List[Line]
  def get_hitbox(): PixelRectangle
}

case class Platform(lines: List[Line], topleft_x: Float, topleft_y: Float) {
  def get_points = get_lines.flatMap(_.points)
  def get_min_x = get_points.map(_.x).min
  def get_max_x = get_points.map(_.x).max
  def get_min_y = get_points.map(_.y).min
  def get_max_y = get_points.map(_.y).max
  def get_hitbox = PixelRectangle(get_min_x, get_min_y, get_max_x, get_max_y)

  def get_lines = lines.map(l => Line(
    Pt(l.pt1.x + topleft_x, l.pt1.y + topleft_y), Pt(l.pt2.x + topleft_x, l.pt2.y + topleft_y)
  ))
}

abstract class MovingPhysicsModule(
  lines: List[Line], sprite: Sprite
) extends PhysicsModule(sprite) with GeometryFactory
{
  override def get_collider() = Platform(
    lines, sprite.position_x, sprite.position_y
  )
  override def get_collider_pos(): Pt = {
    val center = calc_center(lines.flatMap(_.points))
    return Pt(sprite.position_x + center.x, sprite.position_y + center.y)
  }
  override def get_collision_lines = lines
  override def get_hitbox = get_collider.get_hitbox

  override def debug_render(gfx: DrawModule) {
    gfx.lines(lines, RGB(0, 0, 255))
  }

  // Not transformed to position!
  private lazy val bbox = get_collision_dims
  private def get_collision_dims(): PixelRectangle = {
    val pts = lines.flatMap(_.points)
    val min_x = pts.map(_.x).min
    val max_x = pts.map(_.x).max
    val min_y = pts.map(_.y).min
    val max_y = pts.map(_.y).max
    return PixelRectangle(min_x, min_y, max_x, max_y)
  }

  override def movement_hitbox(dx: Float, dy: Float) = PixelRectangle(
    math.min(sprite.position_x + bbox.x1, sprite.position_x + bbox.x1 + dx),
    math.min(sprite.position_y + bbox.y1, sprite.position_y + bbox.y1 + dy),
    math.max(sprite.position_x + bbox.x2, sprite.position_x + bbox.x2 + dx),
    math.max(sprite.position_y + bbox.y2, sprite.position_y + bbox.y2 + dy)
  )
}
