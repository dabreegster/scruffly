package scruffly

import scruffly.engine._

// Global singleton so we don't have to pipe stuff everywhere
object Game {
  var engine: Engine = null
  var assets: AssetManager = null

  val errors = new scruffly.components.ErrorAggregator()
}
