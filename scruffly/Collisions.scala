package scruffly

import scruffly.engine._

object CollisionHandlers {
  def push(hit: Collision): CollisionResponse = {
    // Make the other sprite move
    val moved = hit.other.move(hit.requested)
    // Only retry the movement if the other thing budged
    return if (moved) Retry() else Correct()
  }

  def push_with_key(hit: Collision) =
    if (Game.engine.keymap.keydown(Game.engine.keymap.key_push))
      push(hit)
    else
      normal_hit(hit)

  def bounce(hit: Collision): CollisionResponse = {
    // TODO how much? relate to dt? accel_forward? remember higher is still bound by max_speed.
    hit.pusher.speed_x = -5000 * hit.prescribed._1
    hit.pusher.speed_y = -5000 * hit.prescribed._2
    return Correct()
  }

  def ghost(hit: Collision) = Ghost()
  def normal_hit(hit: Collision) = Correct()
}

// The methods here are pure
object SpriteCollisionModule {
  // req_ are really delta positions
  def apply(sprite: Sprite, obj: CollisionTest, req_dx: Float, req_dy: Float): (Float, Float) = {
    if (req_dx == 0 && req_dy == 0) {
      return (0, 0)
    }

    //println(s"____________request is $req_dx, $req_dy")

    var dx = req_dx
    var dy = req_dy

    for (line <- sprite.physics.get_collision_lines) {
      val pts = sample_points(line)
      assert(pts.nonEmpty)
      for (pt <- pts) {
        val test_line = Line(
          Pt(sprite.position_x + pt.x, sprite.position_y + pt.y),
          Pt(sprite.position_x + pt.x + dx, sprite.position_y + pt.y + dy)
        )
        //println(s"  ---$dir[$pt] means $test_line. by now, $dx, $dy is the req)")

        for (hit <- obj.hit_lines(test_line)) {
          val raw_correction_x = hit._1 - sprite.position_x - pt.x
          val raw_correction_y = hit._2 - sprite.position_y - pt.y
          //println(s"    hit! $hit means $raw_correction_x, $raw_correction_y")

          // Move back one pixel to actually _avoid_ the collision
          val shifted_correction_x =
            if (raw_correction_x < 0)
              raw_correction_x + 1
            else if (raw_correction_x > 0)
              raw_correction_x - 1
            else
              raw_correction_x
          val shifted_correction_y =
            if (raw_correction_y < 0)
              raw_correction_y + 1
            else if (raw_correction_y > 0)
              raw_correction_y - 1
            else
              raw_correction_y

          // Don't correct in the wrong direction
          val correction_x =
            if ((req_dx <= 0 && shifted_correction_x > 0) || (req_dx >= 0 && shifted_correction_x < 0))
              0
            else
              shifted_correction_x
          val correction_y =
            if ((req_dy <= 0 && shifted_correction_y > 0) || (req_dy >= 0 && shifted_correction_y < 0))
              0
            else
              shifted_correction_y
          //println(s"      corrected hit: $correction_x, $correction_y")

          val old_dist = math.sqrt(math.pow(dx, 2) + math.pow(dy, 2))
          val new_dist = math.sqrt(math.pow(correction_x, 2) + math.pow(correction_y, 2))

          // TODO or if correction_x.abs < dx.abs...
          if (new_dist < old_dist) {
            dx = correction_x
            dy = correction_y

            // TODO for slideyness, have the if's. but it may be buggy.
            /*if (dir.horizontal) {
              dx = correction_x
            }
            if (dir.vertical) {
              dy = correction_y
            }*/
          }
        }
      }
    }
    //println(s"======================$req_dx, $req_dy became $dx, $dy")
    return (dx, dy)
  }

  private def sample_points(l: Line) = List(.0, .2, .4, .6, .8, 1).map(alpha => l.pt(alpha))
}
