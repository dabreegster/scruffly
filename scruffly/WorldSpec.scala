package scruffly

import scruffly.engine._
import scruffly.util._
import scruffly.components._

import scala.collection.mutable
import scala.util.parsing.combinator._
import java.io.{PrintWriter, FileWriter, File}
import scala.io.Source

case class WorldSpec(name: String, sprites: List[Instance]) {
  def make_tile_world(
    size_per_tile: Pt, must_avoid: Set[String], should_avoid: Set[String], use_effects: Boolean = true
  ): World = {
    val world = new TileWorld(name, must_avoid, should_avoid)
    val (scenery_instances, other_instances) = sprites.partition(
      i => Game.assets(i.asset, i.effects, use_effects).layer == "scenery"
    )
    val scenery_sprites = scenery_instances.map(_.manifest(world, use_effects))

    world.map = TileFactory.autotile(scenery_sprites, size_per_tile)
    for (instance <- other_instances) {
      instance.manifest(world, use_effects)
    }
    return world
  }

  def save(fn: String) {
    val w = new PrintWriter(new FileWriter(new File(fn)), true /* autoFlush */)
    w.println(toString)
    w.close()
  }

  override def toString =
    "WorldSpec(%s,\n  Sprites(\n%s\n  )\n)".format(
      name, sprites.map("    " + _.toString).mkString(",\n")
    )
}
object WorldSpec {
  def load(fn: String) = WorldSpecParser(Source.fromFile(fn).getLines.mkString("\n"))
}

class AssetManager() extends GeometryFactory {
  assert(Game.assets == null)
  Game.assets = this

  private val assets = new mutable.HashMap[String, Asset]()
  private val plain_effects = new mutable.HashMap[String, (Sprite) => Unit]()
  private val param_effects = new mutable.HashMap[String, (Sprite, String) => Unit]()

  add_effect("serve_zone", (s: Sprite) => {})

  def apply(name: String, effects: List[String], use_effects: Boolean) =
    assets.getOrElse(name, dynamic_asset(name, effects, use_effects))
  def add(asset: Asset) {
    assets(asset.name) = asset
  }
  def add_effect(name: String, effect: (Sprite) => Unit) {
    plain_effects(name) = effect
  }
  def add_effect(name: String, effect: (Sprite, String) => Unit) {
    param_effects(name) = effect
  }
  // no order defined
  def names = assets.keys.toList
  def apply_effect(effect: String, sprite: Sprite) {
    plain_effects(effect)(sprite)
  }
  def apply_effect(effect: String, sprite: Sprite, param: String) {
    param_effects(effect)(sprite, param)
  }
  def has_param_effect(effect: String) = param_effects.contains(effect)

  def dynamic_asset(name: String, effects: List[String], use_effects: Boolean): Asset = {
    val pieces = name.split("_")
    val pts = pieces.head match {
      case "rectangle" => rect(pieces(1).toFloat, pieces(2).toFloat)
      case "polygon" => pieces.tail.map(pt => {
        val coords = pt.split("/")
        Pt(coords(0).toFloat, coords(1).toFloat)
      }).toList
      case _ => throw new IllegalArgumentException(s"Asset $name undefined")
    }
    if (use_effects && effects.contains("serve_zone")) {
      return Asset(name, "zone", NullRenderer(), close(pts), Nil)
    } else {
      return Asset(
        name, "scenery", PolygonRenderer(pts, true, true, RGB(153, 76, 0, 120)),
        close(pts), Nil
      )
    }
  }
}

case class Instance(asset: String, start: Pt, effects: List[String], physics: String) {
  def manifest(world: World, use_effects: Boolean): Sprite = {
    val template = Game.assets(asset, effects, use_effects)

    val layer = effects
      .find(_.startsWith("layer="))
      .map(_.stripPrefix("layer="))
      .getOrElse(template.layer)

    // Renderer may keep state, so just clone...
    val sprite = new Sprite(this, layer, template.renderer.cloneme, world)
    sprite.physics = physics match {
      case "topdown" => new TopdownPhysicsModule(template.topdown_lines, sprite)
      case "tile" => new TilePhysicsModule(template.topdown_lines, sprite)
      case "platformer" => new PlatformerPhysicsModule(template.platformer_lines, sprite)
    }
    // Apply effects
    for (effect <- effects) {
      if (effect.contains("=")) {
        val Array(key, value) = effect.split("=")
        // Is it an effect, or data?
        if (Game.assets.has_param_effect(key)) {
          if (use_effects) {
            Game.assets.apply_effect(key, sprite, value)
          }
        } else {
          sprite.data(key) = value
        }
      } else if (use_effects) {
        Game.assets.apply_effect(effect, sprite)
      }
    }
    // Set up
    world.insert(sprite)
    sprite.warp(start)
    return sprite
  }
}

case class Asset(
  name: String, layer: String, renderer: Renderer,
  topdown_lines: List[Line], platformer_lines: List[Line]
) {
  def recommend_physics =
    if (topdown_lines.nonEmpty)
      "topdown"
    else
      "platformer"
}

trait CommonParser extends RegexParsers {
  val name: Parser[String] = """[ a-zA-Z0-9_/\.#=\-']+""".r
  val int: Parser[Int] = """\-*\d+""".r ^^ { _.toInt }
  val float: Parser[Float] = """\-?\d+(\.\d+)?""".r ^^ { _.toFloat }
  val boolean: Parser[Boolean] = """(true|false)""".r ^^ {
    case "true" => true
    case "false" => false
  }
  val rgb: Parser[RGB] = "RGB(" ~ int ~ "," ~ int ~ "," ~ int ~ ")" ^^ {
    case _ ~ r ~ _ ~ g ~ _ ~ b ~ _ => RGB(r, g, b)
  }

  val pt: Parser[Pt] = "Pt(" ~ float ~ "," ~ float ~ ")" ^^ {
    case _ ~ x ~ _ ~ y ~ _ => Pt(x, y)
  }
  val line: Parser[Line] = "Line(" ~ pt ~ "," ~ pt ~ ")" ^^ {
    case _ ~ pt1 ~ _ ~ pt2 ~ _ => Line(pt1, pt2)
  }
  val tile: Parser[TileCoordinate] = "TileCoordinate(" ~ int ~ "," ~ int ~ ")" ^^ {
    case _ ~ x ~ _ ~ y ~ _ => TileCoordinate(x, y)
  }
}

object WorldSpecParser extends CommonParser {
  val effects: Parser[List[String]] = "List(" ~ repsep(name, ",") ~ ")" ^^ {
    case _ ~ ls ~ _ => ls
  }
  val sprite_instance: Parser[Instance] =
    "Instance(" ~ name ~ "," ~ pt ~ "," ~ effects ~ "," ~ name ~ ")" ^^ {
    case _ ~ id ~ _ ~ at ~ _ ~ fx ~ _ ~ physics ~ _ => Instance(id, at, fx, physics)
  }

  val world: Parser[WorldSpec] =
    ("WorldSpec(" ~ name ~ "," ~ "Sprites(" ~ repsep(sprite_instance, ",") ~ ")" ~ ")") ^^ {
      case _ ~ id ~ _ ~ _ ~ ls ~ _ ~ _ => WorldSpec(id, ls)
    }
  def apply(input: String) = parseAll(world, input) match {
    case Success(result, _) => result
    case failure: NoSuccess => scala.sys.error(failure.msg)
  }
}
