package scruffly

import scruffly.engine._
import scruffly.util._
import scala.collection.mutable

case class Tile(platforms: List[CollisionTest])
case class TileCoordinate(x: Int, y: Int) {
  def asPt = Pt(x, y)
  def left = TileCoordinate(x - 1, y)
  def right = TileCoordinate(x + 1, y)
  def up = TileCoordinate(x, y - 1)
  def down = TileCoordinate(x, y + 1)
}
object TileCoordinate {
  val tie_breaker = (pt1: TileCoordinate, pt2: TileCoordinate) =>
    if (pt1.x < pt2.x)
      -1
    else if (pt1.x > pt2.x)
      1
    else if (pt1.y < pt2.y)
      -1
    else if (pt1.y > pt2.y)
      1
    else
      0
}

class Tilemap(val dims: TileCoordinate, val size_per_tile: Pt) extends Component {
  val tiles = Array.fill(dims.x, dims.y)(new Tile(Nil))
  val sprites = select_all.map(pt => pt -> new mutable.HashSet[Sprite]()).toMap

  // TODO also center at origin, make input transform before passing into autotile!
  var x_off = 0f
  var y_off = 0f

  val debug_tiles = new mutable.HashMap[TileCoordinate, RGB]()

  def add_sprite(s: Sprite) {
    for (pt <- select(s.get_hitbox)) {
      sprites(pt) += s
    }
  }
  def del_sprite(s: Sprite) {
    for (pt <- select(s.get_hitbox)) {
      sprites(pt) -= s
    }
  }

  def topleft_pixel(pt: TileCoordinate) = Pt(
    (pt.x * size_per_tile.x) - x_off,
    (pt.y * size_per_tile.y) - y_off
  )
  def mid_pixel(pt: TileCoordinate) = Pt(
    ((pt.x + 0.5f) * size_per_tile.x) - x_off,
    ((pt.y + 0.5f) * size_per_tile.y) - y_off
  )
  def random_pixel(pt: TileCoordinate) = Pt(
    ((pt.x + RNG.float(0.2f, 0.8f)) * size_per_tile.x) - x_off,
    ((pt.y + RNG.float(0.2f, 0.8f)) * size_per_tile.y) - y_off
  )
  def unpixel(pt: Pt) = TileCoordinate(
    ((pt.x + x_off) / size_per_tile.x).toInt, ((pt.y + y_off) / size_per_tile.y).toInt
  )
  def select(box: PixelRectangle): Iterable[TileCoordinate] = {
    val x1 = math.max((box.x1 + x_off) / size_per_tile.x, 0).toInt
    val y1 = math.max((box.y1 + y_off) / size_per_tile.y, 0).toInt
    val x2 = math.min((box.x2 + x_off) / size_per_tile.x, dims.x - 1).toInt
    val y2 = math.min((box.y2 + y_off) / size_per_tile.y, dims.y - 1).toInt
    return for (
      x <- x1.to(x2); y <- y1.to(y2)
    ) yield TileCoordinate(x, y)
  }
  def select_all =
    for (x <- 0.until(dims.x); y <- 0.until(dims.y))
      yield TileCoordinate(x, y)

  def get(pt: TileCoordinate) = tiles(pt.x)(pt.y)
  def set(pt: TileCoordinate, tile: Tile) {
    tiles(pt.x)(pt.y) = tile
  }

  override def render(gfx: DrawModule) {
    if (gfx.debug) {
      gfx.set_translation(0, 0)
      for (pt <- select(gfx.get_window)) {
        val x1 = -x_off + pt.x * size_per_tile.x
        val y1 = -y_off + pt.y * size_per_tile.y
        val x2 = -x_off + (pt.x + 1) * size_per_tile.x
        val y2 = -y_off + (pt.y + 1) * size_per_tile.y
        val color = RGB(255, 255, 0, 120)
        if (debug_tiles.contains(pt)) {
          gfx.polygon(List(Pt(x1, y1), Pt(x2, y1), Pt(x2, y2), Pt(x1, y2)), true, true, debug_tiles(pt))
        //} else if (get(pt).platforms.isEmpty) {
          //gfx.polygon(List(Pt(x1, y1), Pt(x2, y1), Pt(x2, y2), Pt(x1, y2)), true, true, RGB(255, 0, 255, 120))
        } else {
          gfx.line(x1, y1, x2, y1, color)
          gfx.line(x1, y1, x1, y2, color)
          gfx.line(x2, y1, x2, y2, color)
          gfx.line(x1, y2, x2, y2, color)
        }
      }
    }
    debug_tiles.clear()
  }
  override def zorder = scruffly.hotel.Hotel.tile_zorder

  // Should be much bigger than normal costs (tile size)
  private val avoid_penalty = 100000
  // Low cost to discourage zig-zagging
  private val zigzag_penalty = 10
  // TODO zig-zagging still occurs because we recompute path often
  def in_bounds(tile: TileCoordinate) =
    tile.x >= 0 && tile.x < dims.x && tile.y >= 0 && tile.y < dims.y
  def ok_goto(
    tile: TileCoordinate, must_avoid: Set[String], goal: Option[TileCoordinate]
  ) = (tile == goal.getOrElse(null) ||
    (get(tile).platforms.isEmpty && !sprites(tile).exists(s => must_avoid.contains(s.layer))))
  def path(
    from: TileCoordinate, to: TileCoordinate, must_avoid: Set[String], should_avoid: Set[String]
  ) = AStar.path(Pathfind(
    start = from,
    goal = Some(to),
    successors = (at: TileCoordinate) => adjacent4(at).filter(
      pt => ok_goto(pt, must_avoid, Some(to))
    ),
    calc_cost = (pt1: TileCoordinate, pt2: TileCoordinate, pt1_backref: TileCoordinate) =>
      (pt1.asPt.dist(pt2.asPt) +
       (if (sprites(pt2).exists(s => should_avoid.contains(s.layer))) avoid_penalty else 0) +
       (if (pt2.x != pt1_backref.x && pt2.y != pt1_backref.y) zigzag_penalty else 0)
      ),
    calc_heuristic = (pt1: TileCoordinate) => pt1.asPt.dist(to.asPt),
    tie_breaker = TileCoordinate.tie_breaker
  ))
  def path(
    from: Pt, to: Pt, must_avoid: Set[String], should_avoid: Set[String]
  ): List[TileCoordinate] = path(unpixel(from), unpixel(to), must_avoid, should_avoid)

  def adjacent4(pt: TileCoordinate) = List(
    TileCoordinate(pt.x - 1, pt.y),
    TileCoordinate(pt.x + 1, pt.y),
    TileCoordinate(pt.x, pt.y - 1),
    TileCoordinate(pt.x, pt.y + 1)
  ).filter(pt => pt.x >= 0 && pt.x < dims.x && pt.y >= 0 && pt.y < dims.y)
  def adjacent4_with_self(pt: TileCoordinate) = pt :: adjacent4(pt)

  def random_pt = TileCoordinate(RNG.int(0, dims.x), RNG.int(0, dims.y))
}

class TileWorld(
  level: String, must_avoid: Set[String], should_avoid: Set[String]
) extends World(level, must_avoid, should_avoid) {
  var map: Tilemap = null

  override def select_scenery(hitbox: PixelRectangle) =
    map.select(hitbox).flatMap(pt => map.get(pt).platforms).toList
  override def get_tilemap = Some(map)

  override def get_layer(layer: String, hitbox: PixelRectangle): List[CollisionTest] =
    if (layer == "scenery")
      select_scenery(hitbox)
    else
      map.select(hitbox).flatMap(pt => map.sprites(pt).filter(s => s.layer == layer)).map(
        s => CollisionTest(s, None)
      ).toList
  override def insert(obj: Sprite) {
    super.insert(obj)
    if (obj.layer != "scenery") {
      map.add_sprite(obj)
    }
  }
  override def destroy(obj: Sprite) {
    super.destroy(obj)
    map.del_sprite(obj)
  }
  override def before_move(obj: Sprite) {
    // TODO it's fine to miss this if we haven't set up yet... only scenery will get added, which
    // shouldn't be added/deleted normally anyway
    if (map != null) {
      map.del_sprite(obj)
    }
  }
  override def after_move(obj: Sprite) {
    if (map != null) {
      map.add_sprite(obj)
    }
  }
}

object TileFactory {
  def autotile(raw_platforms: List[Sprite], size_per_tile: Pt): Tilemap = {
    val platforms = raw_platforms.filter(_.get_collider.get_lines.nonEmpty)
    // Need to stretch min bounds to (0, 0)
    val min_x = math.min(0, platforms.map(_.get_collider.get_min_x).min)
    val max_x = platforms.map(_.get_collider.get_max_x).max
    val min_y = math.min(0, platforms.map(_.get_collider.get_min_y).min)
    val max_y = platforms.map(_.get_collider.get_max_y).max
    val width = max_x - min_x
    val height = max_y - min_y
    val num_tiles = TileCoordinate(
      1 + math.max(1, math.ceil(width / size_per_tile.x).toInt),
      1 + math.max(1, math.ceil(height / size_per_tile.y).toInt)
    )
    // TODO do away with offsets entirely. make mandys game and my floor both work, for now.
    val off_x = if (min_x < 0) 0 - min_x else 0
    val off_y = if (min_y < 0) 0 - min_y else 0

    val map = new Tilemap(num_tiles, size_per_tile)
    map.x_off = off_x
    map.y_off = off_y

    // If there's lots of tiles, it's expensive to match all of them to platforms, so match smartly
    val matches = map.select_all.map(pt => pt -> new mutable.ListBuffer[Sprite]()).toMap
    for (p <- platforms) {
      for (pt <- map.select(p.get_hitbox)) {
        matches(pt) += p
      }
    }

    for (tile_coord <- map.select_all) {
      val x1 = -off_x + size_per_tile.x * tile_coord.x
      val x2 = -off_x + size_per_tile.x * (tile_coord.x + 1)
      val y1 = -off_y + size_per_tile.y * tile_coord.y
      val y2 = -off_y + size_per_tile.y * (tile_coord.y + 1)

      // TODO breaks for mandys game
      //val tile = new Tile(matches(tile_coord).map(p => CollisionTest(
      val tile = new Tile(platforms.map(p => CollisionTest(
        p,
        Some(p.get_collider.get_lines.filter(line => Game.engine.collisions.rect_contains_line(x1, y1, x2, y2, line)))
      )).filter(_.lines.get.nonEmpty).toList)
      map.set(tile_coord, tile)
    }
    return map
  }
}
