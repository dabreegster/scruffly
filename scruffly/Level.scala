package scruffly

import scruffly.engine._

import scala.collection.mutable

case class Collision(pusher: Sprite, other: Sprite, requested: (Float, Float), prescribed: (Float, Float))
case class CollisionTest(obj: Sprite, lines: Option[List[Line]]) {
  // TODO head option isnt necessarily best
  def hit_lines(line: Line) = lines.getOrElse(obj.get_collider.get_lines)
    .flatMap(l => Game.engine.collisions.lines_cross(l, line)).headOption
}
case class CollisionHandler(layer1: String, layer2: String, callback: Collision => CollisionResponse)

sealed trait CollisionResponse
// Re-run the check after the 'other' sprite has perhaps moved
case class Retry() extends CollisionResponse
// Correct the collision normally
case class Correct() extends CollisionResponse
// Pretend the sprites didn't collide
case class Ghost() extends CollisionResponse

class World(val level: String, val must_avoid: Set[String], val should_avoid: Set[String]) {
  // Scenery is a bit of a special layer
  var all_sprites: List[Sprite] = Nil
  def select_scenery(hitbox: PixelRectangle): List[CollisionTest] =
    all_sprites.filter(_.layer == "scenery").map(obj => CollisionTest(obj, None))

  // Just for z-order component rendering...
  def get_tilemap(): Option[Tilemap] = None

  lazy val bounds = calc_bounds

  private def calc_bounds(): PixelRectangle = {
    val platforms = all_sprites.filter(_.layer == "scenery").filter(_.get_collider.get_lines.nonEmpty)
    // Need to stretch min bounds to (0, 0)
    val min_x = math.min(0, platforms.map(_.get_collider.get_min_x).min)
    val max_x = platforms.map(_.get_collider.get_max_x).max
    val min_y = math.min(0, platforms.map(_.get_collider.get_min_y).min)
    val max_y = platforms.map(_.get_collider.get_max_y).max
    return PixelRectangle(min_x, min_y, max_x, max_y)
  }

  private val layers = new mutable.HashMap[String, mutable.Set[Sprite]] with mutable.MultiMap[String, Sprite]
  def insert(obj: Sprite) {
    layers.addBinding(obj.layer, obj)
    all_sprites :+= obj
  }
  def destroy(obj: Sprite) {
    layers.removeBinding(obj.layer, obj)
    all_sprites = all_sprites.filter(x => x != obj)
  }
  def before_move(obj: Sprite) {}
  def after_move(obj: Sprite) {}
  def get_layer(layer: String, hitbox: PixelRectangle): List[CollisionTest] =
    if (layer == "scenery")
      select_scenery(hitbox)
    else
      layers.getOrElse(layer, Nil).map(s => CollisionTest(s, None)).toList

  private val handlers = new mutable.ListBuffer[CollisionHandler]
  def handle_collision(layer1: String, layer2: String, callback: Collision => CollisionResponse) {
    if (handlers.exists(h => h.layer1 == layer1 && h.layer2 == layer2)) {
      println(s"WARNING: Collision between $layer1 and $layer2 in $level already registered!")
    }
    handlers += CollisionHandler(layer1, layer2, callback)
  }
  def handle_collisions(layer1: Set[String], layer2: String, callback: Collision => CollisionResponse) {
    for (l1 <- layer1) {
      handle_collision(l1, layer2, callback)
    }
  }
  def handle_collisions(layer1: String, layer2: Set[String], callback: Collision => CollisionResponse) {
    for (l2 <- layer2) {
      handle_collision(layer1, l2, callback)
    }
  }
  def handle_collisions(
    layer1: Set[String], layer2: Set[String], callback: Collision => CollisionResponse,
    except: Set[(String, String)] = Set()
  ) {
    for (l1 <- layer1; l2 <- layer2) {
      if (!except.contains((l1, l2))) {
        handle_collision(l1, l2, callback)
      }
    }
  }
  def add_collisions(ls: List[CollisionHandler]) {
    handlers ++= ls
  }
  def del_collisions(ls: List[CollisionHandler]) {
    handlers --= ls
  }

  def get_colliders(obj: Sprite, hitbox: PixelRectangle): List[(CollisionHandler, CollisionTest)] = handlers
    .filter(h => h.layer1 == obj.layer)
    .flatMap(h => get_layer(h.layer2, hitbox).map(other => (h, other)))
    .toList

  // only client right now is editor, searches in reverse!
  def find_hits(pt: Pt) =
    all_sprites.reverse.find(obj => Game.engine.collisions.point_in_polygon(obj.get_collider.get_lines, pt))
  def all_hits(pt: Pt) =
    all_sprites.filter(obj => Game.engine.collisions.point_in_polygon(obj.get_collider.get_lines, pt))

  def get(layer: String) = all_sprites.find(_.layer == layer).get
  def find(layer: String) = all_sprites.find(_.layer == layer)
  def get_zone(zone: String) = all_sprites.find(_.get("zone") == zone).get

  def to_spec = WorldSpec(
    level,
    all_sprites.map(obj => Instance(
      obj.instance.asset, Pt(obj.position_x, obj.position_y), obj.instance.effects,
      obj.instance.physics
    ))
  )

  def all_waypts(tag: String) = all_sprites.filter(x => x.get("waypt") == tag)

  def check_collisions() {
    val all_layers = all_sprites.map(_.layer).toSet
    val all_possible_hits = for (l1 <- all_layers; l2 <- all_layers) yield (l1, l2)
    val current_hits = handlers.map(h => (h.layer1, h.layer2)).toSet
    val missing = all_possible_hits -- current_hits
    println(s"$level doesn't have collision handlers for:")
    println(missing.toList.sorted.map(x => "  " + x).mkString("\n"))
  }
}
