package scruffly

trait Savable {
  def save(): String
  def load(raw: String)
}
