package scruffly.tools

import scala.language.implicitConversions
import java.io.{PrintWriter, FileWriter, File}
import scala.collection.mutable

// Definitions for declaring the logic

sealed trait Term
case class Node(name: String) extends Term  // choices or derived
case class Not(name: String) extends Term

sealed trait Compound {
  def imply(consequence: String) = Rule(this, consequence)
}
case class And(terms: List[Term]) extends Compound
case class Or(terms: List[Term]) extends Compound

case class Rule(condition: Compound, consequence: String)

case class Mutex(names: List[String])

// Definitions for seeing the worlds
// input assigns a boolean to each choice
case class StateWorld(input: Map[String, Boolean]) {
  def count(choices: List[String]) = choices.count(c => apply(c))

  def csv = (TestStates.choices ++ TestStates.output).map(c => apply(c).toString).mkString(",")

  def apply(state: String): Boolean =
    if (input.contains(state))
      input(state)
    else if (TestStates.rules_by_consequence.contains(state))
      TestStates.rules_by_consequence(state).condition match {
        case And(terms) => terms.forall(term => evaluate(term))
        case Or(terms) => terms.exists(term => evaluate(term))
      }
    else
      custom_state(state)

  def evaluate(term: Term) = term match {
    case Node(name) => apply(name)
    case Not(name) => !apply(name)
  }

  def custom_state(state: String): Boolean = {
    val doc_points = count(List("DOC_bar_success", "DOC_tiara")) + (2 * count(List("DOC_hero")))
    val leon_points = count(List("LEON_tiara", "LEON_plus_one", "MISC_splash_time"))

    return state match {
      case "MISC_leon_wins_mermaid" => leon_points >= doc_points
      case "MISC_doc_wins_mermaid" => doc_points > leon_points
    }
  }
}

object TestStates {
  implicit def str2node(name: String) = Node(name)
  def and(terms: Term*) = And(terms.toList)
  def or(terms: Term*) = Or(terms.toList)
  def not(name: String) = Not(name)

  // TODO auto generate the opposite case?

  def main(args: Array[String]) {
    generate_graphviz("hotel.dot")
    generate_csv("worlds.csv")
  }

  private def color(state: String): String =
    if (state.startsWith("AQUA_"))
      "cyan"
    else if (state.startsWith("MISC_"))
      "white"
    else if (state.startsWith("BUTLER_"))
      "lawngreen"
    else if (state.startsWith("DOC_"))
      "brown"
    else if (state.startsWith("SUZY_"))
      "orangered"
    else if (state.startsWith("CAT_"))
      "darkorchid1"
    else if (state.startsWith("LEON_"))
      "gold"
    else if (state.startsWith("no_"))
      color(state.stripPrefix("no_"))
    else
      throw new Exception(s"How to color $state?")

  def generate_graphviz(fn: String) {
    val w = new PrintWriter(new FileWriter(new File(fn)), true /* autoFlush */)
    w.println("digraph hotel {")
    w.println("  size = \"25,20\" ratio = \"fill\";")
    w.println("  node[height=1, width=2];")
    w.println("")

    for (choice <- choices) {
      w.println(s"  $choice [fillcolor=lightgrey, style=filled];")
    }
    w.println("")

    for ((mutex, idx) <- mutexes.zipWithIndex) {
      w.println(s"  subgraph cluster_$idx {")
      for (node <- mutex.names) {
        w.println(s"    $node [fillcolor=${color(node)}, style=filled];")
      }
      w.println("  }")
    }
    w.println("")

    for (rule <- rules) {
      val causes = rule.condition match {
        case And(terms) => terms
        case Or(terms) => {
          w.println(s"  ${rule.consequence} [shape=diamond];")
          terms
        }
      }
      for (cause <- causes) {
        cause match {
          case Node(name) => w.println(s"  $name -> ${rule.consequence};")
          case Not(name) => w.println(s"  $name -> ${rule.consequence} [color=red];")
        }
      }
    }

    w.println("}")
    w.close()
  }

  def generate_csv(fn: String) {
    val w = new PrintWriter(new FileWriter(new File(fn)), true /* autoFlush */)

    val worlds = filter_worlds(generate_worlds)
    w.println((choices ++ output).mkString(","))
    for (world <- worlds) {
      w.println(world.csv)
    }

    w.close()

    analyze_worlds(worlds)
  }

  def analyze_worlds(worlds: List[StateWorld]) {
    println(worlds.size + " worlds exist")

    // Check mutex violations
    println("Checking mutex violations...")
    for (w <- worlds) {
      for (m <- mutexes) {
        if (w.count(m.names) != 1) {
          val bad_coexist = m.names.filter(x => w(x))
          println(m + " violated: " + bad_coexist + " in " + w)
        }
      }
    }

    // What outputs possible?
    val outputs = worlds.map(w => TestStates.output.map(c => w.apply(c))).toSet
    println(outputs.size + " outcomes reachable")

    val w = new PrintWriter(new FileWriter(new File("unique_output.csv")), true /* autoFlush */)
    w.println(output.mkString(","))
    outputs.foreach(outcome => w.println(outcome.mkString(",")))
    w.close()

    // How many good worlds possible?
    val happy_ending = worlds.filter(w =>
      w("MISC_maid_n_adopt_suzy") && w("MISC_doc_wins_mermaid") && w("MISC_still_no_MISC_flood") &&
      (w("LEON_torch") || w("LEON_ghost"))
    )
    println(happy_ending.size + " worlds lead to a happy ending")
  }

  private def clone(
    world: mutable.HashMap[String, Boolean], choice: String, value: Boolean
  ): mutable.HashMap[String, Boolean] = {
    val copy = world.clone()
    copy(choice) = value
    return copy
  }

  def generate_worlds(): List[StateWorld] = {
    var all_worlds = List(new mutable.HashMap[String, Boolean]())
    for (choice <- choices) {
      all_worlds = all_worlds.flatMap(w => List(clone(w, choice, true), clone(w, choice, false)))
    }
    return all_worlds.map(w => StateWorld(w.toMap))
  }
  def filter_worlds(worlds: List[StateWorld]) = worlds.filter(
    w => w.count(List("BUTLER_no_cat_carpet", "BUTLER_cat_carpet_pre_mermaid", "BUTLER_cat_carpet_post_mermaid")) == 1
  ).filterNot(
    w => w.input("BUTLER_drink_coffee") && w.input("BUTLER_pool_booze")
  ).filterNot(
    w => w.input("BUTLER_cat_carpet_pre_mermaid") && w.input("MAID_clean_all_mud")
  ).filter(
    w => w.count(List("DOC_morepoints", "LEON_morepoints")) == 1
  )

  val choices = List(
    "BUTLER_no_cat_carpet", "BUTLER_cat_carpet_pre_mermaid", "BUTLER_cat_carpet_post_mermaid", "BUTLER_drink_coffee",
    "BUTLER_pool_booze", "BUTLER_carpet_walked_on", "BUTLER_hit_suzy", "MAID_doc_faint", "MAID_leon_linens",
    "BUTLER_wine_spill", "MAID_clean_all_mud", "DOC_morepoints", "LEON_morepoints"
  )
  val output = List(
    "SUZY_runs_free_pirate", "MISC_leon_amelia_adopt_suzy", "MISC_aqua_adopts_suzy",
    "SUZY_end_combusts", "SUZY_end_MISC_mud_monster", "MISC_maid_n_adopt_suzy",
    "MISC_doc_wins_mermaid", "MISC_leon_wins_mermaid", "AQUA_splash_free",
    "MISC_lit_fireplace", "LEON_ghost", "LEON_torch", "MISC_wine_MISC_flood", "MISC_water_MISC_flood", "MISC_still_no_MISC_flood",
    "MISC_murder", "LEON_beacon"
  )
  val mutexes = List(
    Mutex(List("BUTLER_no_cat_carpet", "BUTLER_cat_carpet_pre_mermaid", "BUTLER_cat_carpet_post_mermaid")),
    Mutex(List("MISC_splash_time", "no_MISC_splash_time")),
    Mutex(List("MISC_carpet_clean", "MISC_carpet_dirty")),
    Mutex(List("AQUA_mad", "AQUA_happy")),
    Mutex(List("DOC_bar_success", "no_DOC_bar_success")),
    Mutex(List("SUZY_destroyer", "SUZY_joke_firebug")),
    Mutex(List("CAT_piano", "no_CAT_piano")),
    Mutex(List("CAT_tiara", "DOC_tiara", "LEON_tiara")),
    Mutex(List("AQUA_good_day", "AQUA_bad_day")),
    Mutex(List("SUZY_scary", "SUZY_friendly_firebug", "SUZY_too_nice")),
    Mutex(List("MISC_flood", "no_MISC_flood")),
    Mutex(List("MISC_still_no_MISC_flood", "MISC_wine_MISC_flood", "MISC_water_MISC_flood")),
    Mutex(List("LEON_good_day", "LEON_bad_day")),
    Mutex(List("MISC_murder", "LEON_ghost", "LEON_torch", "LEON_beacon")),
    Mutex(List("MISC_mud_monster", "no_MISC_mud_monster")),
    Mutex(List("SUZY_combusts", "no_SUZY_combusts")),
    Mutex(List("SUZY_runs_free_pirate", "MISC_leon_amelia_adopt_suzy", "MISC_aqua_adopts_suzy",
               "SUZY_end_combusts", "SUZY_end_MISC_mud_monster", "MISC_maid_n_adopt_suzy")),
    Mutex(List("MISC_doc_wins_mermaid", "MISC_leon_wins_mermaid", "AQUA_splash_free")),
    Mutex(List("AQUA_splash_free", "no_AQUA_splash_free")),
    Mutex(List("DOC_hero", "no_DOC_hero")),
    Mutex(List("DOC_morepoints", "LEON_morepoints"))
  )
  val rules = List(
    or("BUTLER_drink_coffee",  not("BUTLER_pool_booze")).imply("no_MISC_splash_time"),
    and("BUTLER_pool_booze").imply("MISC_splash_time"),
    or("BUTLER_no_cat_carpet", "BUTLER_cat_carpet_post_mermaid").imply("CAT_late_carpet"),
    and("CAT_late_carpet", not("BUTLER_carpet_walked_on")).imply("MISC_carpet_clean"),
    or("BUTLER_carpet_walked_on", "BUTLER_cat_carpet_pre_mermaid").imply("MISC_carpet_dirty"),
    or("MISC_splash_time", "MISC_carpet_clean").imply("AQUA_happy"),
    and("no_MISC_splash_time", "MISC_carpet_dirty").imply("AQUA_mad"),
    and("BUTLER_hit_suzy", "BUTLER_drink_coffee").imply("SUZY_chaos"),
    and("SUZY_chaos").imply("SUZY_joke_firebug"),
    or(not("BUTLER_hit_suzy"), not("BUTLER_drink_coffee")).imply("SUZY_destroyer"),
    and("SUZY_joke_firebug", "no_CAT_piano").imply("SUZY_still_bug"),
    and("SUZY_destroyer", "no_CAT_piano").imply("SUZY_scary"),
    and("BUTLER_no_cat_carpet", "SUZY_scary").imply("AMELIA_voodooo"),
    and("AMELIA_voodooo", not("MAID_clean_all_mud"), "no_MISC_flood").imply("MISC_mud_monster"),
    and("SUZY_firey", "LEON_wood", "no_MISC_flood").imply("MISC_lit_fireplace"),
    or("SUZY_scary", "SUZY_friendly_firebug").imply("SUZY_firey"),
    or("SUZY_friendly_firebug", "SUZY_too_nice").imply("storytime"),
    or("SUZY_still_bug", "SUZY_chills").imply("SUZY_friendly_firebug"),
    and("SUZY_joke_firebug", "CAT_piano").imply("SUZY_too_nice"),
    and("SUZY_destroyer", "CAT_piano").imply("SUZY_chills"),
    and("MISC_cat_carpet").imply("CAT_piano"),
    or("BUTLER_no_cat_carpet").imply("no_CAT_piano"),
    or("BUTLER_cat_carpet_pre_mermaid", "BUTLER_cat_carpet_post_mermaid").imply("MISC_cat_carpet"),
    or(not("MAID_doc_faint"), "BUTLER_no_cat_carpet").imply("CAT_cant"),
    or("AQUA_happy", "BUTLER_drink_coffee").imply("DOC_bar_success"),
    and(not("BUTLER_drink_coffee"), "AQUA_mad").imply("no_DOC_bar_success"),
    and("MISC_cat_carpet", "MAID_doc_faint").imply("CAT_tiara"),
    and("CAT_cant", "DOC_bar_success").imply("DOC_tiara"),
    and("CAT_cant", "no_DOC_bar_success").imply("LEON_tiara"),
    or("CAT_tiara", "DOC_tiara").imply("LEON_wood"),
    and("LEON_wood", "no_MISC_splash_time").imply("LEON_bad_day"),
    and("MAID_leon_linens", "no_SUZY_combusts").imply("LEON_ghost"),
    and("DOC_bar_success", "AQUA_happy").imply("AQUA_good_day"),
    or("no_DOC_bar_success", "AQUA_mad").imply("AQUA_bad_day"),
    or("LEON_tiara", "DOC_tiara", "MISC_carpet_clean").imply("no_MISC_flood"),
    and("CAT_tiara", "MISC_carpet_dirty").imply("MISC_flood"),
    and("MISC_flood", "BUTLER_wine_spill").imply("MISC_wine_MISC_flood"),
    and("MISC_wine_MISC_flood", "LEON_torch").imply("DOC_hero"),
    and("MISC_flood", not("BUTLER_wine_spill")).imply("MISC_water_MISC_flood"),
    and("MISC_carpet_clean").imply("LEON_plus_one"),
    or("LEON_tiara", "MISC_splash_time").imply("LEON_good_day"),
    or("MAID_doc_faint", "LEON_tiara").imply("leon_not_bored"),
    and(not("MAID_leon_linens"), "leon_not_bored", "no_SUZY_combusts").imply("LEON_torch"),
    or("CAT_tiara", "DOC_tiara").imply("no_LEON_tiara"),
    and(not("MAID_doc_faint"), not("MAID_leon_linens"), "no_LEON_tiara").imply("attempted_MISC_murder"),
    and("no_SUZY_combusts", "attempted_MISC_murder").imply("MISC_murder"),
    and("no_MISC_flood").imply("MISC_still_no_MISC_flood"),
    and("SUZY_scary", "no_MISC_mud_monster", "no_MISC_flood").imply("SUZY_combusts"),
    or("MISC_cat_carpet", "MAID_clean_all_mud", "MISC_flood", "storytime").imply("no_MISC_mud_monster"),
    or("MISC_mud_monster", "storytime", "MISC_flood").imply("no_SUZY_combusts"),
    and("SUZY_combusts").imply("LEON_beacon"),
    and("MISC_wine_MISC_flood", "not_LEON_torch").imply("MISC_leon_amelia_adopt_suzy"),
    or("LEON_ghost", "MISC_murder", "LEON_beacon").imply("not_LEON_torch"),
    and("MISC_water_MISC_flood").imply("SUZY_runs_free_pirate"),
    and("no_MISC_flood", "SUZY_friendly_firebug").imply("simple_adoption"),
    or("simple_adoption", "DOC_hero").imply("MISC_maid_n_adopt_suzy"),
    and("no_MISC_flood", "SUZY_too_nice").imply("MISC_aqua_adopts_suzy"),
    and("SUZY_combusts").imply("SUZY_end_combusts"),
    and("MISC_mud_monster").imply("SUZY_end_MISC_mud_monster"),
    and("DOC_morepoints", "no_AQUA_splash_free").imply("MISC_doc_wins_mermaid"),
    and("LEON_morepoints", "no_AQUA_splash_free").imply("MISC_leon_wins_mermaid"),
    and("MISC_flood", "no_DOC_hero").imply("AQUA_splash_free"),
    or("DOC_hero", "no_MISC_flood").imply("no_AQUA_splash_free"),
    or("not_LEON_torch", "no_MISC_flood", not("BUTLER_wine_spill")).imply("no_DOC_hero")
  )

  val rules_by_consequence = rules.map(r => r.consequence -> r).toMap
}
