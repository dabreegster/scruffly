package scruffly.tools

import scruffly.Game
import scruffly.util._
import scruffly.engine._
import scruffly.components._

object RunLevel {
  def main(args: Array[String]) {
    val cfg = GameConfig("Hotel Solipcyst", 1000, 700, 30)
    val game = new SlickEngine(cfg) {
      override def setup() {
        // Debugging stuff
        Game.engine.add_component(new DebugControls(), "game")
        Game.engine.add_component(new ZoomControls(), "game")
        Game.engine.add_component(new DebugSprite(), "game")

        RunLevel.setup(args.head, args.tail.toList)
      }
    }
    game.run()
  }

  def setup(level: String, args: List[String]) {
    if (Game.assets == null) {
      level match {
        case _ => scruffly.hotel.Common.make_assets()
      }
    }

    // TODO yay hax kinda
    val load_level = args.headOption.getOrElse(level)
    level match {
      case "levels/atrium" => scruffly.hotel.Atrium.setup(load_level)
      case "levels/storeroom" => scruffly.hotel.Storeroom.setup(load_level)
      case "levels/floor" => scruffly.hotel.Floor.setup(load_level)
      case "levels/garden" => scruffly.hotel.Garden.setup(load_level)
      // Fake level! :D
      case "levels/hotel" => scruffly.hotel.Common.setup_all(args.headOption)
    }
  }
}
