package scruffly.tools

import scruffly.hotel.{Time, Schedule}

import scala.language.implicitConversions
import scala.collection.mutable
import java.io.{PrintWriter, FileWriter, File}

case class Transition(state1: String, state2: String, condition: Condition)
sealed trait Condition
case class NormalCondition(description: String) extends Condition
case class TimeCondition(time: Time, label: String) extends Condition

case class Hint(state: String, time: Time)

object Schedules {
  def main(args: Array[String]) {
    generate_graphviz("complex_schedule.dot", true)
    generate_graphviz("simple_schedule.dot", false)
  }

  def generate_graphviz(fn: String, complex: Boolean) {
    val w = new PrintWriter(new FileWriter(new File(fn)), true /* autoFlush */)
    w.println("digraph schedules {")
    if (complex) {
      w.println("  size = \"25,30\" ratio = \"fill\";")
      w.println("  node[height=1, width=2];")
      w.println("  edge[penwidth=5];")
    }
    w.println("")

    // Make new time nodes
    val times = new mutable.HashMap[Time, Int]().withDefault((_) => 0)
    def canonicalize(time: Time) = "time_" + time.prettyprint.replace(":", "_") + "_"
    def next_time(time: Time): String = {
      times(time) += 1
      return canonicalize(time) + (times(time) - 1)
    }

    // Draw all transitions
    for (transition <- ScheduleData.transitions) {
      transition.condition match {
        case NormalCondition(cond) => w.println("  %s -> %s [label=\"%s\", color=%s];".format(
          transition.state1, transition.state2, cond, color(transition.state1)
        ))
        case TimeCondition(time, label) => {
          val time_node = next_time(time)
          w.println("  %s -> %s [label=\"%s\", color=%s];".format(
            transition.state1, time_node, label, color(transition.state1)
          ))
          w.println("  %s -> %s [color=%s];".format(
            time_node, transition.state2, color(transition.state2)
          ))

          // Label time nodes simply
          if (!complex) {
            w.println("  %s [label=\"%s\"];".format(time_node, time.prettyprint))
          }
        }
      }
    }
    w.println("")

    // Color states
    val all_states = ScheduleData.transitions.flatMap(t => List(t.state1, t.state2)).toSet
    for (state <- all_states) {
      w.println(s"  $state [style=filled, fillcolor=${color(state)}];")
    }

    if (complex) {
      w.println("")

      // Force times and states at that time to line up
      for (time <- times.keys.toList.sortBy(_.raw_time)) {
        val all_instances =
          Range(0, times(time)).map(n => canonicalize(time) + n)// ++ find_states_ending_at(time)
        w.println("  { rank=same " + all_instances.mkString(" ") + " };")
      }
      w.println("")

      // Force time ordering
      /*val ordered_times = times.keys.toList.sortBy(_.raw_time)
      for ((time1, time2) <- ordered_times.zip(ordered_times.tail)) {
        w.println("  %s -> %s [color=black];".format(
          canonicalize(time1) + "0", canonicalize(time2) + "0"
        ))
      }*/
    }

    w.println("}")
    w.close()
  }

  private def color(state: String) =
    if (state.startsWith("Amelia"))
      "pink"
    else if (state.startsWith("Aquastazia"))
      "cyan"
    else if (state.startsWith("Cat"))
      "darkorchid1"
    else if (state.startsWith("Javoozy"))
      "brown"
    else if (state.startsWith("Leon"))
      "gold"
    else if (state.startsWith("SuzySol"))
      "orangered"

  private def find_states_ending_at(time: Time): List[String] = {
    // TODO rewrite functionally
    val ls = new mutable.ListBuffer[String]()
    for (transition <- ScheduleData.transitions) {
      transition.condition match {
        case TimeCondition(t, _) if time == t => {
          // Have to make sure there aren't two times, pick later if there are
          val all_times = ScheduleData.transitions
            .filter(shift => shift.state1 == transition.state1)
            .flatMap(shift => shift.condition match {
              case TimeCondition(shift_time, _) => Some(shift_time)
              case _ => None
            })
          if (!all_times.exists(_.raw_time > t.raw_time)) {
            ls += transition.state1
          }
        }
        case _ =>
      }
    }
    ls ++= ScheduleData.hints.filter(_.time == time).map(_.state)

    // TODO these make time crumple, not sure why
    ls --= List("Leon_SharkMode", "Leon_Paparrazi", "SuzySol_PlayGarden")

    // Duplicates could occur, pretty benign, but remove
    return ls.toSet.toList
  }
}

object ScheduleData {
  implicit def str2cond(description: String) = NormalCondition(description)
  implicit def time2cond(time: Time) = TimeCondition(time, "")

  val transitions = List(
    Transition("Amelia_Brunch", "Amelia_ChaseCat", Schedule.draw_crowd_time),
    Transition("Amelia_ChaseCat", "Amelia_BatheCat", "cat caught in carpet"),
    Transition("Amelia_ChaseCat", "Amelia_PlayPiano", Schedule.piano_time),
    Transition("Amelia_BatheCat", "Amelia_PlayPiano", Schedule.piano_time),
    Transition(
      "Amelia_PlayPiano", "Amelia_Rest", TimeCondition(Schedule.amelia_faint_time, "AND cat free")
    ),
    Transition("Amelia_PlayPiano", "Amelia_Trapped", "chandelier crashes AND cat caught"),
    Transition("Amelia_PlayPiano", "Amelia_Awake", Schedule.need_tiara_time),
    Transition(
      "Amelia_Trapped", "Amelia_Awake", TimeCondition(Schedule.need_tiara_time, "AND Leon frees her")
    ),
    Transition("Amelia_Rest", "Amelia_Awake", Schedule.need_tiara_time),
    Transition(
      "Amelia_Awake", "Amelia_Storytime", TimeCondition(Schedule.story_time, "AND Suzy isn't scary")
    ),
    Transition(
      "Amelia_Awake", "Amelia_Voodoo",
      TimeCondition(Schedule.story_time, "AND Suzy is scary AND no cat carpet")
    ),
    Transition("Amelia_Awake", "Amelia_Dinner", Schedule.dinner_time),
    Transition("Amelia_Storytime", "Amelia_Dinner", Schedule.dinner_time),
    Transition("Amelia_Voodoo", "Amelia_Dinner", Schedule.dinner_time),
    ////////////////////////////////////////////////////////////////////////////////////////////////
    Transition(
      "Aquastazia_DrawCrowd", "Aquastazia_ReturnSwimming",
      TimeCondition(Schedule.draw_crowd_time, "FFW if butler tries to enter garden")
    ),
    Transition("Aquastazia_ReturnSwimming", "Aquastazia_Swim", "at pool"),
    Transition("Aquastazia_Swim", "Aquastazia_JudgeCarpet", Schedule.aqua_judge_time),
    Transition("Aquastazia_JudgeCarpet", "Aquastazia_IdleAtBar", "after judging"),
    Transition(
      "Aquastazia_IdleAtBar", "Aquastazia_ListenPiano",
      TimeCondition(Schedule.piano_time, "AND doc nearby AND (happy splash time OR doc caffeinated)")
    ),
    Transition(
      "Aquastazia_IdleAtBar", "Aquastazia_PipeSmash",
      TimeCondition(Schedule.pipesmash_time, "AND cat has tiara AND carpet dirty")
    ),
    Transition(
      "Aquastazia_ListenPiano", "Aquastazia_PipeSmash",
      TimeCondition(Schedule.pipesmash_time, "AND cat has tiara AND carpet dirty")
    ),
    Transition("Aquastazia_PipeSmash", "Aquastazia_Dinner", Schedule.dinner_time),
    Transition("Aquastazia_ListenPiano", "Aquastazia_Dinner", Schedule.dinner_time),
    Transition("Aquastazia_IdleAtBar", "Aquastazia_Dinner", Schedule.dinner_time),
    ////////////////////////////////////////////////////////////////////////////////////////////////
    Transition("Cat_Brunch", "Cat_PlayGarden", Schedule.draw_crowd_time),
    Transition("Cat_PlayGarden", "Cat_ChaseBait", "butler drops bait somewhere"),
    Transition("Cat_PlayGarden", "Cat_SpreadMud", Schedule.rooms_open_time),
    Transition("Cat_PlayGarden", "Cat_Captured", "butler traps with carpet"),
    Transition("Cat_ChaseBait", "Cat_PlayGarden", "butler fails to trap with carpet"),
    Transition("Cat_ChaseBait", "Cat_Captured", "butler traps with carpet"),
    Transition(
      "Cat_Captured", "Cat_StealTiara",
      TimeCondition(Schedule.chandelier_crash_time, "AND chandelier crashed")
    ),
    Transition("Cat_SpreadMud", "Cat_Sleep", Schedule.need_tiara_time),
    Transition(
      "Cat_Sleep", "Cat_RideMudMonster",
      TimeCondition(Schedule.exit_storeroom_time, "AND mud monster exists")
    ),
    Transition("Cat_StealTiara", "Cat_TeaseAqua", "has tiara"),
    Transition(
      "Cat_TeaseAqua", "Cat_RideMudMonster",
      TimeCondition(Schedule.exit_storeroom_time, "AND mud monster exists")
    ),
    Transition("Cat_TeaseAqua", "Cat_Dinner", Schedule.dinner_time),
    Transition("Cat_Sleep", "Cat_Dinner", Schedule.dinner_time),
    Transition("Cat_RideMudMonster", "Cat_Dinner", Schedule.dinner_time),
    Transition("Cat_Captured", "Cat_Dinner", Schedule.dinner_time),
    ////////////////////////////////////////////////////////////////////////////////////////////////
    Transition("Javoozy_Brunch", "Javoozy_WaterGarden", Schedule.draw_crowd_time),
    Transition(
      "Javoozy_WaterGarden", "Javoozy_PoolBooze",
      TimeCondition(Schedule.pool_booze_march_time, "AND drunk")
    ),
    Transition(
      "Javoozy_WaterGarden", "Javoozy_ChillAtBar",
      TimeCondition(Schedule.pool_booze_march_time, "AND caffeinated")
    ),
    Transition("Javoozy_PoolBooze", "Javoozy_ChillAtBar", "butler splashes coffee"),
    Transition("Javoozy_PoolBooze", "Javoozy_PassedOut", "reaches pool"),
    Transition("Javoozy_PassedOut", "Javoozy_ChillAtBar", Schedule.frat_doc_wakeup_time),
    Transition("Javoozy_ChillAtBar", "Javoozy_ListenPiano", Schedule.piano_time),
    Transition(
      "Javoozy_ListenPiano", "Javoozy_WanderUpstairs",
      TimeCondition(Schedule.need_tiara_time, "AND NOT (caffeinated OR happy splash time)")
    ),
    Transition(
      "Javoozy_ListenPiano", "Javoozy_FindTiara",
      TimeCondition(Schedule.need_tiara_time, "AND (caffeinated OR happy splash time)")
    ),
    Transition("Javoozy_FindTiara", "Javoozy_ReturnTiara", "at the tiara"),
    Transition(
      "Javoozy_FindTiara", "Javoozy_Fainted",
      TimeCondition(Schedule.chandelier_crash_time, "FFW if see naked maid")
    ),
    Transition(
      "Javoozy_ReturnTiara", "Javoozy_Fainted",
      TimeCondition(Schedule.chandelier_crash_time, "FFW if see naked maid")
    ),
    Transition(
      "Javoozy_WanderUpstairs", "Javoozy_Fainted",
      TimeCondition(Schedule.chandelier_crash_time, "FFW if see naked maid")
    ),
    Transition("Javoozy_WanderUpstairs", "Javoozy_WaitForDinner", Schedule.javoozy_chill_time),
    Transition("Javoozy_ReturnTiara", "Javoozy_WaitForDinner", "delivered tiara"),
    Transition("Javoozy_WaitForDinner", "Javoozy_Dinner", Schedule.dinner_time),
    Transition("Javoozy_Fainted", "Javoozy_Dinner", Schedule.dinner_time),
    ////////////////////////////////////////////////////////////////////////////////////////////////
    Transition("Leon_Brunch", "Leon_PaceAnxiously", Schedule.draw_crowd_time),
    Transition("Leon_PaceAnxiously", "Leon_SharkMode", Schedule.leon_shark_time),
    Transition(
      "Leon_SharkMode", "Leon_Paparrazi",
      TimeCondition(Schedule.leon_paparrazi_time, "AND pool not boozed")
    ),
    Transition("Leon_SharkMode", "Leon_GotoRoom", Schedule.rooms_open_time),
    Transition("Leon_Paparrazi", "Leon_AwkwardFlirt", Schedule.aqua_judge_time),
    Transition("Leon_AwkwardFlirt", "Leon_GotoRoom", Schedule.rooms_open_time),
    Transition("Leon_GotoRoom", "Leon_WaitInRoom", "reached door"),
    Transition("Leon_WaitInRoom", "Leon_ReturnDownstairs", Schedule.leon_downstairs_time),
    Transition("Leon_ReturnDownstairs", "Leon_ReturnTiara", "found tiara"),
    Transition("Leon_ReturnDownstairs", "Leon_HandleChandelier", "didn't find tiara"),
    Transition("Leon_ReturnTiara", "Leon_HandleChandelier", "near Aqua"),
    Transition("Leon_HandleChandelier", "Leon_GetWood", "after rescuing Amelia or immediately"),
    ////////////////////////////////////////////////////////////////////////////////////////////////
    Transition("SuzySol_Brunch", "SuzySol_PlayGarden", Schedule.draw_crowd_time),
    Transition("SuzySol_PlayGarden", "SuzySol_TryToSleep", "hit by 3 booze seeds"),
    Transition("SuzySol_PlayGarden", "SuzySol_TryToSleep", Schedule.suzy_sleepy_time),
    Transition("SuzySol_PlayGarden", "SuzySol_ConductChaos", "hit by 3 coffee seeds"),
    Transition("SuzySol_TryToSleep", "SuzySol_LinenDestroyer", "sees maid"),
    Transition("SuzySol_ConductChaos", "SuzySol_TellJokes", Schedule.rooms_open_time),
    Transition("SuzySol_LinenDestroyer", "SuzySol_ListenPiano", Schedule.piano_time),
    Transition("SuzySol_TellJokes", "SuzySol_Firebug", Schedule.jokes_to_firebug_time),
    Transition("SuzySol_Firebug", "SuzySol_ListenPiano", Schedule.piano_time),
    Transition(
      "SuzySol_ListenPiano", "SuzySol_ListenStorytime",
      TimeCondition(Schedule.story_time, "AND not destroyer")
    )
  )

  val hints = List(
    Hint("Aquastazia_ReturnSwimming", Schedule.draw_crowd_time),
    Hint("Aquastazia_JudgeCarpet", Schedule.aqua_judge_time),
    Hint("Cat_ChaseBait", Schedule.rooms_open_time),
    Hint("Javoozy_PoolBooze", Schedule.frat_doc_wakeup_time),
    Hint("Leon_GotoRoom", Schedule.rooms_open_time)
  )
}
