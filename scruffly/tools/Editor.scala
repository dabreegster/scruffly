package scruffly.tools

import scruffly._
import scruffly.util._
import scruffly.engine._
import scruffly.components._

object LevelEditor {
  def main(args: Array[String]) {
    val level = args.head
    val cfg = GameConfig("Hotel Solipcyst - Level Editor", 800, 600, 60)
    val game = new SlickEngine(cfg) {
      override def setup() {
        level match {
          case _ => scruffly.hotel.Common.make_assets()
        }

        add_component(new ZoomControls(), "editor")

        // TODO eww
        val size_per_tile = scruffly.hotel.Hotel.char_hitbox
        val world = WorldSpec.load(level).make_tile_world(size_per_tile, Set(), Set(), use_effects = false)
        world.all_sprites.foreach(s => add_component(s, "editor"))
        add_component(world.get_tilemap.get, "editor")

        val rect_spawner = new RectangleSpawner(world)
        add_component(rect_spawner, "editor")
        val poly_spawner = new PolygonSpawner(world)
        add_component(poly_spawner, "editor")
        val select = new SelectObject(world, rect_spawner, poly_spawner)
        add_component(select, "editor")
        add_component(new TransformObject(select, world), "editor")
        add_component(new DragObject(select), "editor")
        add_component(new SaveWorld(level, world), "editor")
        add_component(new Spawner(world, select), "editor")

        add_component(new Scroller(), "editor")
        add_component(new ShowMouse(), "editor")
        add_component(new DebugControls(), "editor")
      }
    }
    game.run()
  }
}
