package scruffly.util

import scruffly.engine._

trait GeometryFactory {
  def rect(width: Float, height: Float, angle_degrees: Float = 0): List[Pt] = {
    /*val t = math.toRadians(angle_degrees)
    val x1 = 0
    val y1 = 0
    val x2 = width
    val y2 = height

    val rx1 = (x1 * math.cos(t) - y1 * math.sin(t)).toFloat
    val ry1 = (x1 * math.sin(t) + y1 * math.cos(t)).toFloat
    val rx2 = (x2 * math.cos(t) - y2 * math.sin(t)).toFloat
    val ry2 = (x2 * math.sin(t) + y2 * math.cos(t)).toFloat

    return List(
      Pt(rx1, ry1), Pt(rx2, ry1), Pt(rx2, ry2),
      Pt(rx1, ry2)
    )*/

    // TODO dont use slick! I can do my own math! I'm an ADULT! Maaaaaaaan.
    val rect = new org.newdawn.slick.geom.Rectangle(0, 0, width, height)
    val poly = rect.transform(org.newdawn.slick.geom.Transform.createRotateTransform(
      math.toRadians(angle_degrees).toFloat, 0, 0
    ))
    return poly.getPoints.grouped(2).map(pt => Pt(pt(0), pt(1))).toList
  }

  def translate(obj: List[Pt], dx: Float, dy: Float) =
    obj.map(pt => Pt(pt.x + dx, pt.y + dy))
  def translate_lines(obj: List[Line], dx: Float, dy: Float) =
    obj.map(l => Line(Pt(l.pt1.x + dx, l.pt1.y + dy), Pt(l.pt2.x + dx, l.pt2.y + dy)))
  def scale(obj: List[Pt], ratio: Float) =
    obj.map(pt => Pt(pt.x * ratio, pt.y * ratio))

  // y inversion! negative y is up.
  def circle_pt(r: Float, theta_deg: Float) = Pt(
    (r * math.cos(math.toRadians(theta_deg))).toFloat, -(r * math.sin(math.toRadians(theta_deg))).toFloat
  )
  def circle_arc(r: Float, theta1: Float, theta2: Float, segments: Int = 10): List[Pt] = {
    val t2 = if (theta2 < theta1) theta2 + 360 else theta2
    return Range(0, segments + 1).map(
      i => circle_pt(r, theta1 + (t2 - theta1) * (i.toFloat / segments))
    ).toList
  }

  def ellipse_pt(r1: Float, r2: Float, theta_deg: Float) = Pt(
    (r1 * math.cos(math.toRadians(theta_deg))).toFloat,
    -(r2 * math.sin(math.toRadians(theta_deg))).toFloat
  )
  def ellipse_arc(r1: Float, r2: Float, theta1: Float, theta2: Float, segments: Int = 10): List[Pt] = {
    val t2 = if (theta2 < theta1) theta2 + 360 else theta2
    return Range(0, segments + 1).map(
      i => ellipse_pt(r1, r2, theta1 + (t2 - theta1) * (i.toFloat / segments))
    ).toList
  }

  // r1 and r2 should be (inner, outer)
  def ring(r1: (Float, Float), r2: (Float, Float), theta: (Float, Float), segments: Int = 10) =
    ellipse_arc(r1 = r1._1, r2 = r2._1, theta1 = theta._1, theta2 = theta._2, segments) ++
    ellipse_arc(r1 = r1._2, r2 = r2._2, theta1 = theta._1, theta2 = theta._2, segments).reverse

  def points_to_closed_polygon(pts: List[Pt]) = Line(pts.last, pts.head) :: points_to_lines(pts)
  // TODO unify name
  def close(pts: List[Pt]) = points_to_closed_polygon(pts)
  def close_points(pts: List[Pt]) =
    if (pts.head == pts.last)
      pts
    else
      pts.last :: pts
  def points_to_lines(pts: List[Pt]) = pts.zip(pts.tail).map(pair => Line(pair._1, pair._2))

  // Holes must be sorted
  def intervals(range: (Float, Float), holes: List[(Float, Float)]): List[(Float, Float)] = holes match {
    case (t1, t2) :: rest => (range._1, t1) :: intervals((t2, range._2), rest)
    case Nil => List(range)
  }

  // So tired of specifying these manually.
  def hitbox(xs: (Float, Float), ys: (Float, Float), indent: Float = 0) = List(
    // Let the left/right line stretch fully, but avoid two points being in two different lines by
    // indenting up/down forcibly
    Line(Pt(xs._1 + 1 + indent, ys._1), Pt(xs._2 - 1 - indent, ys._1)),
    Line(Pt(xs._2, ys._1 + indent), Pt(xs._2, ys._2 - indent)),
    Line(Pt(xs._2 - 1 + indent, ys._2), Pt(xs._1 + 1 - indent, ys._2)),
    Line(Pt(xs._1, ys._2 - indent), Pt(xs._1, ys._1 + indent))
  )

  def isobox(pts: List[Pt]): List[Line] = {
    val top = pts.minBy(_.y)
    val left = pts.minBy(_.x)
    val right = pts.maxBy(_.x)
    val down = pts.maxBy(_.y)
    assert(Set(top, left, right, down).size == 4)

    assert(top.x < right.x)
    assert(right.y < down.y)
    assert(left.x < down.x)
    assert(top.y < left.y)
    return points_to_closed_polygon(List(top, right, down, left))
  }

  def calc_dims(pts: List[Pt]): Pt = {
    val min_x = pts.map(_.x).min
    val max_x = pts.map(_.x).max
    val min_y = pts.map(_.y).min
    val max_y = pts.map(_.y).max
    return Pt(max_x - min_x, max_y - min_y)
  }

  def calc_center(pts: Iterable[Pt]): Pt = {
    val sum_x = pts.map(_.x).sum
    val sum_y = pts.map(_.y).sum
    return Pt(sum_x / pts.size, sum_y / pts.size)
  }
}
