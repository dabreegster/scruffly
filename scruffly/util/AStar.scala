package scruffly.util

import scala.collection.mutable
import java.util

case class Pathfind[T](
  start: T,
  goal: Option[T],
  successors: (T) => Seq[T],
  // old state, new state, old state's backref
  calc_cost: (T, T, T) => Float,
  calc_heuristic: (T) => Float,
  tie_breaker: (T, T) => Int
)

object AStar {
  def path[T](spec: Pathfind[T]): List[T] = {
    // Stitch together our path
    val backrefs = new mutable.HashMap[T, T]()
    // We're finished with these
    val visited = new mutable.HashSet[T]()
    // Best cost so far
    val costs = new mutable.HashMap[T, Float]()

    val open = new JavaPriorityQueue[T](spec.tie_breaker)

    costs(spec.start) = 0
    open.insert(spec.start, spec.calc_heuristic(spec.start))
    // Indicate start in backrefs by not including it

    while (open.nonEmpty) {
      val current = open.shift()
      visited += current

      val done =
        if (spec.goal.isDefined)
          current == spec.goal.get
        else
          open.isEmpty && visited.size > 1
      if (done) {
        // Reconstruct the path
        val path = new mutable.ListBuffer[T]()
        var pointer: Option[T] = Some(current)
        while (pointer.isDefined) {
          path.prepend(pointer.get)
          pointer = backrefs.remove(pointer.get)
        }
        return path.toList
      } else {
        // This foreach manually rewritten as a while to avoid closures in a tight loop.
        for (next_state <- spec.successors(current)) {
          if (!visited.contains(next_state)) {
            val tentative_cost = costs(current) + spec.calc_cost(
              current, next_state, backrefs.getOrElse(current, current)
            )
            if (!open.contains(next_state) || tentative_cost < costs(next_state)) {
              backrefs(next_state) = current
              costs(next_state) = tentative_cost
              // if they're in open_members, modify weight in the queue? or
              // new step will clobber it. fine.
              open.insert(next_state, tentative_cost + spec.calc_heuristic(next_state))
            }
          }
        }
      }
    }

    // TODO this is how we indicate failure...
    return Nil
  }
}

class JavaPriorityQueue[T](tie_breaker: (T, T) => Int) {
  private case class Item(item: T, weight: Float)

  private val pq = new util.PriorityQueue[Item](100, new util.Comparator[Item]() {
    override def compare(a: Item, b: Item) =
      if (a.weight < b.weight)
        -1
      else if (a.weight > b.weight)
        1
      else
        tie_breaker(a.item, b.item)
  })
  private val members = new mutable.HashSet[T]()

  def insert(item: T, weight: Float) {
    pq.add(Item(item, weight))
    members += item
  }

  def shift(): T = {
    val item = pq.poll().item
    members -= item // TODO not true if there are multiples!
    return item
  }

  def contains(item: T) = members.contains(item)
  def isEmpty = pq.isEmpty
  def nonEmpty = !pq.isEmpty
  def clear() {
    pq.clear()
  }
}
