package scruffly.util

object Util {
  def lerp(x1: Double, x2: Double, alpha: Double) = x1 * (1.0 - alpha) + alpha * x2
  // Pick value closer to 0. cap should be positive
  def smart_min(value: Float, cap: Float) =
    if (value > 0)
      math.min(value, cap)
    else
      math.max(value, -cap)
  // Yield 1, -1, or 0
  def normalize(value: Float) =
    if (value > 0)
      1
    else if (value < 0)
      -1
    else
      0
}
