package scruffly.util

import scala.util.Random

object RNG {
  val rand = new Random(System.currentTimeMillis)

  def double(min: Double, max: Double): Double =
    if (min > max)
      throw new IllegalArgumentException("rand(" + min + ", " + max + ") requested")
    else if (min == max)
      min
    else
      min + rand.nextDouble * (max - min)
  def int(min: Int, max: Int) = double(min, max).toInt
  def float(min: Float, max: Float) = double(min, max).toFloat
  def choose[T](from: Seq[T]): T = from(rand.nextInt(from.length))
  // return true 'p'% of the time. p is [0.0, 1.0]
  def percent(p: Double) = double(0.0, 1.0) < p

  def weighted_choose[T](choices: List[(T, Int)]): T = {
    assert(choices.map(_._2).sum == 100)
    val x = int(0, 100)
    var sum = 0
    for ((obj, n) <- choices) {
      sum += n
      if (x <= sum) {
        return obj
      }
    }
    throw new Exception("impossible")
  }
}
