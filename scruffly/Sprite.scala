package scruffly

import scruffly.engine._
import scruffly.util._
import scruffly.components._

import scala.collection.mutable

class Sprite(
  orig_instance: Instance, val layer: String, orig_renderer: Renderer, val world: World
) extends Component with GeometryFactory {
  var renderer = orig_renderer
  var instance = orig_instance
  var physics: PhysicsModule = null
  var behavior: Behavior = new NullBehavior(this)
  // TODO hacky, used for sight zones to point to owner
  var owner: Sprite = null
  // TODO hacky, because child_components don't always make sense
  val fated_others = new mutable.ListBuffer[Component]()
  var debug_me = false

  // Sprites contain components and delegate to them.
  val child_components = new mutable.ListBuffer[(String, Component)]()
  // May lazily create certain child components if they don't exist
  def get_child(name: String): Component = child_components.find(c => c._1 == name) match {
    case Some(pair) => pair._2
    case None => name match {
      case "speech" => {
        val c = new SpeechBubble(this)
        child_components += (("speech", c))
        c
      }
      case "pause" => {
        val c = new PauseSprite(this)
        child_components += (("pause", c))
        c
      }
      case "shake" => {
        val c = new ShakeEffect(this)
        child_components += (("shake", c))
        c
      }
      case "glow" => {
        val c = new GlowSprite(this)
        child_components += (("glow", c))
        c
      }
      case _ => throw new IllegalArgumentException(s"$this doesn't have $name!")
    }
  }
  def remove_child(name: String) {
    child_components -= child_components.find(_._1 == name).get
  }
  def has_child(name: String) = child_components.find(_._1 == name).isDefined
  def speech = get_child("speech").asInstanceOf[SpeechBubble]
  def pauser = get_child("pause").asInstanceOf[PauseSprite]

  def all_components = List(this, behavior) ++ child_components.map(_._2)

  //override def toString = s"$instance @ ($position_x, $position_y)"
  override def toString = name

  // Position represents the top-left corner of the sprite
  var position_x = 0f
  var position_y = 0f
  // This'll be set when this component runs, so anything depending on it should register after
  val current_zones = new mutable.HashSet[Sprite]()
  var speed_x = 0f
  var speed_y = 0f
  // Angles in increments of 45 degrees. Make a type?
  private var direction = 270

  val data = new mutable.HashMap[String, String]()
  def get(key: String) = data.getOrElse(key, "")

  // Should only be used for setup, defies all the collision physics stuff
  def warp(to: Pt) {
    world.before_move(this)
    position_x = to.x
    position_y = to.y
    world.after_move(this)
  }

  def destroy() {
    world.destroy(this)
    Game.engine.del_component(this, world.level)
    for ((_, c) <- child_components) {
      Game.engine.del_component(c, world.level)
    }
    for (c <- fated_others) {
      c match {
        case s: Sprite => s.destroy()
        case _ => Game.engine.del_component(c, world.level)
      }
    }
  }

  override def render(gfx: DrawModule) {
    gfx.set_translation(position_x, position_y)
    renderer.draw(this)
    if (gfx.debug) {
      physics.debug_render(gfx)
      gfx.text(instance.effects.mkString("\n"), 5, 5, RGB(255, 0, 0))
      gfx.set_translation(0, 0)
      gfx.circle(5, RGB(0, 255, 0), get_collider_pos.x, get_collider_pos.y)
      gfx.set_translation(position_x, position_y)
    }
    gfx.zorder_render(behavior)
    for ((_, c) <- child_components) {
      gfx.zorder_render(c)
    }
  }

  override def update(dt: Float) {
    current_zones.clear()

    // Short-circuit otherwise
    if (behavior.is_agent || speed_x != 0 || speed_y != 0) {
      move((dt * speed_x, dt * speed_y))

      val old_speed_x = speed_x
      val old_speed_y = speed_y
      behavior.update(dt)
      direction = (Util.normalize(speed_x), Util.normalize(speed_y)) match {
        case (-1, 0) => 180
        case (1, 0) => 0
        case (0, -1) => 90
        case (0, 1) => 270
        case (-1, -1) => 135
        case (-1, 1) => 225
        case (1, -1) => 45
        case (1, 1) => 315
        case _ => direction
      }
      val move_requested_x = Util.normalize(speed_x - old_speed_x)
      val move_requested_y = Util.normalize(speed_y - old_speed_y)
      if (move_requested_x != 0 || move_requested_y != 0) {
        renderer.set_mode(direction.toString)
      }

      physics.adjust_speed(dt, move_requested_x != 0, move_requested_y != 0)
    }

    for ((_, c) <- child_components) {
      c.update(dt)
    }
  }

  // True if moved
  def move(orig_move: (Float, Float)): Boolean = {
    world.before_move(this)
    val final_move = try_move(orig_move)
    position_x += final_move._1
    position_y += final_move._2
    world.after_move(this)
    return final_move._1 != 0 || final_move._2 != 0
  }

  // Returns the delta to position that we should apply
  private def try_move(delta: (Float, Float)): (Float, Float) = {
    // This could in theory recurse really deeply. That's a bug if it happens, so let the stack
    // loudly overflow.
    val hitbox = physics.movement_hitbox(delta._1, delta._2)
    val all_ents = world.get_colliders(this, hitbox).filter(other => other._2.obj != this)

    for ((handler, tester) <- all_ents) {
      // See if we're in their zone, different type of collision check than below
      if (tester.obj.data.contains("zone")) {
        val l1 = tester.obj.get_collider.get_lines
        val l2 = get_collider.get_lines
        if (Game.engine.collisions.polygons_hit(l1, l2) || Game.engine.collisions.point_in_polygon(l1, get_collider_pos)) {
          current_zones += tester.obj
        }
      }

      val prescripted_delta = SpriteCollisionModule(this, tester, delta._1, delta._2)
      if (prescripted_delta != delta) {
        handler.callback(Collision(this, tester.obj, delta, prescripted_delta)) match {
          case Retry() => return try_move(delta)
          case Correct() => return try_move(prescripted_delta)
          case Ghost() => // ignore
        }
      }
    }

    // We didn't hit anything bad, so the original delta must be fine
    return delta
  }

  def get_pos = Pt(position_x, position_y)
  def get_center = Pt(
    position_x + renderer.get_dims.x / 2,
    position_y + renderer.get_dims.y / 2
  )

  private val dist_threshold = 500
  def nearby(other: Sprite, dist: Float = dist_threshold) =
    other.get_collider_pos.dist(get_collider_pos) <= dist

  def get_collider = physics.get_collider
  def get_collider_pos = physics.get_collider_pos
  def get_collider_pos_topleft = physics.get_hitbox.topleft
  def get_hitbox = physics.get_hitbox

  def direction_angle = direction

  // TODO rename. but the point is, have it be easy to set up some stuff.
  def ez_say(line: String) {
    speech.say(line)
  }
  def ez_pause(time: Float) {
    pauser.pause(Some(time))
  }
  def ez_shake(time: Float) {
    get_child("shake").asInstanceOf[ShakeEffect].shake(time)
  }
  def ez_goto(spot: Sprite) {
    behavior = new NPCGotoStaticBehavior(this, spot)
  }
  def ez_chase(target: Sprite) {
    behavior = new BreadcrumbChaseTileBehavior(this, () => Some(target))
  }
  def ez_roam(region: Sprite, pause: Float, exclude: Option[Sprite] = None) {
    behavior = new RoamTileBehavior(this, region, exclude, pause)
  }
  def ez_patrol(layer: String, pause: Float = 0) {
    behavior = new RandomPatrolTileBehavior(
      this, () => world.all_sprites.filter(_.layer == layer), pause
    )
  }
  def ez_on_fire = world.all_sprites.exists(s => s.get("id") == "flames_" + name)

  def get_zone(zone: String) = current_zones.find(z => z.data("zone") == zone)
  def in_zone(zone: String) = get_zone(zone).isDefined
  def in_sight_zone(owner: String) =
    current_zones.exists(z => z.get("zone") == "sight" && z.owner.name == owner)

  // Draw top to bottom.
  // TODO doesnt use center of hitbox, that'd be... weird for big scenery
  override def zorder = data.get("zorder").map(_.toFloat).getOrElse(get_pos.y)

  def name = instance.asset
}

abstract class Behavior(sprite: Sprite) extends Component {
  def is_agent(): Boolean = true
}

class NullBehavior(sprite: Sprite) extends Behavior(sprite) {
  override def update(dt: Float) {}
  override def is_agent() = false
}
